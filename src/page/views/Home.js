import React from 'react';

import PageHero from '../components/home/Hero';
import PageAbout from '../components/home/About';
import PageWorks from '../components/home/Work';
import PageFaq from '../components/home/Faq';
import PageApplication from '../components/home/Application';
import PageContact from '../components/home/Contact';

const PageHome = () => {
  return (
    <div className="main">
      <PageHero/>
      <PageAbout/>
      <PageApplication/>
      <PageWorks/>
      <PageFaq/>
      <PageContact/>
    </div>
  );
}

export default PageHome;