import React from "react";
import SubMenu from "antd/lib/menu/SubMenu";
import { Menu, Layout } from "antd";
import {
  PieChartOutlined,
  HddOutlined,
  TeamOutlined,
  SettingOutlined,
  UsergroupAddOutlined,
  SyncOutlined,
  LineChartOutlined
} from "@ant-design/icons";


import { Link, useLocation } from "react-router-dom";

const Sidebar = ({collapsed}) => {
  let location = useLocation();
  console.log('Location: ', location.pathname);
  
  return (
    <Layout.Sider
      width={"100%"}
      trigger={null}
      collapsible
      collapsed={collapsed}
      className="site-layout-background"
    >
      <Menu
        // mode="inline"
        mode="inline"
        theme="dark"
        defaultSelectedKeys={location.pathname == '/' ? 'dashboard' : location.pathname}
        style={{ borderRight: 0 }}
        className="ccc-sidebar-menu"
      >
        <Menu.Item style={{marginTop:"15px"}} key="dashboard" icon={<PieChartOutlined className="ccc-icon"/>}>
          <Link to="/">
            Dashboard
          </Link>
        </Menu.Item>

        <SubMenu
          key="utilizadores"
          icon={<UsergroupAddOutlined className="ccc-icon" />}
          title="Gestão de Utilizadores"
        >
          <Menu.Item key="utilizadores">
            <Link
              to="/utilizadores"
            >
              Utilizadores
            </Link>
          </Menu.Item>
          <Menu.Item key="2">
            <Link
              to="/utilizadores-ccc"
            >
              Gestão de Credenciais
            </Link>
          </Menu.Item>
        </SubMenu>

        <SubMenu
          key="clientes"
          icon={<TeamOutlined className="ccc-icon" />}
          title="Gestão de clientes"
        >
          <Menu.Item key="gestao-clientes">
            <Link
              to="/clientes"
            >
              Clientes
            </Link>
          </Menu.Item>
          <Menu.Item key="3">
            <Link
              to="/credenciais-vendendores"
            >
              Gestão de Credenciais
            </Link>
          </Menu.Item>
        </SubMenu>

        <SubMenu key="maquinas" icon={<HddOutlined className="ccc-icon" />} title="Máquinas">
          <Menu.Item key="4">
            <Link
              to="/maquinas"
            >
              Máquinas
            </Link>
          </Menu.Item>
          <Menu.Item key="5">
            <Link
              to="/associar-maquinas"
            >
              Associar Máquinas
            </Link>
          </Menu.Item>
        </SubMenu>

        <Menu.Item key="operacoes" icon={<SyncOutlined className="ccc-icon" />}>
          <Link
            to="/operacoes"
          >
            Operações
          </Link>
        </Menu.Item>

        <SubMenu key="relatorios" icon={<LineChartOutlined className="ccc-icon" />} title="Relatórios">
          <Menu.Item key="6">
            <Link
              to="/relatorios-ganhos"
            >
              Ganhos
            </Link>
          </Menu.Item>
          <Menu.Item key="7">
            <Link
              to="/relatorios-ussd"
            >
              Uso do USSD
            </Link>
          </Menu.Item>
          <Menu.Item key="8">
            <Link
              to="/relatorios-despesas"
            >
              Despesas SIMO
            </Link>
          </Menu.Item>
        </SubMenu>

        <Menu.Item key="configuracoes" icon={<SettingOutlined className="ccc-icon" />}>
          <Link
            to="/configuracoes"
          >
            Configurações
          </Link>
        </Menu.Item>
      </Menu>
    </Layout.Sider>
  );
};

export default Sidebar;
