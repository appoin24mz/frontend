import axios from "axios";
import { downloadFile } from "./utils/file";

const call = (
  method,
  endpoint,
  params,
  token = "no-token-4-u",
  responseType
) => {
  let body = undefined;

  // We only fill the `body` with data when dealing with a POST,
  // because a GET doesn't allow content inside the `body`.
  if (method === "POST") {
    body = JSON.stringify(params);
  }

  return axios({
    baseURL: process.env.REACT_APP_API_URL,
    responseType: responseType,
    method: method,
    url: endpoint,
    data: body,
    headers: { "app24-authorization":"", "app24-client-type":"app", "content-type": "application/json" },
  })
    .then(responseHandler({ method, endpoint, params }))
    .catch((err) => {
      if (!err.response) {
        // Handle connection errors such as 'Network Error' when the server is not responding,
        // or a connection to the server could not be estabilished.
        return resolveServerError(
          {
            message: "Sem conexão com o servidor.",
            httpcode: 503, // Let's use HTTP Status 503: Service Unavailable.
          },
          { method, endpoint, params }
        );
      }

      const { message, status } = err.response.data;
      // Test for 4xx and 5xx.
      if (status.toString().match("4[0-9]{2}|5[0-9]{2}")) {
        return resolveServerError(
          {
            message: message || "Ocorreu um erro.",
            httpcode: status,
          },
          { method, endpoint, params }
        );
      }

      return Promise.reject({ message, status });
    });
};

const resolveServerError = (error, { method, endpoint, params }) => {
  if (process.env.NODE_ENV !== "production") {
    console.error(
      `%c API: ${method} `,
      "color: white; background-color: red;",
      { endpoint, params, error }
    );
  }

  return Promise.reject(error);
};

const responseHandler = ({ method, endpoint, params }) => (resp) => {
  const { status } = resp;
  // Test for 4xx and 5xx.
  if (status.toString().match("4[0-9]{2}|5[0-9]{2}")) {
    return resolveServerError(
      {
        message: "Ocorreu um erro.",
        httpcode: status,
      },
      { method, endpoint, params }
    );
  }

  const encoding = resp.headers["content-transfer-encoding"];
  if (encoding === "binary") {
    return resolveFile(resp, { method, endpoint, params });
  }

  return resolveJSON(resp.data, { method, endpoint, params });
};

const resolveFile = (resp, { method, endpoint, params }) => {
  return new Promise((resolve) => {
    const contentDisp = resp.headers["content-disposition"];

    const extractedFilename = contentDisp.substring(22, contentDisp.length - 1);

    if (process.env.NODE_ENV !== "production") {
      console.log(
        `%c API: ${method} `,
        "color: white; background-color: green;",
        { endpoint, params, binaryData: resp.data }
      );
    }

    downloadFile({
      filename: extractedFilename,
      contentType: resp.headers["content-type"],
      binaryData: resp.data,
    });

    resolve();
  });
};

const resolveJSON = (json, { method, endpoint, params }) => {
  return new Promise((resolve, reject) => {
    if (process.env.NODE_ENV !== "production") {
      console.log(
        `%c API: ${method} `,
        "color: white; background-color: green;",
        { endpoint, params, json }
      );
    }

    resolve(json);
  });
};

export default {
  get: (endpoint, params) => {
    return call("GET", endpoint, params, null);
  },
  post: (endpoint, params, responseType = "json") => {
    return call("POST", endpoint, params, null, responseType);
  },
};
