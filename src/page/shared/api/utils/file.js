export const downloadFile = ({ filename, contentType, binaryData }) => {
  // Adapted from: https://blog.jayway.com/2017/07/13/open-pdf-downloaded-api-javascript/
  // It is necessary to create a new blob object with mime-type explicitly set
  // otherwise only Chrome works like it should.
  const blob = new Blob([binaryData], { type: contentType });

  // IE doesn't allow using a blob object directly as link href
  // instead it is necessary to use msSaveOrOpenBlob.
  if (navigator && navigator.msSaveOrOpenBlob) {
    navigator.msSaveOrOpenBlob(blob);
    return;
  }

  // For other browsers:
  // Create a link pointing to the ObjectURL containing the blob.
  const data = URL.createObjectURL(blob);
  const link = document.createElement('a');
  link.href = data;
  link.download = filename;
  link.click();
  setTimeout(() => {
    // For Firefox it is necessary to delay revoking the ObjectURL.
    URL.revokeObjectURL(data);
  }, 100);
};
