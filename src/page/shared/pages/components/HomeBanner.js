import React from "react";

const HomeBanner = () => {
        return (
            <div>
                <div class="home-banner">
              <div class="home-banner-background">
                <img
                  alt="background"
                  src="https://gw.alipayobjects.com/mdn/rms_08e378/afts/img/A*kJM2Q6uPXCAAAAAAAAAAAABkARQnAQ"
                />
                {/* SVG Elements */}
              </div>
              <div class="home-banner-holder">
                <div class="home-banner-content">
                  <div>
                    <div class="home-card-logo">
                      <img
                        alt="Ant Design"
                        src="https://gw.alipayobjects.com/zos/basement_prod/80e7d303-aa05-4c2d-94e9-2255ab5cea6c.svg"
                        class="home-banner-mini"
                      />
                      <img
                        width="490"
                        height="87"
                        alt="Ant Design"
                        src="https://gw.alipayobjects.com/zos/basement_prod/5b153736-dfe3-4a73-9454-68607c8103e4.svg"
                        class="home-banner-normal"
                      />
                      <svg viewBox="0 0 64 64" class="home-card-logo-icon">
                        <g
                          transform="translate(16, 16)"
                          opacity="1"
                          frame="0"
                          cubic="0.25,0.1,0.25,1"
                        >
                          <g transform="matrix(1, 0, 0, 1, 0, 0)">
                            <g transform="rotate(0, 16, 16)">
                              <svg viewBox="0 0 32 32">
                                <image
                                  href="https://gw.alipayobjects.com/zos/basement_prod/dcb1b8f8-becd-4f90-ba32-574260a7b18d.svg"
                                  height="16"
                                  width="16"
                                />
                              </svg>
                            </g>
                          </g>
                        </g>
                      </svg>
                    </div>
                  </div>
                  <p>
                    A design system for enterprise-level products. Create an efficient
                    and enjoyable work experience.
                  </p>
                  <a class="banner-video">
                    <span
                      role="img"
                      aria-label="play-circle"
                      class="anticon anticon-play-circle"
                    >
                      <svg
                        viewBox="64 64 896 896"
                        focusable="false"
                        data-icon="play-circle"
                        width="1em"
                        height="1em"
                        fill="currentColor"
                        aria-hidden="true"
                      >
                        <path d="M512 64C264.6 64 64 264.6 64 512s200.6 448 448 448 448-200.6 448-448S759.4 64 512 64zm144.1 454.9L437.7 677.8a8.02 8.02 0 01-12.7-6.5V353.7a8 8 0 0112.7-6.5L656.1 506a7.9 7.9 0 010 12.9z"></path>
                      </svg>
                    </span>{" "}
                    Play video
                  </a>
                  <div class="home-banner-content-operations">
                    <a href="/docs/react/introduce">
                      <button
                        type="button"
                        class="ant-btn ant-btn-primary ant-btn-round"
                      >
                        Getting Started
                      </button>
                    </a>
                    <a href="/docs/spec/introduce">
                      <button
                        type="button"
                        class="ant-btn ant-btn-round ant-btn-background-ghost"
                      >
                        Design Language
                      </button>
                    </a>
                  </div>
                </div>
              </div>
            </div>
            </div>
          )
    };

export default HomeBanner;
