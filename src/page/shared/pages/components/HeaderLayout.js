import React, { Component } from 'react';
import { Menu } from 'antd';
import { MailOutlined, AppstoreOutlined, SettingOutlined } from '@ant-design/icons';
import logo from '../../assets/img/eDoc.png';
import MenuItem from 'antd/lib/menu/MenuItem';

class HeaderLayout extends Component {

  render() {
    return (
      <header className="site-layout-background" style={{ padding: 0 }}>
        <Menu mode="horizontal">
          <Menu.Item>
            <img src={logo} />
          </Menu.Item>
          <Menu.Item key="mail" icon={<MailOutlined />}>
            Navigation One
          </Menu.Item>
          <Menu.Item key="app" disabled icon={<AppstoreOutlined />}>
            Navigation Two
          </Menu.Item>
          <Menu.Item key="alipay">
            <a href="https://ant.design" target="_blank" rel="noopener noreferrer">
              Navigation Four - Link
            </a>
          </Menu.Item>
      </Menu>
      </header>
    );
  }
}

export default HeaderLayout;