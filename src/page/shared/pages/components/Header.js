import React from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";
import { Row, Col, Badge, Avatar, Menu, Dropdown, Timeline } from "antd";
import {
  MenuUnfoldOutlined,
  PoweroffOutlined,
  MenuFoldOutlined,
  BellFilled,
} from "@ant-design/icons";

const notifications = (
  <div className="notifications-dropdown-container">
    <div className="notifications-dropdown-header"></div>
    <Timeline>
      <Timeline.Item color="green">
        <a className="notification-item" href="">
          <div>
            <p>
              <span>Entidade:</span>Bacela, Lda
            </p>
            <p>
              <span>Assunto:</span>Solicitação de Reset de Senha
            </p>
            <p>
              <span>Data:</span>11/01/2021
            </p>
          </div>
        </a>
      </Timeline.Item>
      <Timeline.Item color="green">
        <a className="notification-item" href="">
          <div>
            <p>
              <span>Entidade:</span>Bacela, Lda
            </p>
            <p>
              <span>Assunto:</span>Solicitação de Reset de Senha
            </p>
            <p>
              <span>Data:</span>11/01/2021
            </p>
          </div>
        </a>
      </Timeline.Item>
      <Timeline.Item color="red">
        <a className="notification-item" href="">
          <div>
            <p>
              <span>Entidade:</span>Bacela, Lda
            </p>
            <p>
              <span>Assunto:</span>Solicitação de Reset de Senha
            </p>
            <p>
              <span>Data:</span>11/01/2021
            </p>
          </div>
        </a>
      </Timeline.Item>
      <Timeline.Item color="gray">
        <a className="notification-item" href="">
          <div>
            <p>
              <span>Entidade:</span>Bacela, Lda
            </p>
            <p>
              <span>Assunto:</span>Solicitação de Reset de Senha
            </p>
            <p>
              <span>Data:</span>11/01/2021
            </p>
          </div>
        </a>
      </Timeline.Item>
    </Timeline>
    <div className="notifications-dropdown-footer">
      <a href="">Ver mais...</a>
    </div>
  </div>
);

const Header = ({ toggler, collapsed }) => {

  return (
    <StyledNav id="header" className="top-nav home-header ">
      <Row>
        <Col span={4}>
          <div className="logo-header">
            <div className="logo-brand">
              <img
                className="img-fluid "
                src={process.env.PUBLIC_URL + "/logo.png"}
                // src={process.env.PUBLIC_URL + "/favicon.ico"}
              />
            </div>
            {/* <div className="brand-slogan">App de testes</div> */}
            <div className="brand-slogan">Appoint24 Heatlh</div>
          </div>
        </Col>
        <Col span={20} className="menu-row">
          <div id="menu-toogler">
            <a href="#" onClick={() => toggler()}>
              {collapsed ? <MenuUnfoldOutlined /> : <MenuFoldOutlined />} Menu
            </a>
          </div>
          <ul className="top-menu">
            <li className="top-menu-item">
              <Badge count={3} offset={[15, 0]}>
                <Dropdown
                  overlayClassName="notifications-dropdown"
                  overlay={notifications}
                  placement={"bottomCenter"}
                  trigger={["click"]}
                >
                  <a
                    href="#"
                    className="ant-dropdown-link"
                    onClick={(e) => e.preventDefault()}
                  >
                    <BellFilled /> Notificações
                  </a>
                </Dropdown>
              </Badge>
            </li>
            <li style={{ marginLeft: "30px" }} className="top-menu-item">
              <Link to="/perfil">
                <Avatar size="small" className="user-avatar">
                  {/* {userAvatar} */}
                </Avatar>
                {/* {userData.nome} */}
              </Link>
            </li>
            <li className="top-menu-item">
              <a
                href="#"
              >
                <PoweroffOutlined /> Sair
              </a>
            </li>
          </ul>
        </Col>
      </Row>
    </StyledNav>
  );
};

const StyledNav = styled.nav`
  background: #ffffff;
  height: 55px;

  .user-avatar {
    margin-right: 5px;
    background-color: #444;
    color: #ffffff;
  }

`;

export default Header;
