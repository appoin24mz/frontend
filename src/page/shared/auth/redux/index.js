import { combineReducers } from "redux";
import API from "../../api";

export const login = ({ username, password }) => async (dispatch) => {
  dispatch({ type: LOGIN_REQUEST });

  try {
    const response = await API.post("/auth/login", { username, password });
    dispatch({ type: LOGIN_SUCCESS, payload: response });
  } catch (error) {
    dispatch({ type: LOGIN_ERROR, payload: error });
  }
};

export const logout = () => (dispatch) => {
  dispatch({ type: LOGOUT });
};

// ---------------------------

export const isAuthProcessing = (state) => state.auth.isProcessing;
export const getAuthError = (state) => {
  return {
    hasError: state.auth.error !== null,
    errorMessage: state.auth.error?.message,
  };
};

export const getLoggedUser = (state) => state.auth.userInfo;
export const isUserLoggedIn = (state) =>
  Object.keys(state.auth.userInfo).length !== 0;

// ---------------------------

const LOGIN_REQUEST = "LOGIN_REQUEST";
const LOGIN_SUCCESS = "LOGIN_SUCCESS";
const LOGIN_ERROR = "LOGIN_ERROR";
const LOGOUT = "LOGOUT";

// ---------------------------

const authorization = (state = {}, action) => {
  const { type, payload } = action;

  switch (type) {
    case LOGIN_SUCCESS:
      return { token: payload.accessToken };
    case LOGOUT:
      return {};
    default:
      return state;
  }
};

const userInfo = (state = {}, action) => {
  const { type, payload } = action;

  switch (type) {
    case LOGIN_SUCCESS:
      return {
        utilizador: payload.username,
	      nome: payload.name,
        funcionarioId: payload.userID,
        phoneNumber: payload.phoneNumber
      };
    case LOGOUT:
      return {};
    default:
      return state;
  }
};

const isProcessing = (state = false, action) => {
  const { type } = action;

  switch (type) {
    case LOGIN_REQUEST:
      return true;
    case LOGIN_SUCCESS:
    case LOGIN_ERROR:
      return false;
    default:
      return state;
  }
};

const error = (state = null, action) => {
  const { type, payload } = action;

  switch (type) {
    case LOGIN_ERROR:
      return { message: payload.message, httpcode: payload.httpcode };
    case LOGIN_REQUEST:
    case LOGIN_SUCCESS:
      return null;
    default:
      return state;
  }
};

export default combineReducers({
  authorization,
  userInfo,
  isProcessing,
  error,
});


//estudar essa cena next time

