import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import styled from "styled-components";
import { Form, Input, Button } from "antd";
import { UserOutlined, LockOutlined } from "@ant-design/icons";
import { Link, useHistory } from "react-router-dom";
// import logo from '%PUBLIC_URL%/logo.png';
// import {
//   login,
//   isAuthProcessing,
//   getAuthError,
//   isUserLoggedIn,
// } from "../redux";

const ClientInit = () => {
  const [isMessageVisible, setMessageVisibility] = useState(false);
  // const isProcessing = useSelector(isAuthProcessing);
  // const isAuthenticated = useSelector(false);
  // const { hasError, errorMessage } = useSelector(getAuthError);

  const dispatch = useDispatch();
  const history = useHistory();

  // useEffect(() => {
  //   if (isAuthenticated) {
  //     history.push("/");
  //   }
  // }, [isAuthenticated, history]);

  const onFinish = (formValues) => {
    // dispatch(
    //   login({ utilizador: formValues.utilizador, senha: formValues.senha })
    // );

    setMessageVisibility(true);
  };

  const forgotPassword = (values) => {
    history.push("/forgetpassword");
  };

  return (
    <StyledDiv className="client-login-block">
      <div>
        <div className="client-login-container">
          <div className="logo">
            <img src={process.env.PUBLIC_URL + "/logo.png"} />
          </div>
          <p className="client-login-app-name">Appoint24 Health</p>
          <div className="buttons">
            <Link
              to="/client-login"
              className="ant-btn ant-btn-block btn-client"
            >
              Entrar no Sistema
            </Link>
            <div className="mt-30 pb-10 aditional-text">Não possui conta?</div>
            <Link
              to="/client-register"
              className="ant-btn ant-btn-block btn-client"
            >
              Registar-se
            </Link>
          </div>
        </div>
      </div>
    </StyledDiv>
  );
};

const StyledDiv = styled.div`
  display: flex;
  height: 100%;
  justify-content: flex-start;

  > div {
    width: 450px;
    height: 100%;
    display: flex;
    background-color: rgba(43, 205, 213, 0.6);
    padding: 15px 80px;
    box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
  }

  .client-login-container {
    align-items: center;
    display: flex;
    flex-direction: column;
    justify-content: center;
  }

  .login-form-button {
    border-radius: 0px;
    height: 40px;
    background: #71473d !important;
    border-color: #71473d !important;
    padding: 2px 15px;
  }

  p.error-message {
    text-align: center;
    color: var(--vermelho-padrao);
    margin: 0;
  }

  form {
    width: 100%;
  }

  .forgot-section {
    margin: 0;
    padding-top: 50px;
  }

  .ant-form-item-control-input-content {
    display: flex;
    justify-content: center;
  }

  .login-form-forgot {
    padding-left: 5px;
  }

  .aditional-text{
    font-weight: 500;
    color: #71473d;
  }

  .ant-input-affix-wrapper {
    line-height: 2.5;
  }
`;

export default ClientInit;
