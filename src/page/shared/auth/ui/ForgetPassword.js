import React from "react";
import styled from "styled-components";
import { Form, Input, Button } from "antd";
import { MailOutlined } from "@ant-design/icons";
import { useHistory, Link } from "react-router-dom";

const ForgetPassword = () => {
  const history = useHistory();

  const onFinish = (values) => {
    console.log("Received values of form: ", values);
    history.push("/app");
  };

  return (
    <StyledDiv>
      <div>
        <div className="logo">e-Doc</div>
        <p className="app-name">Recuperação da senha de Acesso</p>
        <Form
          name="normal_login"
          className="login-form"
          initialValues={{
            remember: true,
          }}
          onFinish={onFinish}
        >
          <Form.Item
            name="username"
            rules={[
              {
                required: true,
                message: "Por favor insira o email usado no âmbito do registo!",
              },
            ]}
          >
            <Input
              prefix={<MailOutlined className="site-form-item-icon" />}
              placeholder="Insira o email usado no âmbito do registo"
            />
          </Form.Item>

          <Form.Item style={{ marginTop: "2rem" }}>
            <Button
              type="primary"
              htmlType="submit"
              className="login-form-button"
            >
              Recuperar a Senha
            </Button>
          </Form.Item>

          <p style={{textAlign:"center"}}>Ou</p>

          <Form.Item>
            <Link to="/" className="login-form-forgot">
              Aceder o Sistema{" "}
              <span className="logo" style={{ fontSize: "16pt" }}>
                e-Doc
              </span>
            </Link>
          </Form.Item>
        </Form>
      </div>
    </StyledDiv>
  );
};

const StyledDiv = styled.div`
  display: flex;
  height: 100%;
  justify-content: center;

  > div {
    width: 400px;
    display: flex;
    flex-direction: column;
    align-items: center;
    align-self: center;
    background-color: white;
    padding: 15px 40px;
    box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
  }

  p.app-name {
    padding-bottom: 1.2rem;
  }

  form {
    width: 100%;
  }

  .ant-form-item-control-input-content {
    display: flex;
    justify-content: center;
  }

  a {
    color: var(--vermelho-padrao);
  }

  .login-form-forgot {
    padding-left: 5px;
  }

  .ant-input-affix-wrapper {
    line-height: 2.5;
  }
`;

export default ForgetPassword;
