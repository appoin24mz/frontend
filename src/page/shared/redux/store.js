import { combineReducers } from "redux";
import { configureStore, getDefaultMiddleware } from "@reduxjs/toolkit";
import {
  persistStore,
  persistReducer,
  FLUSH,
  REHYDRATE,
  PAUSE,
  PERSIST,
  PURGE,
  REGISTER,
} from "redux-persist";
import { CookieStorage } from "redux-persist-cookie-storage";
import Cookies from "cookies-js";
import auth from "../auth/redux";
import users from "../../shared/auth/client/redux";
import storage from 'redux-persist/lib/storage';

const rootReducer = combineReducers({
  auth: persistReducer(
    {
      key: "session",
      whitelist: ["authorization", "userInfo"],
      storage: new CookieStorage(Cookies),
      storage
    },
    auth
  ),
  users,
});

const loggerMiddleware = (store) => (next) => (action) => {
  console.group(action.type);
  console.log("Dispatched", action);

  let result = next(action);

  console.log("New State", store.getState());
  console.groupEnd();

  return result;
};

export default function () {
  let customMiddleware = getDefaultMiddleware({
    serializableCheck: {
      ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
    },
  });

  if (process.env.NODE_ENV !== "production") {
    customMiddleware = customMiddleware.concat(loggerMiddleware);
  }

  const store = configureStore({
    reducer: rootReducer,
    middleware: customMiddleware,
  });

  const persistor = persistStore(store);
  return { store, persistor };
}
