import React, { useState } from "react";

import { Anchor, Drawer, Button } from "antd";

const PageHeader = () => {
  const [visible, setVisible] = useState(false);

  const showDrawer = () => {
    setVisible(true);
  };

  const onClose = () => {
    setVisible(false);
  };

  const becomePartner = () =>{
    
  }

  return (
    <div className="container-fluid">
      <div className="header">
        <div className="home-logo">
          <img
            className="img-fluid "
            src={process.env.PUBLIC_URL + "/logo.png"}
          />
          <a href="#">Appoint24 Health</a>
        </div>
        <div className="mobileHidden">
          <Anchor targetOffset="65">
            <Anchor.Link href="#hero" title="Inicio" />
            <Anchor.Link href="#about" title="Sobre Nós" />
            <Anchor.Link href="#pricing" title="Produtos" />
            <Anchor.Link href="#faq" title="Perguntas Frequentes" />
            <Anchor.Link href="#contact" title="Contactos" />
            <Anchor.Link onClick={()=>becomePartner()} title="Torne-se Parceiro" />
          </Anchor>
        </div>
        <div className="mobileVisible">
          <Button type="primary" onClick={showDrawer}>
            <i className="fas fa-bars"></i>
          </Button>
          <Drawer
            placement="right"
            closable={false}
            onClose={onClose}
            visible={visible}
          >
            <Anchor targetOffset="65">
              <Anchor.Link href="#hero" title="Inicio" />
              <Anchor.Link href="#about" title="Sobre Nós" />
              <Anchor.Link href="#pricing" title="Produtos" />
              <Anchor.Link href="#faq" title="Perguntas Frequentes" />
              <Anchor.Link href="#contact" title="Contactos" />
            </Anchor>
          </Drawer>
        </div>
      </div>
    </div>
  );
};

export default PageHeader;
