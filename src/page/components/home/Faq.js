import React from 'react';

import { Collapse, Button, Anchor } from 'antd';

const { Panel } = Collapse;

const PageFaq = () => {
  return(
    <div id="faq" className="block faqBlock">
      <div className="container-fluid">
        <div className="titleHolder">
          <h2>Perguntas Frequentes</h2>
          <p>Temos abaixo algumas questões frequentes sobre os produtos da Appoint24</p>
        </div>
        <Collapse defaultActiveKey={['1']}>
          <Panel header="Como Reservar Medicamentos online?" key="1">
            <p>Ut per tempor latine fuisset, cu quod posidonium vix. Mei cu erant bonorum, te ius vide maiorum hendrerit. In alii instructior vix, vis et elit maluisset, usu recusabo atomorum ut. Nibh diceret dolores vix et, id omnis adhuc maiestatis vim, ei mel legendos mnesarchum argumentum. Semper nusquam urbanitas sea te.</p>
          </Panel>
          <Panel header="Como cancelar uma Consulta Médica?" key="2">
            <p>Postea ceteros corrumpit ius te, eos epicuri intellegebat consequuntur et. Sint quot suscipiantur ea nam. Nam pericula evertitur ut, per et quod nostro, autem augue id has. Virtute epicurei quo te, pri et novum essent senserit.</p>
          </Panel>
          <Panel header="Como Aderir ao Plano de Saúde a partir da Appoint24?" key="3">
            <p>Eu veritus ancillae suavitate per, cum in appellantur efficiantur. Eum cu clita ponderum lobortis, usu dicat exerci et, eam alii oblique interesset ea. Suas quidam volumus id eam, id populo euripidis temporibus pri. At eum quas putent iriure, fugit tritani sed ad. Per ad magna possim aliquam, est aeque exerci verear an, qui cu modus audire detraxit. Duo ne nostro rationibus, nam mutat omittam evertitur ad, meliore gubergren voluptatum at mel.</p>
          </Panel>
          <Panel header="Como registar a minha Farmácia na Appoint24 para que possa ser encontrada na busca pelos Medicamentos?" key="4">
            <p>Mentitum offendit appareat nam ex, mea timeam nonumes pertinacia ne. Autem altera an vix, cu soluta aliquid pro, ne sit natum neglegentur. Ea ridens iudicabit eam, in dico appetere mediocrem nec. Sea idque consetetur no. Sonet minimum ex eam, vis an semper consequuntur definitionem. Vel legimus nostrum hendrerit eu, ea velit facete nec.</p>
          </Panel>
          <Panel header="Como Registar minha Clinica ou meu Consultório na Appoint24 para que possa ser encontrada na busca pelos serviços de saúde?" key="5">
            <p>Usu dolorem ceteros te. Veri exerci ne vix, modo ignota an qui. Ne oblique antiopam quo. Ex quem saepe cum, temporibus comprehensam qui at. Aliquip habemus fierent qui at. No facete omnesque argumentum sea, est tale error nihil ad.</p>
          </Panel>
        </Collapse>
        <div className="quickSupport">
          <h3>Precisa de uma Ajuda Rapida?</h3>
          <p>Envie um email com a questão e será respondido em menos de 3 horas!</p>
          <Anchor className="internal-anchor">
            <Anchor.Link className="ant-btn ant-btn-primary ant-btn-lg" href="#contact" title="Enviar email com a Questão" />
          </Anchor> 
          {/* <Button type="primary" size="large"><i className="fas fa-envelope"></i> Enviar email com a Questão</Button> */}
        </div>
      </div>
    </div>  
  );
}

export default PageFaq;