import React from 'react';

import { Row, Col } from 'antd';

const items = [
  {
    key: '1',
    icon: <i className="fas fa-bullseye"></i>,
    title: 'Missão',
    content: 'cu nostro dissentias consectetuer mel. Ut admodum conceptam mei, cu eam tation fabulas abhorreant. His ex mandamus.',
  },
  {
    key: '2',
    icon: <i className="fas fa-eye"></i>,
    title: 'Visão',
    content: 'cu nostro dissentias consectetuer mel. Ut admodum conceptam mei, cu eam tation fabulas abhorreant. His ex mandamus.',
  },
  {
    key: '3',
    icon: <i className="fas fa-info-circle"></i>,
    title: 'Valores',
    content: 'cu nostro dissentias consectetuer mel. Ut admodum conceptam mei, cu eam tation fabulas abhorreant. His ex mandamus.',
  },
]

const PageAbout = () => {
  return (
    <div id="about" className="block aboutBlock">
      <div className="container-fluid">
        <div className="titleHolder">
          <h2>Sobre Nós</h2>
          <p>Empresa 100% Moçambicana</p>
        </div>
        <div className="contentHolder">
          <p>
            A <span style={{ color: "#735348", fontWeight: 500 }}>Appoint24, LDA</span> é uma empresa Moçambicana de direito privado que opera no mercado Moçambicano, 
            especializada nas áreas de Consultoria de Sistemas de Informação/Tecnologias de Informação, 
            Desenho e Desenvolvimento de Soluções Sustentáveis com foco no cidadão 
            (explorando no máximo as ICT4D - Information and Communications Technology for Development 
            [Tecnologias de Informação e Comunicação para o Desenvolvimento]), entre outos serviços. 
          </p>
        </div>
        <Row gutter={[16, 16]}>   
          {items.map(item => {
            return (
              <Col md={{ span: 8 }} key={item.key}>
                <div className="content">
                  <div className="icon">
                    {item.icon}
                  </div>
                  <h3>{item.title}</h3>
                  <p>{item.content}</p>
                </div>
              </Col>
            );
          })}
        </Row>
      </div>
    </div>
  );
}

export default PageAbout;