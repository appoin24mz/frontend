import React from "react";
import { Card, Col, Row } from "antd";
import { ReactComponent as HealthIcon } from "../../assets/images/health.svg";
import { ReactComponent as ShieldIcon } from "../../assets/images/shield.svg";
import { ReactComponent as PillsIcon } from "../../assets/images/pills.svg";
import { Link } from "react-router-dom";

const PageHero = () => {
  return (
    <div id="hero" className="heroBlock">
      <div className="container">
        <div className="content">
          <Row gutter={12}>
            <Col
              xs={{ span: 22, offset: 1 }}
              sm={{ span: 12, offset: 6 }}
              md={{ span: 10, offset: 7 }}
              lg={{ span: 6, offset: 3 }}
              xl={{ span: 6, offset: 3 }}
              xxl={{ span: 4, offset: 6 }}
            >
              <Link to="/client-init" className="app-card-link">
                <Card hoverable={true} className="app-card" bordered={false}>
                  <div className="icon-image">
                    <PillsIcon />
                  </div>
                  <div className="title">Reserva de Medicamentos</div>
                </Card>
              </Link>
            </Col>

            <Col
              xs={{ span: 22, offset: 1 }}
              sm={{ span: 12, offset: 6 }}
              md={{ span: 10, offset: 7 }}
              lg={{ span: 6, offset: 0 }}
              xl={{ span: 6, offset: 0 }}
              xxl={{ span: 4, offset: 0 }}
            >
              <Link to="/client-init" className="app-card-link">
                <Card hoverable={true} className="app-card" bordered={false}>
                  <div className="icon-image">
                    <HealthIcon />
                  </div>
                  <div className="title">Consultas Médicas</div>
                </Card>
              </Link>
            </Col>

            <Col
              xs={{ span: 22, offset: 1 }}
              sm={{ span: 12, offset: 6 }}
              md={{ span: 10, offset: 7 }}
              lg={{ span: 6, offset: 0 }}
              xl={{ span: 6, offset: 0 }}
              xxl={{ span: 4, offset: 0 }}
            >
              <Link to="/client-init" className="app-card-link">
                <Card hoverable={true} className="app-card" bordered={false}>
                  <div className="icon-image">
                    <ShieldIcon />
                  </div>
                  <div className="title">Adesão ao Plano de Saúde</div>
                </Card>
              </Link>
            </Col>
          </Row>
        </div>
      </div>
    </div>
  );
};

export default PageHero;
