import React from "react";

import { Row, Col, Button } from "antd";
import image1 from "../../assets/images/1.png";
import image2 from "../../assets/images/2.png";
import image3 from "../../assets/images/3.png";

const PageApplication = () => {
  return (
    <div id="pricing" className="block pricingBlock bgGray">
      <div className="container-fluid">
        <div className="titleHolder">
          <h2>Nosso Produto</h2>
          <p>
            Baixe para mais detalhes o{" "}
            <span style={{ color: "#735348", fontWeight: 600 }}>
              Appoint24 Health
            </span>
          </p>
        </div>
        <Row gutter={[16, 16]} className="app-preview">
          <Col
            className="sec-col"
            xs={{ span: 8, offset: 0 }}
            sm={{ span: 7, offset: 1 }}
            md={{ span: 7, offset: 1 }}
            lg={{ span: 7, offset: 1 }}
            xl={{ span: 5, offset: 4 }}
            xxl={{ span: 5, offset: 4 }}
          >
            <img className="img-fluid" src={image3} />
          </Col>

          <Col
            xs={{ span: 8 }}
            sm={{ span: 8 }}
            md={{ span: 8 }}
            lg={{ span: 8 }}
            xl={{ span: 6 }}
            xxl={{ span: 6 }}
          >
            <img className="img-fluid main-img" src={image1} />
          </Col>

          <Col
            className="sec-col"
            xs={{ span: 8 }}
            sm={{ span: 7 }}
            md={{ span: 7 }}
            lg={{ span: 7 }}
            xl={{ span: 5 }}
            xxl={{ span: 5 }}
          >
            <img className="img-fluid" src={image2} />
          </Col>
        </Row>
        <div className="download-app-buttons">
          <a href="#" target="_blank" className="download-button ant-btn">
            <div className="icon">
              <i className="fab fa-apple"></i>
            </div>
            <div className="info">
              <div className="into-text">Disponível na</div>
              <div className="download-store">AppStore</div>
            </div>
          </a>

          <a href="#" target="_blank" className="download-button ant-btn">
            <div className="icon">
              <i className="fab fa-google-play"></i>
            </div>
            <div className="info">
              <div className="into-text">Disponível na</div>
              <div className="download-store">PlayStore</div>
            </div>
          </a>
        </div>
      </div>
    </div>
  );
};

export default PageApplication;
