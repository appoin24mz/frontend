import React from "react";

import { Form, Input, Button, Checkbox } from "antd";
import { SendOutlined } from "@ant-design/icons";
const { TextArea } = Input;

const PageContact = () => {
  return (
    <div id="contact" className="block contactBlock bgGray">
      <div className="container-fluid">
        <div className="titleHolder">
          <h2>Entre em Contacto</h2>
          <p>Estamos sempre disponiveis para esclarecer as suas duvidas ...</p>
        </div>
        <Form
          name="normal_login"
          className="login-form"
          initialValues={{ remember: true }}
        >
          <Form.Item
            name="fullname"
            rules={[
              {
                required: true,
                message: "Por favor informe o seu Nome Completo!",
              },
            ]}
          >
            <Input placeholder="Nome Completo" />
          </Form.Item>
          <Form.Item
            name="email"
            rules={[
              {
                type: "email",
                message: "Por favor informe um email valido!",
              },
              {
                required: true,
                message: "Por favor informe o seu email!",
              },
            ]}
          >
            <Input placeholder="Email" />
          </Form.Item>
          <Form.Item name="telephone">
            <Input placeholder="Telefone" />
          </Form.Item>
          <Form.Item name="subject">
            <Input placeholder="Assunto" />
          </Form.Item>
          <Form.Item name="message">
            <TextArea placeholder="Mensagem" />
          </Form.Item>
          <Form.Item>
            <Form.Item
              name="remember"
              valuePropName="checked"
              noStyle
              rules={[
                {
                  validator: (_, value) =>
                    value
                      ? Promise.resolve()
                      : Promise.reject("Should accept agreement"),
                },
              ]}
            >
              <Checkbox>Eu concordo com os Termos e Condições.</Checkbox>
            </Form.Item>
          </Form.Item>
          <Form.Item>
            <Button
              type="primary"
              htmlType="submit"
              className="login-form-button"
            >
              Enviar
              <i className="fas fa-paper-plane" style={{ paddingLeft: "10px" }} ></i>
            </Button>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
};

export default PageContact;
