import React from 'react';

import { Button, Modal } from 'antd';

class PageWorks extends React.Component {
  state = { visible: false };

  showModal = () => {
    this.setState({
      visible: true,
    });
  };

  handleCancel = e => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };

  render() {
    return (
      <div id="works" className="block worksBlock">
        <div className="container-fluid">
          <div className="titleHolder">
            <h2>Como a Solução Funciona</h2>
            <p>Veja Rapidamente o video e saiba como tirar maior proveito da solução <span style={{ color: "", fontWeight: 600 }}>Appoint24 Health</span></p>
          </div>
          <div className="contentHolder">
            <Button size="large" onClick={this.showModal}><i className="fas fa-play"></i></Button>
          </div>
          <Modal
            title="Como usar a Appoint24 Health"
            visible={this.state.visible}
            onCancel={this.handleCancel}
            footer={null}
            destroyOnClose = {true}
          >
            <iframe title="5 Dicas para ter uma Vida Saudável | IMEB" width="100%" height="350" src="https://www.youtube.com/embed/w6lqLoSOcLI"></iframe>
          </Modal>
        </div>
      </div>
    );
  }
}

export default PageWorks;