import React from "react";
import "./Page.css";
import "antd/dist/antd.css";

import PageHeader from './components/common/Header';
import PageFooter from './components/common/Footer';
import PageHome from './views/Home';

import { Layout } from 'antd';
const { Header, Content, Footer } = Layout;

const Page = () => {
  return (
    <Layout className="mainLayout">
      <Header>
        <PageHeader />
      </Header>
      <Content>
        <PageHome />
      </Content>
      <Footer>
        <PageFooter />
      </Footer>
    </Layout>
  );
};

export default Page;
