import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import Init from "./Init";
import { Provider } from "react-redux";
import initStore from "./app/shared/redux/store";
import { PersistGate } from "redux-persist/integration/react";
import { BrowserRouter as Router } from "react-router-dom";

const { store, persistor } = initStore();

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <Router>
          <Init />
        </Router>
      </PersistGate>
    </Provider>
  </React.StrictMode>,
  document.getElementById("root")
);
