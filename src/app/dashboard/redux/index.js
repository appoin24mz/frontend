import { combineReducers } from "redux";
import API from "../../shared/api";

//Actions Creators

//Get Request
export const getData = () => async (dispatch) => {
  dispatch({ type: GET_DASHBOARD_REQUEST });

  try {
    const response = await API.get("/reporting/dashboard/");
    dispatch({ type: GET_DASHBOARD_SUCCESS, payload: response });
  } catch (error) {
    dispatch({ type: GET_DASHBOARD_ERROR, payload: error });
  }
};

//Get Request
export const getFarmacias = () => async (dispatch) => {
  dispatch({ type: GET_FARMACIAS_REQUEST });

  try {
    const response = await API.get("/pharmacies/");
    dispatch({ type: GET_FARMACIAS_SUCCESS, payload: response });
  } catch (error) {
    dispatch({ type: GET_FARMACIAS_ERROR, payload: error });
  }
};

export const getMedicamentos = () => async (dispatch) => {
  dispatch({ type: GET_MEDICAMENTOS_REQUEST });

  try {
    const response = await API.get("/medicines/");
    dispatch({ type: GET_MEDICAMENTOS_SUCCESS, payload: response });
  } catch (error) {
    dispatch({ type: GET_MEDICAMENTOS_ERROR, payload: error });
  }
};


//Get by ID Request
export const getDataById = (code) => async (dispatch) => {
  dispatch({ type: GET_DASHBOARD_BY_ID_REQUEST });

  try {
    const response = await API.get(`/reporting/dashboard/${code}`);
    dispatch({ type: GET_DASHBOARD_BY_ID_SUCCESS, payload: response });
  } catch (error) {
    dispatch({ type: GET_DASHBOARD_BY_ID_ERROR, payload: error });
  }
};

//Selectors
export const getAllDashboardInfo = (state) => state.dashboard.allDashboardInfo;
export const getAllFarmacias = (state) => state.dashboard.allFarmacias;
export const getAllMedicamentos = (state) => state.dashboard.allMedicamentos;
export const getDashboardInfo = (state) => state.dashboard.dashboardInfo;
export const getIsGetProcessing = (state) => state.dashboard.isGetProcessing;
export const getIsGetFarmaciaProcessing = (state) =>
  state.dashboard.isGetFarmaciaProcessing;
  export const getIsGetMedicamentoProcessing = (state) =>
  state.dashboard.isGetMedicamentosProcessing;
export const getIsGetByIdProcessing = (state) =>
  state.dashboard.isGetByIdProcessing;
export const getError = (state) => state.dashboard.errorGet;
export const getMedicamentosError = (state) => state.dashboard.errorGetMedicamentos;
export const getByIdError = (state) => state.dashboard.errorGetById;

// ---------------------------
//Accoes
//Requests
const GET_DASHBOARD_REQUEST = "GET_DASHBOARD_REQUEST";
const GET_FARMACIAS_REQUEST = "GET_FARMACIAS_REQUEST";
const GET_MEDICAMENTOS_REQUEST = "GET_MEDICAMENTOS_REQUEST";
const GET_DASHBOARD_BY_ID_REQUEST = "GET_DASHBOARD_BY_ID_REQUEST";
//Erros
const GET_DASHBOARD_ERROR = "GET_DASHBOARD_ERROR";
const GET_FARMACIAS_ERROR = "GET_FARMACIAS_ERROR";
const GET_MEDICAMENTOS_ERROR = "GET_MEDICAMENTOS_ERROR";
const GET_DASHBOARD_BY_ID_ERROR = "GET_DASHBOARD_BY_ID_ERROR";

//Sucesso
const GET_DASHBOARD_SUCCESS = "GET_DASHBOARD_SUCCESS";
const GET_FARMACIAS_SUCCESS = "GET_FARMACIAS_SUCCESS";
const GET_MEDICAMENTOS_SUCCESS = "GET_MEDICAMENTOS_SUCCESS";
const GET_DASHBOARD_BY_ID_SUCCESS = "GET_DASHBOARD_BY_ID_SUCCESS";

// ---------------------------

//Reducers -> Mensagens de Erro

const errorGet = (state = null, action) => {
  const { type, payload } = action;

  switch (type) {
    case GET_DASHBOARD_ERROR:
    case GET_FARMACIAS_ERROR:
    case GET_MEDICAMENTOS_ERROR:
      return { message: payload.message, httpcode: payload.httpcode };
    default:
      return state;
  }
};

const errorGetMedicamentos = (state = null, action) => {
  const { type, payload } = action;

  switch (type) {
    case GET_MEDICAMENTOS_ERROR:
      return { message: payload.message, httpcode: payload.httpcode };
    default:
      return state;
  }
};

const errorGetById = (state = null, action) => {
  const { type, payload } = action;

  switch (type) {
    case GET_DASHBOARD_BY_ID_ERROR:
      return { message: payload.message, httpcode: payload.httpcode };
    default:
      return state;
  }
};

const isGetProcessing = (state = { status: false, message: null }, action) => {
  const { type } = action;

  switch (type) {
    case GET_DASHBOARD_REQUEST:
      return {
        status: true,
        message: null,
      };
    case GET_DASHBOARD_BY_ID_REQUEST:
      return {
        status: false,
        message: null,
      };
    case GET_DASHBOARD_SUCCESS:
      return {
        status: false,
        message: "Sucesso",
      };
    case GET_DASHBOARD_ERROR:
      return {
        status: false,
        message: "Erro",
      };
    default:
      return state;
  }
};

const isGetFarmaciaProcessing = (
  state = { status: false, message: null },
  action
) => {
  const { type } = action;

  switch (type) {
    case GET_FARMACIAS_REQUEST:
      return {
        status: true,
        message: null,
      };
    case GET_FARMACIAS_SUCCESS:
      return {
        status: false,
        message: "Sucesso",
      };
    case GET_FARMACIAS_ERROR:
      return {
        status: false,
        message: "Erro",
      };
    default:
      return state;
  }
};

const isGetMedicamentosProcessing = (
  state = { status: false, message: null },
  action
) => {
  const { type } = action;

  switch (type) {
    case GET_MEDICAMENTOS_REQUEST:
      return {
        status: true,
        message: null,
      };
    case GET_MEDICAMENTOS_SUCCESS:
      return {
        status: false,
        message: "Sucesso",
      };
    case GET_MEDICAMENTOS_ERROR:
      return {
        status: false,
        message: "Erro",
      };
    default:
      return state;
  }
};

const isGetByIdProcessing = (
  state = { status: false, message: null },
  action
) => {
  const { type } = action;

  switch (type) {
    case GET_DASHBOARD_BY_ID_REQUEST:
      return {
        status: true,
        message: null,
      };
    case GET_DASHBOARD_REQUEST:
      return {
        status: false,
        message: null,
      };
    case GET_DASHBOARD_BY_ID_SUCCESS:
      return {
        status: false,
        message: "Sucesso",
      };
    case GET_DASHBOARD_BY_ID_ERROR:
      return {
        status: false,
        message: "Erro",
      };
    default:
      return state;
  }
};

//Reducers -> Manipulacao do estado
const allDashboardInfo = (state = { amountsPerMonth: [] }, action) => {
  const { type, payload } = action;

  switch (type) {
    case GET_DASHBOARD_SUCCESS:
      return {
        amountsPerMonth: payload.amountsPerMonth,
        totalAmountToday: payload.totalAmountToday,
        totalCustomers: payload.totalCustomers,
        currentlyLoggedUsers: payload.currentlyLoggedUsers,
        totalAverageOperationsPerDay: payload.totalAverageOperationsPerDay,
      };
    default:
      return state;
  }
};

const allFarmacias = (state = { data: [] }, action) => {
  const { type, payload } = action;

  switch (type) {
    case GET_FARMACIAS_SUCCESS:
      return {
        currentPage: payload.currentPage,
        totalElements: payload.totalElements,
        totalPages: payload.totalPages,
        data: payload.list,
      };
    default:
      return state;
  }
};

const allMedicamentos = (state = { data: [] }, action) => {
  const { type, payload } = action;

  switch (type) {
    case GET_MEDICAMENTOS_SUCCESS:
      return {
        currentPage: payload.currentPage,
        totalElements: payload.totalElements,
        totalPages: payload.totalPages,
        data: payload.list,
      };
    default:
      return state;
  }
};


//Reducers -> Manipulacao do estado
const dashboardInfo = (state = { amountsPerMonth: [] }, action) => {
  const { type, payload } = action;

  switch (type) {
    case GET_DASHBOARD_BY_ID_SUCCESS:
      return {
        amountsPerMonth: payload.amountsPerMonth,
        totalAmountToday: payload.totalAmountToday,
        totalCustomers: payload.totalCustomers,
        currentlyLoggedUsers: payload.currentlyLoggedUsers,
        totalAverageOperationsPerDay: payload.totalAverageOperationsPerDay,
      };
    default:
      return state;
  }
};

//Export All Reducers
export default combineReducers({
  allDashboardInfo,
  dashboardInfo,
  allFarmacias,
  allMedicamentos,
  isGetMedicamentosProcessing,
  isGetFarmaciaProcessing,
  errorGet,
  errorGetMedicamentos,
  errorGetById,
  isGetProcessing,
  isGetByIdProcessing,
});
