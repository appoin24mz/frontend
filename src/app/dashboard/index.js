import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Row, Col, Form, Table, notification, Skeleton } from "antd";
import {
  ShopOutlined,
  DisconnectOutlined,
  FileSearchOutlined,
} from "@ant-design/icons";
import { Link } from "react-router-dom";
import { useHistory } from "react-router-dom";
import NumberFormat from "react-number-format";
import {
  getAllFarmacias,
  getFarmacias,
  getError,
  getIsGetFarmaciaProcessing,
  getMedicamentos,
  getAllMedicamentos,
  getMedicamentosError,
  getIsGetMedicamentoProcessing,
} from "./redux";

const Dashboard = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  //Local State
  const [dashboardInfo, setDashboardInfo] = useState();
  const [farmacias, setFarmacias] = useState();
  const [totalFarmaciasIrregulares, setTotalFarmaciasIrregulares] = useState();
  const [totalMedicamentos, setTotalMedicamentos] = useState();

  //Store State
  const allFarmacias = useSelector(getAllFarmacias);
  const allMedicamentos = useSelector(getAllMedicamentos);

  //Processing status
  const isProcessing = useSelector(getIsGetFarmaciaProcessing);
  const isGetMedicamentosProcessing = useSelector(
    getIsGetMedicamentoProcessing
  );

  //Error status
  const errorGet = useSelector(getError);
  const errorGetMedicamentos = useSelector(getMedicamentosError);

  //Local Variables
  const SUCESSO = "sucesso";

  //useEffect Operations
  useEffect(() => {
    dispatch(getFarmacias({ page: "", searchTerm: "", size: "" }));
    dispatch(getMedicamentos({ page: "", searchTerm: "", size: "" }));
  }, []);

  //Get Processing
  useEffect(() => {
    if (allFarmacias !== undefined) {
      setFarmacias(generateIrregularPharmacies(allFarmacias.data));
      // setFarmaciasIrregulares(generateIrregularPharmacies(allFarmacias.data));
    }
  }, [isProcessing, errorGet, allFarmacias]);

  useEffect(() => {
    if (allMedicamentos !== undefined) {
      setTotalMedicamentos(allMedicamentos.totalElements);
    }
  }, [isGetMedicamentosProcessing, errorGetMedicamentos, allMedicamentos]);

  const handleDetails = (record) => {
    //Correr Modal com os detalhes da operação
    console.log("Record: ", record);
  };

  const columns = [
    {
      title: "Farmácias",
      dataIndex: "farmacia",
      key: "farmacia",
      width: "25%",
      sorter: (a, b) => a.farmacia.length - b.farmacia.length,
      ellipsis: true,
    },
    {
      title: "Alvará",
      dataIndex: "alvara",
      key: "alvara",
      width: "15%",
      sorter: (a, b) => a.alvara.length - b.alvara.length,
      ellipsis: true,
    },
    {
      title: "Email",
      dataIndex: "email",
      key: "email",
      width: "20%",
      sorter: (a, b) => a.email.length - b.email.length,
      ellipsis: true,
    },
    {
      title: "Telefone",
      dataIndex: "phone_number",
      key: "phone_number",
      width: "10%",
      sorter: (a, b) => a.phone_number.length - b.phone_number.length,
      ellipsis: true,
    },
    {
      title: "Endereço",
      dataIndex: "address",
      key: "address",
      width: "20%",
      sorter: (a, b) => a.address.length - b.address.length,
      ellipsis: true,
    },
    {
      title: "",
      dataIndex: "accoes",
      key: "accoes",
      render: (text, record) => {
        //console.log(record.key);
        return (
          <>
            <a href="#" onClick={() => handleDetails(record)}>
              <FileSearchOutlined /> Detalhes
            </a>
          </>
        );
      },
    },
  ];

  const generateData = (data) => {
    const generatedData =
      data &&
      data.map((item) => {
        return {
          key: item.id,
          farmacia: item.name,
          alvara: item.alvara,
          email: item.contact.email,
          phone_number: item.contact.phoneNumber,
          address: item.location.address,
          accoes: "",
        };
      });

    return generatedData;
  };

  const generateIrregularPharmacies = (data) => {
    let newData = data.filter(el => el.alvara.toLowerCase() === 'N/A'.toLocaleLowerCase());
    setTotalFarmaciasIrregulares(newData.length);
    console.log('NEW DATA: ',newData.length)
    console.log('DATA: ',data)
    return generateData(newData);
  };

  const openNotificationWithIcon = (tipo, titulo, mensagem) => {
    notification[tipo]({
      message: titulo,
      description: mensagem,
    });
  };

  return (
    <Row style={{ margin: "60px" }}>
      <Skeleton loading={isProcessing.status} active>
        <Col span={24} style={{ marginBottom: "10px" }}>
          <Row gutter={[16, 16]}>
            <Col className="gutter-row" xs={24} sm={24} md={12} lg={6} xl={6}>
              <div className="dash-card clientes">
                <Link to="/app/medicamentos">
                  <div className="dash-valor">{totalMedicamentos}</div>
                  <div className="dash-designacao">Total de Medicamentos</div>
                  <div className="dash-icon">
                    <DisconnectOutlined className="ccc-icon" />
                  </div>
                </Link>
              </div>
            </Col>

            <Col className="gutter-row" xs={24} sm={24} md={12} lg={6} xl={6}>
              <div className="dash-card transacoes">
                <Link to="/app/farmacias">
                  <div className="dash-valor">{allFarmacias.totalElements}</div>
                  <div className="dash-designacao">Total de Farmácias</div>
                  <div className="dash-icon">
                    <ShopOutlined className="ccc-icon" />
                  </div>
                </Link>
              </div>
            </Col>

            <Col className="gutter-row" xs={24} sm={24} md={12} lg={6} xl={6}>
              <div className="dash-card utilizadores">
                <Link to="/app/farmacias">
                  <div className="dash-valor">{totalFarmaciasIrregulares}</div>
                  <div className="dash-designacao">
                    Farmácias em Sit. Irregular
                  </div>
                  <div className="dash-icon">
                    <ShopOutlined className="ccc-icon" />
                  </div>
                </Link>
              </div>
            </Col>

            <Col className="gutter-row" xs={24} sm={24} md={12} lg={6} xl={6}>
              <div className="dash-card entradas">
                <Link to="/app/farmacias">
                  <div className="dash-valor">
                    <NumberFormat
                      value={allFarmacias.totalElements}
                      displayType={"text"}
                      thousandSeparator={true}
                    />
                  </div>
                  <div className="dash-designacao">
                    Farmácias Registadas no Mês
                  </div>
                  <div className="dash-icon">
                    <ShopOutlined />
                  </div>
                </Link>
              </div>
            </Col>
          </Row>
        </Col>

        <Col span={24}>
          <div className="dash-chart">
            <Row>
              <Col span={18}>
                <div className="chart-title">
                  Farmácias em Situação Irregular
                </div>
              </Col>
              <Col span={6}>
                <Form.Item
                  name="periodo"
                  rules={[
                    { required: false, message: "Por selecione o periodo!" },
                  ]}
                ></Form.Item>
              </Col>
            </Row>

            <Row>
              <Col span={24}>
                <Table
                  columns={columns}
                  pagination={{
                    pageSize: 10,
                    total: allFarmacias.totalElements,
                    defaultPageSize: 10,
                    defaultCurrent: 1,
                  }}
                  dataSource={farmacias}
                  size="small"
                  loading={isProcessing.status}
                />
              </Col>
            </Row>
          </div>
        </Col>
      </Skeleton>
    </Row>
  );
};

export default Dashboard;
