import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import styled from "styled-components";
import { Form, Input, Button } from "antd";
import { UserOutlined, LockOutlined } from "@ant-design/icons";
import { useHistory } from "react-router-dom";
// import logo from '%PUBLIC_URL%/logo.png';
import {
  login,
  isAuthProcessing,
  getAuthError,
  isUserLoggedIn,
} from "../redux";

const Login = () => {
  const [isMessageVisible, setMessageVisibility] = useState(false);
  const isProcessing = useSelector(isAuthProcessing);
  const isAuthenticated = useSelector(false);
  const { hasError, errorMessage } = useSelector(getAuthError);

  const dispatch = useDispatch();
  const history = useHistory();

  useEffect(() => {
    if (isAuthenticated) {
      history.push("/dashboard");
    }
  }, [isAuthenticated, history]);

  const onFinish = (formValues) => {
    dispatch(
      login({ username: formValues.utilizador, senha: formValues.senha })
    );

    setMessageVisibility(true);
  };

  const forgotPassword = (values) => {
    history.push("/forgetpassword");
  };

  return (
    <StyledDiv>
      <div>
        <div className="logo"><img src={process.env.PUBLIC_URL+"/logo.png"} /></div>
        <p className="app-name">Card Cell Cash</p>
        <Form
          name="normal_login"
          className="login-form"
          onFieldsChange={() => {
            setMessageVisibility(false);
          }}
          onFinish={onFinish}
        >
          <Form.Item
            name="utilizador"
            rules={[
              {
                required: true,
                message: "Por favor insira teu nome de utilizador!",
              },
            ]}
          >
            <Input
              prefix={<UserOutlined className="site-form-item-icon" />}
              placeholder="Nome de Utilizador"
              size="small"
            />
          </Form.Item>
          <Form.Item
            name="senha"
            rules={[
              {
                required: true,
                message: "Por favor insira a sua senha!",
              },
            ]}
          >
            <Input
              prefix={<LockOutlined className="site-form-item-icon" />}
              type="password"
              placeholder="Senha de Acesso"
              size="small"
            />
          </Form.Item>

          {isMessageVisible && hasError && (
            <p className="error-message">{errorMessage}</p>
          )}

          <Form.Item style={{ marginTop: "2rem" }}>
            <Button
              type="primary"
              htmlType="submit"
              className="login-form-button"
              loading={isProcessing}
            >
              Aceder minha Conta
            </Button>
          </Form.Item>

          <Form.Item className="forgot-section">
            Esqueceu senha?
            <a className="login-form-forgot" onClick={forgotPassword} href="">
              Recupere aqui
            </a>
          </Form.Item>
        </Form>
      </div>
    </StyledDiv>
  );
};

const StyledDiv = styled.div`
  display: flex;
  height: 100%;
  justify-content: center;

  > div {
    width: 400px;
    display: flex;
    flex-direction: column;
    align-items: center;
    align-self: center;
    background-color: white;
    padding: 15px 40px;
    box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
  }

  p.error-message {
    text-align: center;
    color: var(--vermelho-padrao);
    margin: 0;
  }

  form {
    width: 100%;
  }

  .forgot-section{
    margin: 0;
    padding-top: 50px;
  }

  .ant-form-item-control-input-content {
    display: flex;
    justify-content: center;
  }

  a {
    color: var(--app-vermelho);
    text-decoration: underline;
  }

  .login-form-forgot {
    padding-left: 5px;
  }

  .ant-input-affix-wrapper {
    line-height: 2.5;
  }
`;

export default Login;
