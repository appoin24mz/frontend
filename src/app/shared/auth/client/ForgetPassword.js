import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import styled from "styled-components";
import { Form, Input, Button } from "antd";
import { UserOutlined, LockOutlined } from "@ant-design/icons";
import { Link, useHistory } from "react-router-dom";
import {
  updateRow,
  getIsUpdateProcessing,
  getUpdateError
} from "./redux";

const ForgotPassword = () => {
  const [isMessageVisible, setMessageVisibility] = useState(false);
  const isProcessing = useSelector(isAuthProcessing);
  const isAuthenticated = useSelector(isUserLoggedIn);
  const { hasError, errorMessage } = useSelector(getAuthError);

  const dispatch = useDispatch();
  const history = useHistory();

  useEffect(() => {
    if (isAuthenticated) {
      //history.push("/system/home");
    }
  }, [isAuthenticated, history]);

  const onFinish = (formValues) => {
    dispatch(
      login({ username: formValues.utilizador, password: formValues.senha })
    );

    setMessageVisibility(true);
  };

  const forgotPassword = (values) => {
    history.push("/forgetpassword");
  };

  return (
    <StyledDiv className="client-login-block">
      <div>
        <div className="client-login-container">
          <div className="logo">
            <img src={process.env.PUBLIC_URL + "/logo.png"} />
          </div>
          <p className="client-login-app-name">Appoint24 Health</p>
          <Form
            name="normal_login"
            className="login-form"
            onFieldsChange={() => {
              setMessageVisibility(false);
            }}
            onFinish={onFinish}
          >
            <Form.Item
              name="utilizador"
              rules={[
                {
                  required: true,
                  message: "Por favor insira teu nome de utilizador!",
                },
              ]}
            >
              <Input
                prefix={<UserOutlined className="site-form-item-icon" />}
                placeholder="Nome de Utilizador/Nº do Telefone"
                size="small"
              />
            </Form.Item>
            <Form.Item
              name="senha"
              rules={[
                {
                  required: true,
                  message: "Por favor insira a sua senha!",
                },
              ]}
            >
              <Input
                prefix={<LockOutlined className="site-form-item-icon" />}
                type="password"
                placeholder="Senha de Acesso"
                size="small"
              />
            </Form.Item>

            {isMessageVisible && (
              <p className="error-message">{/* errorMessage */}</p>
            )}

            <Form.Item style={{ marginTop: "2rem" }}>
              <Button
                type="primary"
                htmlType="submit"
                className="login-form-button"
                loading={false}
              >
                Aceder minha Conta
              </Button>
            </Form.Item>

            <Form.Item className="forgot-section">
              Esqueceu senha?
              <a className="login-form-forgot" onClick={forgotPassword} href="">
                Recupere aqui
              </a>
            </Form.Item>
          </Form>
        </div>
      </div>
    </StyledDiv>
  );
};

const StyledDiv = styled.div`
  display: flex;
  height: 100%;
  justify-content: flex-start;

  > div {
    width: 450px;
    height: 100%;
    display: flex;
    background-color: rgba(43, 205, 213, 0.6);
    padding: 15px 45px;
    box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
  }

  .client-login-container {
    align-items: center;
    display: flex;
    flex-direction: column;
    justify-content: center;
  }

  .login-form-button {
    border-radius: 0px;
    height: 40px;
    background: #71473d !important;
    border-color: #71473d !important;
    padding: 2px 15px;
  }

  p.error-message {
    text-align: center;
    color: var(--vermelho-padrao);
    margin: 0;
  }

  form {
    width: 100%;
  }

  .forgot-section {
    margin: 0;
    padding-top: 50px;
  }

  .ant-form-item-control-input-content {
    display: flex;
    justify-content: center;
  }

  a {
    color: #71473d;
    text-decoration: underline;
  }

  .login-form-forgot {
    padding-left: 5px;
  }

  .ant-input-affix-wrapper {
    line-height: 2.5;
  }
`;

export default ForgotPassword;
