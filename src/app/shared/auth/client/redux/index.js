import { combineReducers } from "redux";
import API from "../../../api";

//Actions Creators 
//Post Request
export const saveData = ({ username, phoneNumber, password, name, email, alternativePhoneNumber }) => async (dispatch) => {
  dispatch({ type: CREATE_USER_REQUEST });
  try {
    const response = await API.post("/users/create", { username, phoneNumber, password, name, email, alternativePhoneNumber });
    dispatch({ type: CREATE_USER_SUCCESS, payload: response });
  } catch (error) {
    dispatch({ type: CREATE_USER_ERROR, payload: error });
  }
};

//Update Request
export const updateRow = ({ code, username, phoneNumber, password, name, email, alternativePhoneNumber }) => async (dispatch) => {
  dispatch({ type: UPDATE_USER_REQUEST });
  try {
    const response = await API.post(`/users/${code}/update`, { username, phoneNumber, password, name, email, alternativePhoneNumber });
    dispatch({ type: UPDATE_USER_SUCCESS, payload: response });
  } catch (error) {
    dispatch({ type: UPDATE_USER_ERROR, payload: error });
  }
};

//Delete Request
export const deleteRow = ({ code }) => async (dispatch) => {
  dispatch({ type: DELETE_USER_REQUEST });

  try {
    const response = await API.post(`/users/${code}/delete`);
    dispatch({ type: DELETE_USER_SUCCESS, payload: response });
  } catch (error) {
    dispatch({ type: DELETE_USER_ERROR, payload: error });
  }
};

//Get Request
export const getData = ({ page, searchTerm, size }) => async (dispatch) => {
  dispatch({ type: GET_USER_REQUEST });

  try {
    const response = await API.get("/users", { page, searchTerm, size });
    dispatch({ type: GET_USER_SUCCESS, payload: response });
  } catch (error) {
    dispatch({ type: GET_USER_ERROR, payload: error });
  }
};

//Get by ID Request
export const getDataById = (code) => async (dispatch) => {
  dispatch({ type: GET_USER_BY_ID_REQUEST });

  try {
    const response = await API.get(`/users/${code}`);
    dispatch({ type: GET_USER_BY_ID_SUCCESS, payload: response });
  } catch (error) {
    dispatch({ type: GET_USER_BY_ID_ERROR, payload: error });
  }
};

//Selectors 
export const getAllUsers = state => state.users.allUsers;
export const getUser = state => state.users.user;
export const getIsDeleteProcessing = state => state.users.isDeleteProcessing;
export const getIsGetProcessing = state => state.users.isGetProcessing;
export const getIsGetByIdProcessing = state => state.users.isGetByIdProcessing;
export const getIsCreateProcessing = state => state.users.isCreateProcessing;
export const getIsUpdateProcessing = state => state.users.isUpdateProcessing;
export const getError = state => state.users.errorGet;
export const getByIdError = state => state.users.errorGetById;
export const getCreateError = state => state.users.errorCreate;
export const getDeleteError = state => state.users.errorDelete;
export const getUpdateError = state => state.users.errorUpdate;

//Reducers -> Mensagens de Erro

//ActionTypes
//Requests
export const GET_USER_REQUEST = "GET_USER_REQUEST";
export const GET_USER_BY_ID_REQUEST = "GET_USER_BY_ID_REQUEST";
export const CREATE_USER_REQUEST = "CREATE_USER_REQUEST";
export const DELETE_USER_REQUEST = "DELETE_USER_REQUEST";
export const UPDATE_USER_REQUEST = "UPDATE_USER_REQUEST";
//Erros
export const GET_USER_ERROR = "GET_USER_ERROR";
export const GET_USER_BY_ID_ERROR = "GET_USER_BY_ID_ERROR";
export const CREATE_USER_ERROR = "CREATE_USER_ERROR";
export const UPDATE_USER_ERROR = "UPDATE_USER_ERROR";
export const DELETE_USER_ERROR = "DELETE_USER_ERROR";

//Sucesso
export const CREATE_USER_SUCCESS = "CREATE_USER_SUCCESS";
export const GET_USER_SUCCESS = "GET_USER_SUCCESS";
export const GET_USER_BY_ID_SUCCESS = "GET_USER_BY_ID_SUCCESS";
export const DELETE_USER_SUCCESS = "DELETE_USER_SUCCESS";
export const UPDATE_USER_SUCCESS = "UPDATE_USER_SUCCESS";

// ---------------------------

const errorCreate = (state = null, action) => {
  const { type, payload } = action;

  switch (type) {
    case CREATE_USER_ERROR:
      return { message: payload.message, httpcode: payload.httpcode };
    default:
      return state;
  }
};

const errorUpdate = (state = null, action) => {
  const { type, payload } = action;

  switch (type) {
    case UPDATE_USER_ERROR:
      return { message: payload.message, httpcode: payload.httpcode };
    default:
      return state;
  }
};

const errorDelete = (state = null, action) => {
  const { type, payload } = action;

  switch (type) {
    case DELETE_USER_ERROR:
      return { message: payload.message, httpcode: payload.httpcode };
    default:
      return state;
  }
};

const errorGet = (state = null, action) => {
  const { type, payload } = action;

  switch (type) {
    case GET_USER_ERROR:
      return { message: payload.message, httpcode: payload.httpcode };
    default:
      return state;
  }
};

const errorGetById = (state = null, action) => {
  const { type, payload } = action;

  switch (type) {
    case GET_USER_BY_ID_ERROR:
      return { message: payload.message, httpcode: payload.httpcode };
    default:
      return state;
  }
};

const isGetProcessing = (state = { status: false, message: null }, action) => {
  const { type } = action;

  switch (type) {
    case GET_USER_REQUEST:
      return {
        status: true,
        message: null
      };
    case UPDATE_USER_REQUEST:
    case DELETE_USER_REQUEST:
    case GET_USER_BY_ID_REQUEST:
    case CREATE_USER_REQUEST:
      return {
        status: false,
        message: null
      };
    case GET_USER_SUCCESS:
      return {
        status: false,
        message: 'Sucesso'
      };
    case GET_USER_ERROR:
      return {
        status: false,
        message: 'Erro'
      };
    default:
      return state;
  }
};

const isGetByIdProcessing = (state = { status: false, message: null }, action) => {
  const { type } = action;

  switch (type) {
    case GET_USER_BY_ID_REQUEST:
      return {
        status: true,
        message: null
      };
    case UPDATE_USER_REQUEST:
    case DELETE_USER_REQUEST:
    case GET_USER_REQUEST:
    case CREATE_USER_REQUEST:
      return {
        status: false,
        message: null
      };
    case GET_USER_BY_ID_SUCCESS:
      return {
        status: false,
        message: 'Sucesso'
      };
    case GET_USER_BY_ID_ERROR:
      return {
        status: false,
        message: 'Erro'
      };
    default:
      return state;
  }
};

const isCreateProcessing = (state = { status: false, message: null }, action) => {
  const { type } = action;

  switch (type) {
    case CREATE_USER_REQUEST:
      return {
        status: true,
        message: null
      };
    case DELETE_USER_REQUEST:
    case GET_USER_BY_ID_REQUEST:
    case GET_USER_REQUEST:
    case UPDATE_USER_REQUEST:
      return {
        status: false,
        message: null
      };
    case CREATE_USER_SUCCESS:
      return {
        status: false,
        message: 'Sucesso'
      };
    case CREATE_USER_ERROR:
      return {
        status: false,
        message: 'Erro'
      };
    default:
      return state;
  }
};

const isUpdateProcessing = (state = { status: false, message: null }, action) => {
  const { type } = action;

  switch (type) {
    case UPDATE_USER_REQUEST:
      return {
        status: true,
        message: null
      };
    case DELETE_USER_REQUEST:
    case GET_USER_BY_ID_REQUEST:
    case GET_USER_REQUEST:
    case CREATE_USER_REQUEST:
      return {
        status: false,
        message: null
      };
    case UPDATE_USER_SUCCESS:
      return {
        status: false,
        message: 'Sucesso'
      };
    case UPDATE_USER_ERROR:
      return {
        status: false,
        message: 'Erro'
      };
    default:
      return state;
  }
};

const isDeleteProcessing = (state = { status: false, message: null }, action) => {
  const { type } = action;

  switch (type) {
    case DELETE_USER_REQUEST:
      return {
        status: true,
        message: null
      };
    case CREATE_USER_REQUEST:
    case GET_USER_BY_ID_REQUEST:
    case GET_USER_REQUEST:
    case UPDATE_USER_REQUEST:
      return {
        status: false,
        message: null
      }
    case DELETE_USER_SUCCESS:
      return {
        status: false,
        message: 'sucesso'
      };
    case DELETE_USER_ERROR:
      return {
        status: false,
        message: 'erro'
      };
    default:
      return state;
  }
};

//Reducers -> Manipulacao do estado
const allUsers = (state = { data: [] }, action) => {
  const { type, payload } = action;

  switch (type) {
    case GET_USER_SUCCESS:
      return {
        data: payload.list,
        totalPages: payload.totalPages,
        currentPage: payload.currentPage,
        totalElements: payload.totalElements
      };
    default:
      return state;
  }
};

//Reducers -> Manipulacao do estado
const user = (state = { metadata: [] }, action) => {
  const { type, payload } = action;

  switch (type) {
    case GET_USER_BY_ID_SUCCESS:
      return {
        // metadata: payload.machine.metadata,
        machineId: payload.id,
        serialNumber: payload.serialNumber,
        brand: payload.brand,
        model: payload.model,
        macAddress: payload.macAddress,
        bluetoothPin: payload.bluetoothPin,
      };
    default:
      return state;
  }
};

//Export All Reducers 
export default combineReducers({
  allUsers,
  user,
  errorCreate,
  errorUpdate,
  errorDelete,
  errorGet,
  errorGetById,
  isGetProcessing,
  isCreateProcessing,
  isDeleteProcessing,
  isUpdateProcessing,
  isGetByIdProcessing
});

