import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import styled from "styled-components";
import { Form, Input, Button, Row, Col, Select, notification } from "antd";
import { UserOutlined, LockOutlined } from "@ant-design/icons";
import { Link, useHistory } from "react-router-dom";
import { saveData, getIsCreateProcessing, getCreateError } from "./redux";

const { Option } = Select;

const ClientRegister = () => {
  const [isMessageVisible, setMessageVisibility] = useState(false);
  const isSaveProcessing = useSelector(getIsCreateProcessing);
  const errorCreate = useSelector(getCreateError);

  const dispatch = useDispatch();
  const history = useHistory();

  const onFinish = (formValues) => {
    dispatch(
      saveData({
        alternativePhoneNumber: "string",
        email: "string",
        name: formValues.name,
        phoneNumber: formValues.prefix + formValues.phone_number,
        username: formValues.username,
        password: formValues.password,
      })
    );
  };

  useEffect(() => {
    if (isSaveProcessing.message !== null) {
      if (
        isSaveProcessing.message.toLowerCase() == "Sucesso".toLowerCase() &&
        !isSaveProcessing.status
      ) {
        openNotificationWithIcon("success", "Sucesso", "Inserido com Sucesso!");
        //history.push("/maquinas");
      } else {
        openNotificationWithIcon("error", "Erro", "" + errorCreate);
      }
    }
  }, [isSaveProcessing, errorCreate]);

  const openNotificationWithIcon = (tipo, titulo, mensagem) => {
    notification[tipo]({
      message: titulo,
      description: mensagem,
    });
  };

  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 24 },
      md: { span: 24 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 24 },
      md: { span: 24 },
    },
  };

  const prefixSelector = (
    <Form.Item name="prefix" noStyle>
      <Select
        style={{
          width: 80,
        }}
      >
        <Option value="258">+258</Option>
      </Select>
    </Form.Item>
  );

  return (
    <StyledDiv className="client-login-block">
      <div>
        <div className="client-login-container">
          <div className="logo">
            <img
              style={{ width: "10%" }}
              src={process.env.PUBLIC_URL + "/logo.png"}
            />
          </div>
          <p className="client-login-app-name">Appoint24 Health</p>
          <p className="app-form-title">Registo de Utilizador</p>
          <Form
            {...formItemLayout}
            name="normal_registration"
            className="login-form"
            onFieldsChange={() => {
              setMessageVisibility(false);
            }}
            onFinish={onFinish}
          >
            <Row gutter={24}>
              <Col span={24}>
                <Form.Item
                  name="name"
                  label="Nome completo"
                  rules={[
                    {
                      required: true,
                      message: "Por favor informe o seu Nome Completo!",
                    },
                  ]}
                >
                  <Input placeholder="Nome Completo" className="text-center" />
                </Form.Item>
              </Col>

              <Col xs={24} sm={24} md={12} lg={12} xl={12}>
                <Form.Item
                  name="phone_number"
                  label="Nº de Telefone"
                  rules={[
                    {
                      required: true,
                      message: "Por favor informe o seu Nº de Telefone!",
                    },
                  ]}
                >
                  <Input
                    addonBefore={prefixSelector}
                    style={{
                      width: "100%",
                    }}
                    placeholder="Telefone"
                    className="text-center"
                  />
                </Form.Item>
              </Col>

              <Col xs={24} sm={24} md={12} lg={12} xl={12}>
                <Form.Item
                  name="username"
                  label="Nome do Utilizador"
                  rules={[
                    {
                      required: true,
                      message: "Por favor informe o Nome de Utilizador!",
                    },
                  ]}
                >
                  <Input
                    placeholder="Nome de Utilizador"
                    className="text-center"
                  />
                </Form.Item>
              </Col>

              {/* Password */}
              <Col xs={24} sm={24} md={12} lg={12} xl={12}>
                <Form.Item
                  name="password"
                  label="Senha"
                  rules={[
                    {
                      required: true,
                      message: "Por favor informe a Senha!",
                    },
                  ]}
                  hasFeedback
                >
                  <Input.Password
                    size="small"
                    placeholder="Senha"
                    className="text-center"
                  />
                </Form.Item>
              </Col>

              <Col xs={24} sm={24} md={12} lg={12} xl={12}>
                <Form.Item
                  name="confirm"
                  label="confirmar Senha"
                  dependencies={["password"]}
                  hasFeedback
                  rules={[
                    {
                      required: true,
                      message: "Por favor Confirme a senha!",
                    },
                    ({ getFieldValue }) => ({
                      validator(_, value) {
                        if (!value || getFieldValue("password") === value) {
                          return Promise.resolve();
                        }

                        return Promise.reject(
                          new Error("As senhas introduzidas não são iguais!")
                        );
                      },
                    }),
                  ]}
                >
                  <Input.Password
                    placeholder="Confirme a Senha"
                    className="text-center"
                    size="small"
                  />
                </Form.Item>
              </Col>
            </Row>

            {isMessageVisible && (
              <p className="error-message">{/* errorMessage */}</p>
            )}

            <Form.Item style={{ marginTop: "2rem", marginBottom: "5rem" }}>
              <Button
                type="primary"
                htmlType="submit"
                className="login-form-button"
                loading={false}
              >
                Registar a Conta
              </Button>
            </Form.Item>
          </Form>
        </div>
      </div>
    </StyledDiv>
  );
};

const StyledDiv = styled.div`
  display: flex;
  height: 100%;
  justify-content: center;
  align-items: center;

  > div {
    max-width: 600px;
    display: flex;
    background-color: rgba(255, 255, 255, 0.7);
    margin: 40px;
    padding: 15px 45px;
    box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
  }

  .client-login-container {
    align-items: center;
    display: flex;
    flex-direction: column;
    justify-content: center;
  }

  .login-form-button {
    border-radius: 0px;
    height: 40px;
    background: #71473d !important;
    border-color: #71473d !important;
    padding: 2px 15px;
  }

  p.error-message {
    text-align: center;
    color: var(--vermelho-padrao);
    margin: 0;
  }

  form {
    width: 100%;
  }

  .forgot-section {
    margin: 0;
    padding-top: 50px;
  }

  .ant-form-item-control-input-content {
    display: flex;
    justify-content: center;
  }

  a {
    color: #71473d;
    text-decoration: underline;
  }

  .login-form-forgot {
    padding-left: 5px;
  }

  .ant-input-affix-wrapper {
    line-height: 2.5;
  }

  .ant-form-item-label {
    text-align: left;
  }

  .client-login-app-name {
    font-weight: 700;
    font-size: medium;
    padding: 20px 0 0 0;
    color: #71473d;
    font-family: "Arial";
  }

  .app-form-title {
    font-weight: 500;
    font-size: medium;
    color: #71473d;
    font-family: "Arial";
  }
`;

export default ClientRegister;
