import React, { useState } from "react";
import { Route, Switch } from "react-router-dom";
import styled from "styled-components";

// ***** ANT.DESIGN ******
import { Layout, Row, Col, Button } from "antd";

// ***** SHARED FILES *****
import Header from "../pages/components/Header";
import HomeBanner from "./components/HomeBanner";

const Home = () => {
  const [collapsed, toggleSidebar] = useState(false);

  const toggle = () => {
    toggleSidebar(!collapsed);
  };

  return (
    <Layout style={{ height: "100%" }}>
      <Header toggler={toggle} collapsed={collapsed} />
      <HomeBanner /> 
      <Layout hasSider={false}>
        <Row style={{ height: "100%" }}>
          <Col span={ 24 } className="default-layout">
            <Layout>
              <Layout.Content>
                <Switch>
                  {/* <HomeBanner /> */}
                </Switch>
              </Layout.Content>
            </Layout>
          </Col>
        </Row>
      </Layout>
    </Layout>
  );
};

const Separator = styled.div`
  margin: 0 25px;
  border-bottom-width: thin;
  border-bottom: 1px solid #9e9e9e;
`;

export default Home;
