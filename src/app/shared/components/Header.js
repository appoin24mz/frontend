import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, useHistory } from "react-router-dom";
import { Menu, Anchor, Button, Drawer } from "antd";
import {
  DashboardOutlined,
  BarChartOutlined,
  ShopOutlined,
  DisconnectOutlined,
  UserOutlined,
} from "@ant-design/icons";
import { logout, getLoggedUser } from "../auth/redux";

const { SubMenu } = Menu;

const Header = ({ toggler, collapsed }) => {
  const dispatch = useDispatch();
  const history = useHistory();

  const userData = useSelector((state) => state.auth.userInfo);

  const [visible, setVisible] = useState(false);
  const [current, setCurrent] = useState('');

  const showDrawer = () => {
    setVisible(true);
  };

  const onClose = () => {
    setVisible(false);
  };

  const logOut = () => {
    dispatch(logout());
  };

  const handleClick = (e) => {
    console.log("click ", e);
    setCurrent(e.key);
  };

  //Get de siglas do nome do utilizador
  var siglasUser = userData.nome.split(" ", 2);
  const userAvatar = siglasUser[0].charAt(0) + siglasUser[1].charAt(0);

  return (
    <div className="main-header">
      <div className="container-fluid">
        <div className="header">
          <div className="home-logo">
            <img
              className="img-fluid "
              src={process.env.PUBLIC_URL + "/logo.png"}
            />
            <a href="#">Appoint24 Health</a>
          </div>
          <div className="mobileHidden">
            <Menu
              onClick={handleClick}
              selectedKeys={[current]}
              mode="horizontal"
            >
              <Menu.Item key="dashboard" icon={<DashboardOutlined />}>
                <Link to="/app">Dashboard</Link>
              </Menu.Item>

              <SubMenu
                key="farmacias"
                icon={<ShopOutlined />}
                title="Farmácias"
              >
                <Menu.Item key="todas">
                  <Link to="/app/farmacias">Todas</Link>
                </Menu.Item>
                <Menu.Item key="irregulares">
                  <Link to="/app/farmacias">Irregulares</Link>
                </Menu.Item>
              </SubMenu>

              <SubMenu
                key="medicamentos"
                icon={<DisconnectOutlined />}
                title="Medicamentos"
              >
                <Menu.Item key="todos_medicamentos">
                  <Link to="/app/medicamentos">Medicamentos</Link>
                </Menu.Item>
                <Menu.Item key="grupo_medicamentos">
                  <Link to="/app/grupo-medicamentos">Grupo de Medicamentos</Link>
                </Menu.Item>
                <Menu.Item key="fabricantes">
                  <Link to="/app/fabricante-medicamentos">Fabricantes</Link>
                </Menu.Item>
              </SubMenu>

              <Menu.Item key="relatorios" icon={<BarChartOutlined />}>
                Relatórios
              </Menu.Item>

              <SubMenu
                key="perfil"
                icon={<UserOutlined />}
                title={userData.nome}
              >
                <Menu.Item key="meuperfil">
                  <Link to="/app/perfil">Meu Perfil</Link>
                </Menu.Item>
                <Menu.Item key="sair">
                  <Link onClick={() => logOut()}>Sair</Link>
                </Menu.Item>
              </SubMenu>
            </Menu>
          </div>
          <div className="mobileVisible">
            <Button type="primary" onClick={showDrawer}>
              <i className="fas fa-bars"></i>
            </Button>
            <Drawer
              placement="right"
              closable={false}
              onClose={onClose}
              visible={visible}
            >
              <Menu
                onClick={handleClick}
                selectedKeys={[current]}
                mode="vertical"
              >
                <Menu.Item key="dashboard" icon={<DashboardOutlined />}>
                  <Link to="/app">Dashboard</Link>
                </Menu.Item>

                <SubMenu
                  key="farmacias"
                  icon={<ShopOutlined />}
                  title="Farmácias"
                >
                  <Menu.Item key="todas">
                    <Link to="/app/farmacias">Todas</Link>
                  </Menu.Item>
                  <Menu.Item key="irregulares">
                    <Link to="/app/farmacias-irregulares">Irregulares</Link>
                  </Menu.Item>
                </SubMenu>

                <Menu.Item key="medicamentos" icon={<DisconnectOutlined />}>
                  <Link to="/app/medicamentos">Medicamentos</Link>
                </Menu.Item>

                <Menu.Item key="relatorios" icon={<BarChartOutlined />}>
                  Relatórios
                </Menu.Item>

                <SubMenu key="perfil" icon={<UserOutlined />} title="Perfil">
                  <Menu.Item key="meuperfil">
                    <Link to="/app/perfil">Meu Perfil</Link>
                  </Menu.Item>
                  <Menu.Item key="sair">
                    <Link to="/app/perfil">Sair</Link>
                  </Menu.Item>
                </SubMenu>
              </Menu>
            </Drawer>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Header;
