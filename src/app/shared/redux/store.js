import { combineReducers } from "redux";
import { configureStore, getDefaultMiddleware } from "@reduxjs/toolkit";
import {
  persistStore,
  persistReducer,
  FLUSH,
  REHYDRATE,
  PAUSE,
  PERSIST,
  PURGE,
  REGISTER,
} from "redux-persist";
import { CookieStorage } from "redux-persist-cookie-storage";
import Cookies from "cookies-js";
import auth from "../auth/redux";
import machines from "../../maquinas/redux";
import pharmacies from "../../farmacias/redux";
import medicines from "../../medicamentos/redux";
import medicineGroups from "../../medicamentos/grupos/redux";
import manufacturers from "../../medicamentos/fabricantes/redux";
import users from "../../shared/auth/client/redux";
import dashboard from "../../dashboard/redux";
import branches from "../../agencias/redux";
import operations from "../../operacoes/redux";
import menus from "../../configuracoes/menus/redux";
import userProfiles from "../../configuracoes/perfis/redux";
import sellers from "../../vendedores/redux";
import accounts from "../../contas/redux";
import accesslevels from "../../configuracoes/perfis/redux";

import storage from 'redux-persist/lib/storage';

const rootReducer = combineReducers({
  auth: persistReducer(
    {
      key: "session",
      whitelist: ["authorization", "userInfo"],
      storage: new CookieStorage(Cookies),
      storage
    },
    auth
  ),
  machines,
  medicines,
  users,
  dashboard,
  operations,
  menus,
  userProfiles,
  medicineGroups,
  manufacturers,
  pharmacies,
  // branches, 
  // sellers,
  // accounts,
  // accesslevels
});

const loggerMiddleware = (store) => (next) => (action) => {
  console.group(action.type);
  console.log("Dispatched", action);

  let result = next(action);

  console.log("New State", store.getState());
  console.groupEnd();

  return result;
};

export default function () {
  let customMiddleware = getDefaultMiddleware({
    serializableCheck: {
      ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
    },
  });

  if (process.env.NODE_ENV !== "production") {
    customMiddleware = customMiddleware.concat(loggerMiddleware);
  }

  const store = configureStore({
    reducer: rootReducer,
    middleware: customMiddleware,
  });

  const persistor = persistStore(store);
  return { store, persistor };
}
