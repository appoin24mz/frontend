import { combineReducers } from "redux";
import API from "../../shared/api";

//Actions Creators 
//Post Request
export const saveData = ({ location, name }) => async (dispatch) => {
  dispatch({ type: CREATE_BRANCH_REQUEST });
  try {
    const response = await API.post("/branches/create", { location, name });
    dispatch({ type: CREATE_BRANCH_SUCCESS, payload: response });
  } catch (error) {
    dispatch({ type: CREATE_BRANCH_ERROR, payload: error });
  }
};

//Update Request
export const updateRow = ({ code, location, name }) => async (dispatch) => {
  dispatch({ type: UPDATE_BRANCH_REQUEST });
  try {
    const response = await API.post(`/branches/${code}/update`, { location, name });
    dispatch({ type: UPDATE_BRANCH_SUCCESS, payload: response });
  } catch (error) {
    dispatch({ type: UPDATE_BRANCH_ERROR, payload: error });
  }
};

//Delete Request
export const deleteRow = ({ code }) => async (dispatch) => {
  dispatch({ type: DELETE_BRANCH_REQUEST });
  try {
    const response = await API.post(`/branches/${code}/delete`);
    dispatch({ type: DELETE_BRANCH_SUCCESS, payload: response });
  } catch (error) {
    dispatch({ type: DELETE_BRANCH_ERROR, payload: error });
  }
};

//Get Request
export const getData = ({ page, searchTerm, size }) => async (dispatch) => {
    dispatch({ type: GET_BRANCH_REQUEST });
  
    try {
      const response = await API.get("/branches", { page, searchTerm, size });
      dispatch({ type: GET_BRANCH_SUCCESS, payload: response });
    } catch (error) {
      dispatch({ type: GET_BRANCH_ERROR, payload: error });
    }
  };

  //Get by ID Request
export const getDataById = (code) => async (dispatch) => {
  dispatch({ type: GET_BY_ID_BRANCH_REQUEST });

  try {
    const response = await API.get(`/branches/${code}`);
    dispatch({ type: GET_BY_ID_BRANCH_SUCCESS, payload: response });
  } catch (error) {
    dispatch({ type: GET_BY_ID_BRANCH_ERROR, payload: error });
  }
};

//Selectors 
export const getAllBranches = state => state.branches.allBranches;
export const getBranch = state => state.branches.branch;
export const getIsDeleteProcessing = state => state.branches.isDeleteProcessing;
export const getIsGetProcessing = state => state.branches.isGetProcessing;
export const getIsGetByIdProcessing = state => state.branches.isGetByIdProcessing;
export const getIsCreateProcessing = state => state.branches.isCreateProcessing;
export const getIsUpdateProcessing = state => state.branches.isUpdateProcessing;
export const getError = state => state.branches.errorGet;
export const getByIdError = state => state.branches.errorGetById;
export const getCreateError = state => state.branches.errorCreate;
export const getDeleteError = state => state.branches.errorDelete;
export const getUpdateError = state => state.branches.errorUpdate;

// ---------------------------
//Accoes
//Requests
const GET_BRANCH_REQUEST = "GET_BRANCH_REQUEST";
const GET_BY_ID_BRANCH_REQUEST = "GET_BY_ID_BRANCH_REQUEST";
const CREATE_BRANCH_REQUEST = "CREATE_BRANCH_REQUEST";
const DELETE_BRANCH_REQUEST = "DELETE_BRANCH_REQUEST";
const UPDATE_BRANCH_REQUEST = "UPDATE_BRANCH_REQUEST";
//Erros
const GET_BRANCH_ERROR = "GET_BRANCH_ERROR";
const GET_BY_ID_BRANCH_ERROR = "GET_BY_ID_BRANCH_ERROR";
const CREATE_BRANCH_ERROR = "CREATE_BRANCH_ERROR";
const UPDATE_BRANCH_ERROR = "UPDATE_BRANCH_ERROR";
const DELETE_BRANCH_ERROR = "DELETE_BRANCH_ERROR";

//Sucesso
const CREATE_BRANCH_SUCCESS = "CREATE_BRANCH_SUCCESS";
const GET_BRANCH_SUCCESS = "GET_BRANCH_SUCCESS";
const GET_BY_ID_BRANCH_SUCCESS = "GET_BY_ID_BRANCH_SUCCESS";
const DELETE_BRANCH_SUCCESS = "DELETE_BRANCH_SUCCESS";
const UPDATE_BRANCH_SUCCESS = "UPDATE_BRANCH_SUCCESS";

// ---------------------------

//Reducers -> Mensagens de Erro

const errorCreate = (state = null, action) => {
  const { type, payload } = action;

  switch (type) {
    case CREATE_BRANCH_ERROR:
      return { message: payload.message, httpcode: payload.httpcode };
    default:
      return state;
  }
};

const errorUpdate = (state = null, action) => {
  const { type, payload } = action;

  switch (type) {
    case UPDATE_BRANCH_ERROR:
      return { message: payload.message, httpcode: payload.httpcode };
    default:
      return state;
  }
};

const errorDelete = (state = null, action) => {
  const { type, payload } = action;

  switch (type) {
    case DELETE_BRANCH_ERROR:
      return { message: payload.message, httpcode: payload.httpcode };
    default:
      return state;
  }
};

const errorGet = (state = null, action) => {
  const { type, payload } = action;

  switch (type) {
    case GET_BRANCH_ERROR:
        return { message: payload.message, httpcode: payload.httpcode };
    default:
      return state;
  }
};

const errorGetById = (state = null, action) => {
  const { type, payload } = action;

  switch (type) {
    case GET_BY_ID_BRANCH_ERROR:
        return { message: payload.message, httpcode: payload.httpcode };
    default:
      return state;
  }
};

  const isGetProcessing = (state = false, action) => {
    const { type } = action;
  
    switch (type) {
        case GET_BRANCH_REQUEST:
          return true;
        case GET_BRANCH_SUCCESS:
          return false;
        case GET_BRANCH_ERROR:
          return false;
      default:
        return state;
    }
  };

  const isGetByIdProcessing = (state = false, action) => {
    const { type } = action;
  
    switch (type) {
        case GET_BY_ID_BRANCH_REQUEST:
          return true;
        case GET_BY_ID_BRANCH_SUCCESS:
          return false;
        case GET_BY_ID_BRANCH_ERROR:
          return false;
      default:
        return state;
    }
  };

  const isCreateProcessing = (state = false, action) => {
    const { type } = action;
  
    switch (type) {
        case CREATE_BRANCH_REQUEST:
          return true;
        case CREATE_BRANCH_SUCCESS:
          return false;
        case CREATE_BRANCH_ERROR:
          return false;
        default:
          return state;
    }
  };

  const isUpdateProcessing = (state = false, action) => {
    const { type } = action;
  
    switch (type) {
        case UPDATE_BRANCH_REQUEST:
          return true;
        case UPDATE_BRANCH_SUCCESS:
          return false;
        case UPDATE_BRANCH_ERROR:
          return false;
        default:
          return state;
    }
  };

  const isDeleteProcessing = (state = false, action) => {
    const { type } = action;
  
    switch (type) {
        case DELETE_BRANCH_REQUEST:
          return true;
        case DELETE_BRANCH_SUCCESS:
          return false;
        case DELETE_BRANCH_ERROR:
          return false;
      default:
        return state;
    }
  };

  //Reducers -> Manipulacao do estado
  const allBranches = (state = {data:[]}, action) => {
    const { type, payload } = action;
  
    switch (type) {
        case GET_BRANCH_SUCCESS:
            return {
                data: payload.list,
                totalPages: payload.totalPages,
                currentPage: payload.currentPage,
                totalElements: payload.totalElements
            };
        default:
            return state;
    }
  };

  //Reducers -> Manipulacao do estado
  const branch = (state = {metadata:[]}, action) => {
    const { type, payload } = action;
  
    switch (type) {
        case GET_BY_ID_BRANCH_SUCCESS:
          return {
                metadata: payload.branch.metadata,
                branchId: payload.branch.branchId,
                name: payload.branch.name,
                location: payload.branch.location,
          };
        default:
            return state;
    }
  };

  //Export All Reducers 
export default combineReducers({
  allBranches,
  branch,
  errorCreate,
  errorUpdate,
  errorDelete,
  errorGet,
  errorGetById,
  isGetProcessing,
  isCreateProcessing,
  isDeleteProcessing,
  isUpdateProcessing,
  isGetByIdProcessing
});

