import React, { useEffect } from "react";
import {
  Row,
  Col,
  PageHeader,
  Button,
  Spin,
  Form,
  Input,
  Skeleton,
} from "antd";
import {
  getBranch,
  isRequestProcessing,
  getDataById,
  updateRow,
} from "./redux";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, useParams } from "react-router-dom";
import styled from "styled-components";
import { SaveOutlined } from "@ant-design/icons";

const Alterar = () => {
  const dispatch = useDispatch();
  const [form] = Form.useForm();
  const history = useHistory();

  const agencia = useSelector(getBranch);
  const isProcessing = useSelector(isRequestProcessing);
  let { id } = useParams();

  useEffect(() => {
    dispatch(getDataById(id));
  }, []);

  const handleUpdate = (id) => {
    console.log("Update - Action: ", id);
  };

  const onFill = () => {
    form.setFieldsValue({
      location: agencia.location,
      name: agencia.name,
    });
  };

  const handleDelete = (id) => {
    console.log("DELETE - ID: ", id);
  };

  const handleShowAllBranches = () => {
    history.push(`/agencias`);
  };

  const onFinish = (formValues) => {
    dispatch(
      updateRow({
        code: id,
        name: formValues.name,
        location: formValues.location,
      })
    );

    if (!isProcessing) {
      history.push(`/agencias`);
    }
  };

  return (
    <Row className="edoc-layout-content-general">
      <Col span={24}>
        <Skeleton loading={isProcessing} active avatar>
          {onFill()}
          <PageHeader
            className="site-page-header-responsive"
            onBack={() => window.history.back()}
            title="Alterar Agência"
            subTitle=""
            extra={[
              <>
                <Button
                  type="default"
                  style={{ marginLeft: "10px" }}
                  onClick={() => handleShowAllBranches()}
                >
                  Todas Agências
                </Button>
                <Button
                  type="primary"
                  danger
                  style={{ marginLeft: "10px" }}
                  onClick={() => handleDelete(agencia.branchId)}
                >
                  Excluir
                </Button>
              </>,
            ]}
          >
            <Row>
              <Col span={24}>
                <Spin
                  spinning={isProcessing}
                  delay={500}
                  size="large"
                  tip="Guardando os Dados..."
                >
                  <FormStyled className="edoc-form">
                    <Form
                      name="alterar-maquina"
                      form={form}
                      initialValues={{ remember: true }}
                      onFinish={onFinish}
                    >
                      <Row gutter={48}>
                        <Col span={8} offset={8}>
                          <Form.Item
                            name="name"
                            rules={[
                              {
                                required: true,
                                message: "Por favor informe o Nome!",
                              },
                            ]}
                          >
                            <Input
                              placeholder="Nome"
                              className="text-center"
                            />
                          </Form.Item>

                          <Form.Item
                            name="location"
                            rules={[
                              {
                                required: true,
                                message: "Por favor informe a Localização!",
                              },
                            ]}
                          >
                            <Input
                              placeholder="Localização"
                              className="text-center"
                            />
                          </Form.Item>
                        </Col>
                      </Row>

                      <Row>
                        <Col span={24}>
                          <div className="separator"></div>
                        </Col>
                      </Row>
                      <Row>
                        <Col span={8} offset={8}>
                          <Form.Item className="botoes">
                            <Button
                              type="primary"
                              htmlType="submit"
                              className="edoc-btn-sec"
                              icon={<SaveOutlined />}
                            >
                              Gravar
                            </Button>
                          </Form.Item>
                        </Col>
                      </Row>
                    </Form>
                  </FormStyled>
                </Spin>
              </Col>
            </Row>
          </PageHeader>
        </Skeleton>
      </Col>
    </Row>
  );
};

const FormStyled = styled.div`
  .botoes .ant-form-item-control-input-content {
    display: flex;
    justify-content: center;
    align-items: center;
  }

  button {
    margin-left: 20px;
  }

  .separator {
    border-bottom: 1px solid #e4e2e2;
    margin: 20px 0px;
  }

  .upload-column {
    display: flex;
    justify-content: center;
    align-items: center;
  }

  .ant-upload-picture-card-wrapper {
    width: auto;
  }

  .ant-input:placeholder-shown {
    text-align: center;
  }

  .ant-upload.ant-upload-select-picture-card {
    text-align: center;
    width: 100%;
    height: 100%;
    vertical-align: top;
    background-color: unset;
    border: none;
  }

  .ant-upload.ant-upload-select-picture-card > .ant-upload {
    flex-direction: column;
  }

  .preview-uploaded-image {
    height: 150px;
    width: 150px;
    border: 1px solid #bbb6b6;
    border-radius: 6px;
    margin: 20px;
    overflow: hidden;
  }
`;

export default Alterar;
