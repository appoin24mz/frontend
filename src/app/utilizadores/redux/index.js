import { combineReducers } from "redux";
import API from "../../shared/api";

//Actions Creators 
//Post Request
export const saveData = ({ name, password, userProfileId, username }) => async (dispatch) => {
  dispatch({ type: CREATE_USER_REQUEST });
  try {
    const response = await API.post("/users/create", { name, password, userProfileId, username });
    dispatch({ type: CREATE_USER_SUCCESS, payload: response });
  } catch (error) {
    dispatch({ type: CREATE_USER_ERROR, payload: error });
  }
};

//Update Request
export const updateRow = ({ code, name, password, userProfileId, username }) => async (dispatch) => {
  dispatch({ type: UPDATE_USER_REQUEST });
  try {
    const response = await API.post(`/users/${code}/update`, { name, password, userProfileId, username });
    dispatch({ type: UPDATE_USER_SUCCESS, payload: response });
  } catch (error) {
    dispatch({ type: UPDATE_USER_ERROR, payload: error });
  }
};

//Delete Request
export const deleteRow = ({ code }) => async (dispatch) => {
  dispatch({ type: DELETE_USER_REQUEST });

  try {
    const response = await API.post(`/users/${code}/delete`);
    dispatch({ type: DELETE_USER_SUCCESS, payload: response });
  } catch (error) {
    dispatch({ type: DELETE_USER_ERROR, payload: error });
  }
};

//Get Request
export const getData = ({ page, searchTerm, size }) => async (dispatch) => {
  dispatch({ type: GET_USER_REQUEST });

  try {
    const response = await API.get(`/users?page=${page}&searchTerm=${searchTerm == "" ? "" : searchTerm}&page=${size}`, { page, searchTerm, size });
    dispatch({ type: GET_USER_SUCCESS, payload: response });
  } catch (error) {
    dispatch({ type: GET_USER_ERROR, payload: error });
  }
};

//Get by ID Request
export const getDataById = (code) => async (dispatch) => {
  dispatch({ type: GET_USER_BY_ID_REQUEST });

  try {
    const response = await API.get(`/users/${code}`);
    dispatch({ type: GET_USER_BY_ID_SUCCESS, payload: response });
  } catch (error) {
    dispatch({ type: GET_USER_BY_ID_ERROR, payload: error });
  }
};

//Selectors 
export const getAllUsers = state => state.users.allUsers;
export const getUser = state => state.users.user;
export const getIsDeleteProcessing = state => state.users.isDeleteProcessing;
export const getIsGetProcessing = state => state.users.isGetProcessing;
export const getIsGetByIdProcessing = state => state.users.isGetByIdProcessing;
export const getIsCreateProcessing = state => state.users.isCreateProcessing;
export const getIsUpdateProcessing = state => state.users.isUpdateProcessing;
export const getError = state => state.users.errorGet;
export const getByIdError = state => state.users.errorGetById;
export const getCreateError = state => state.users.errorCreate;
export const getDeleteError = state => state.users.errorDelete;
export const getUpdateError = state => state.users.errorUpdate;

// ---------------------------
//Accoes
//Requests
const GET_USER_REQUEST = "GET_USER_REQUEST";
const GET_USER_BY_ID_REQUEST = "GET_USER_BY_ID_REQUEST";
const CREATE_USER_REQUEST = "CREATE_USER_REQUEST";
const DELETE_USER_REQUEST = "DELETE_USER_REQUEST";
const UPDATE_USER_REQUEST = "UPDATE_USER_REQUEST";
//Erros
const GET_USER_ERROR = "GET_USER_ERROR";
const GET_USER_BY_ID_ERROR = "GET_USER_BY_ID_ERROR";
const CREATE_USER_ERROR = "CREATE_USER_ERROR";
const UPDATE_USER_ERROR = "UPDATE_USER_ERROR";
const DELETE_USER_ERROR = "DELETE_USER_ERROR";

//Sucesso
const CREATE_USER_SUCCESS = "CREATE_USER_SUCCESS";
const GET_USER_SUCCESS = "GET_USER_SUCCESS";
const GET_USER_BY_ID_SUCCESS = "GET_USER_BY_ID_SUCCESS";
const DELETE_USER_SUCCESS = "DELETE_USER_SUCCESS";
const UPDATE_USER_SUCCESS = "UPDATE_USER_SUCCESS";

// ---------------------------

//Reducers -> Mensagens de Erro

const errorCreate = (state = null, action) => {
  const { type, payload } = action;

  switch (type) {
    case CREATE_USER_ERROR:
      return { message: payload.message, httpcode: payload.httpcode };
    default:
      return state;
  }
};

const errorUpdate = (state = null, action) => {
  const { type, payload } = action;

  switch (type) {
    case UPDATE_USER_ERROR:
      return { message: payload.message, httpcode: payload.httpcode };
    default:
      return state;
  }
};

const errorDelete = (state = null, action) => {
  const { type, payload } = action;

  switch (type) {
    case DELETE_USER_ERROR:
      return { message: payload.message, httpcode: payload.httpcode };
    default:
      return state;
  }
};

const errorGet = (state = null, action) => {
  const { type, payload } = action;

  switch (type) {
    case GET_USER_ERROR:
      return { message: payload.message, httpcode: payload.httpcode };
    default:
      return state;
  }
};

const errorGetById = (state = null, action) => {
  const { type, payload } = action;

  switch (type) {
    case GET_USER_BY_ID_ERROR:
      return { message: payload.message, httpcode: payload.httpcode };
    default:
      return state;
  }
};

const isGetProcessing = (state = { status: false, message: null }, action) => {
  const { type } = action;

  switch (type) {
    case GET_USER_REQUEST:
      return {
        status: true,
        message: null
      };
    case UPDATE_USER_REQUEST:
    case DELETE_USER_REQUEST:
    case GET_USER_BY_ID_REQUEST:
    case CREATE_USER_REQUEST:
      return {
        status: false,
        message: null
      };
    case GET_USER_SUCCESS:
      return {
        status: false,
        message: 'Sucesso'
      };
    case GET_USER_ERROR:
      return {
        status: false,
        message: 'Erro'
      };
    default:
      return state;
  }
};

const isGetByIdProcessing = (state = { status: false, message: null }, action) => {
  const { type } = action;

  switch (type) {
    case GET_USER_BY_ID_REQUEST:
      return {
        status: true,
        message: null
      };
    case UPDATE_USER_REQUEST:
    case DELETE_USER_REQUEST:
    case GET_USER_REQUEST:
    case CREATE_USER_REQUEST:
      return {
        status: false,
        message: null
      };
    case GET_USER_BY_ID_SUCCESS:
      return {
        status: false,
        message: 'Sucesso'
      };
    case GET_USER_BY_ID_ERROR:
      return {
        status: false,
        message: 'Erro'
      };
    default:
      return state;
  }
};

const isCreateProcessing = (state = { status: false, message: null }, action) => {
  const { type } = action;

  switch (type) {
    case CREATE_USER_REQUEST:
      return {
        status: true,
        message: null
      };
    case DELETE_USER_REQUEST:
    case GET_USER_BY_ID_REQUEST:
    case GET_USER_REQUEST:
    case UPDATE_USER_REQUEST:
      return {
        status: false,
        message: null
      };
    case CREATE_USER_SUCCESS:
      return {
        status: false,
        message: 'Sucesso'
      };
    case CREATE_USER_ERROR:
      return {
        status: false,
        message: 'Erro'
      };
    default:
      return state;
  }
};

const isUpdateProcessing = (state = { status: false, message: null }, action) => {
  const { type } = action;

  switch (type) {
    case UPDATE_USER_REQUEST:
      return {
        status: true,
        message: null
      };
    case DELETE_USER_REQUEST:
    case GET_USER_BY_ID_REQUEST:
    case GET_USER_REQUEST:
    case CREATE_USER_REQUEST:
      return {
        status: false,
        message: null
      };
    case UPDATE_USER_SUCCESS:
      return {
        status: false,
        message: 'Sucesso'
      };
    case UPDATE_USER_ERROR:
      return {
        status: false,
        message: 'Erro'
      };
    default:
      return state;
  }
};

const isDeleteProcessing = (state = { status: false, message: null }, action) => {
  const { type } = action;

  switch (type) {
    case DELETE_USER_REQUEST:
      return {
        status: true,
        message: null
      };
    case CREATE_USER_REQUEST:
    case GET_USER_BY_ID_REQUEST:
    case GET_USER_REQUEST:
    case UPDATE_USER_REQUEST:
      return {
        status: false,
        message: null
      }
    case DELETE_USER_SUCCESS:
      return {
        status: false,
        message: 'sucesso'
      };
    case DELETE_USER_ERROR:
      return {
        status: false,
        message: 'erro'
      };
    default:
      return state;
  }
};

//Reducers -> Manipulacao do estado
const allUsers = (state = { data: [] }, action) => {
  const { type, payload } = action;

  switch (type) {
    case GET_USER_SUCCESS:
      return {
        data: payload.list,
        totalPages: payload.totalPages,
        currentPage: payload.currentPage,
        totalElements: payload.totalElements
      };
    default:
      return state;
  }
};

//Reducers -> Manipulacao do estado
const user = (state = { metadata: [] }, action) => {
  const { type, payload } = action;

  switch (type) {
    case GET_USER_BY_ID_SUCCESS:
      return {
        userId: payload.id,
        userProfileId: payload.userProfileId,
        username: payload.username,
        name: payload.name,
      };
    default:
      return state;
  }
};

//Export All Reducers 
export default combineReducers({
  allUsers,
  user,
  errorCreate,
  errorUpdate,
  errorDelete,
  errorGet,
  errorGetById,
  isGetProcessing,
  isCreateProcessing,
  isDeleteProcessing,
  isUpdateProcessing,
  isGetByIdProcessing
});

