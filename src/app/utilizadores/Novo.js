import React, { useEffect } from "react";
import { Input, Row, Col, Button, Form, PageHeader, Spin, notification, Select } from "antd";
import { SaveOutlined } from "@ant-design/icons";
import styled from "styled-components";
import {
  saveData,
  getIsCreateProcessing,
  getCreateError
} from "./redux";

import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";

const { Option } = Select;

const Novo = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const isSaveProcessing = useSelector(getIsCreateProcessing);
  const errorCreate = useSelector(getCreateError);

  const onFinish = (formValues) => {
    dispatch(
      saveData({
        name: formValues.name,
        password: formValues.password,
        userProfileId: formValues.user_profile_id,
        username: formValues.username,
      })
    );
  };

  useEffect(() => {

    if (isSaveProcessing.message !== null) {
      if (isSaveProcessing.message.toLowerCase() == 'Sucesso'.toLowerCase() && !isSaveProcessing.status) {
        openNotificationWithIcon('success', 'Sucesso', 'Inserido com Sucesso!');
        history.push("/utilizadores");
      } else {
        openNotificationWithIcon('error', 'Erro', '' + errorCreate);
      }
    }
  }, [isSaveProcessing, errorCreate]);

  const openNotificationWithIcon = (tipo, titulo, mensagem) => {
    notification[tipo]({
      message: titulo,
      description: mensagem
    });
  };

  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 8 },
      md: { span: 8 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 16 },
      md: { span: 16 },
    },
  };

  const onChange = (value) => {
    console.log(`selected ${value}`);
  }
  
  const onBlur = () => {
    console.log('blur');
  }
  
  const onFocus = () => {
    console.log('focus');
  }
  
  const onSearch = (val) => {
    console.log('search:', val);
  }

  return (
    <Row className="ccc-layout-content-general">
      <Col span={24}>
        <PageHeader
          className="site-page-header-responsive"
          onBack={() => window.history.back()}
          title="Adiconar Novo Utilizador"
          subTitle=""
        >
          <Row>
            <Col span={24}>
              <Spin
                spinning={isSaveProcessing.status}
                delay={500}
                size="large"
                tip="Guardando os Dados..."
              >
                <FormStyled className="ccc-form">
                  <Form
                    {...formItemLayout}
                    name="add-user"
                    initialValues={{ remember: true }}
                    onFinish={onFinish}
                  >
                    <Row gutter={48}>
                      <Col span={12} offset={4}>
                        <Form.Item
                          name="name"
                          label="Nome Proprio"
                          rules={[
                            {
                              required: true,
                              message: "Por favor informe o Nome!",
                            },
                          ]}
                        >
                          <Input placeholder="Nome Proprio" className="text-center" />
                        </Form.Item>

                        <Form.Item
                          name="username"
                          label="Nome de Utilizador"
                          rules={[
                            {
                              required: true,
                              message: "Por favor informe o Nome de Utilizador!",
                            },
                          ]}
                        >
                          <Input placeholder="Nome de Utilizador" className="text-center" />
                        </Form.Item>

                        <Form.Item
                          name="password"
                          label="Senha"
                          rules={[
                            {
                              required: true,
                              message: "Por favor informe a Senha!",
                            },
                          ]}
                        >
                          <Input.Password
                            placeholder="Informa a Senha"
                            className="text-center"
                          />
                        </Form.Item>

                        <Form.Item
                          name="password-confirmation"
                          label="Confirme a Senha"
                          rules={[
                            {
                              required: true,
                              message: "Por favor confirme a Senha!",
                            },
                          ]}
                        >
                          <Input.Password
                            placeholder="Confirme a Senha"
                            className="text-center"
                          />
                        </Form.Item>

                        <Form.Item
                          name="user_profile_id"
                          label="Perfil de Utilizador"
                          rules={[
                            {
                              required: true,
                              message: "Por favor Seleccione o perfil de utilizador!",
                            },
                          ]}
                        >
                          <Select
                            showSearch
                            // style={{ width: 200 }}
                            placeholder="Seleccione o Perfil"
                            optionFilterProp="children"
                            onChange={onChange}
                            onFocus={onFocus}
                            onBlur={onBlur}
                            onSearch={onSearch}
                            filterOption={(input, option) =>
                              option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                            }
                          >
                            <Option value="admin">Administrador</Option>
                            <Option value="operator">Operador</Option>
                            <Option value="user">Utilizador</Option>
                          </Select>
                        </Form.Item>
                      </Col>
                    </Row>

                    <Row>
                      <Col span={24}>
                        <div className="separator"></div>
                      </Col>
                    </Row>
                    <Row>
                      <Col span={8} offset={8}>
                        <Form.Item className="botoes">
                          <Button
                            type="primary"
                            htmlType="submit"
                            className="edoc-btn-sec"
                            icon={<SaveOutlined />}
                            loading={isSaveProcessing.status}
                          >
                            Gravar
                          </Button>
                        </Form.Item>
                      </Col>
                    </Row>
                  </Form>
                </FormStyled>
              </Spin>
            </Col>
          </Row>
        </PageHeader>
      </Col>
    </Row>
  );
};

const FormStyled = styled.div`
  .botoes .ant-form-item-control-input-content {
    display: flex;
    justify-content: center;
    align-items: center;
  }

  button {
    margin-left: 20px;
  }

  .separator {
    border-bottom: 1px solid #e4e2e2;
    margin: 20px 0px;
  }

  .upload-column {
    display: flex;
    justify-content: center;
    align-items: center;
  }

  .ant-upload-picture-card-wrapper {
    width: auto;
  }

  .ant-input:placeholder-shown {
    text-align: center;
  }

  .ant-upload.ant-upload-select-picture-card {
    text-align: center;
    width: 100%;
    height: 100%;
    vertical-align: top;
    background-color: unset;
    border: none;
  }

  .ant-upload.ant-upload-select-picture-card > .ant-upload {
    flex-direction: column;
  }

  .preview-uploaded-image {
    height: 150px;
    width: 150px;
    border: 1px solid #bbb6b6;
    border-radius: 6px;
    margin: 20px;
    overflow: hidden;
  }
`;

export default Novo;
