import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  Row,
  Col,
  Table,
  Input,
  Button,
  Menu,
  Modal,
  Dropdown,
  PageHeader,
  Tabs,
  Spin,
  notification,
} from "antd";
import {
  EditOutlined,
  DeleteOutlined,
  PicLeftOutlined,
  DownOutlined,
  ExclamationCircleOutlined,
} from "@ant-design/icons";
import {
  useHistory,
} from "react-router-dom";

import {
  getData,
  deleteRow,
  getAllUsers,
  getError,
  getDeleteError,
  getIsDeleteProcessing,
  getIsGetProcessing,
} from "./redux";

const { confirm } = Modal;

const Utilizadores = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  //Local State
  const [currentPageSize, setCurrentPageSize] = useState(5);
  const [users, setUsers] = useState();
  const [searchVal, setSearchVal] = useState(null);

  //Store State 
  const allUsers = useSelector(getAllUsers);

  //Processing status
  const isProcessing = useSelector(getIsGetProcessing);
  const isDeleteProcessing = useSelector(getIsDeleteProcessing);

  //Error status
  const errorGet = useSelector(getError);
  const errorDelete = useSelector(getDeleteError);

  //Local Variables 
  const SUCESSO = 'sucesso';

  //useEffect Operations
  useEffect(() => {
    dispatch(getData({ page: 1, searchTerm: "", size: 1400 }));
  }, []);

  //Get Processing 
  useEffect(() => {
    if (isProcessing.message !== null) {
      if (isProcessing.message.toLowerCase() == SUCESSO && !isProcessing.status) {
        console.info('Carregado com sucesso');
        setUsers(generateUsers(allUsers.data));
        console.log('Data: ', generateUsers(allUsers.data));
      } else {
        openNotificationWithIcon('error', 'Erro', '' + errorGet.message);
      }
    }
  }, [isProcessing, errorGet]);

  //Delete Processing 
  useEffect(() => {
    if (isDeleteProcessing.message !== null) {
      if (isDeleteProcessing.message.toLowerCase() == SUCESSO && !isDeleteProcessing.status) {
        openNotificationWithIcon('success', 'Sucesso', 'Registo Eliminado com Sucesso!');
        dispatch(getData({ page: 1, searchTerm: "", size: 1500 }));
      } else {
        openNotificationWithIcon('error', 'Erro', '' + errorDelete.message);
      }
    }
  }, [isDeleteProcessing, errorDelete]);

  //Local Functions 
  const handleMenuOption = (record, key) => {
    if (key.key === "0") {
      handleUpdate(record.key);
    } else if (key.key === "1") {
      handleDetails(record.key);
      // console.log(record);
    } else if (key.key === "2") {
      handleDelete(record.key);
    }
  };

  const handleUpdate = (id) => {
    history.push(`/alterar-utilizador/${id}`);
  };

  const handleDetails = (id) => {
    history.push(`/detalhes-utilizador/${id}`);
  };

  const handleDelete = (id) => {
    showDeleteConfirm(id);
  };

  const showDeleteConfirm = (id) => {
    confirm({
      title: 'Tem certeza que quer APAGAR/EXCLUIR esse Registo?',
      icon: <ExclamationCircleOutlined />,
      content: '',
      okText: 'Sim',
      okType: 'danger',
      cancelText: 'Não',
      onOk() {
        console.log('OK');
        dispatch(
          deleteRow({
            code: id,
          })
        );
      },
      onCancel() {
        console.log('Cancelado');
      },
    });
  }

  const menu = (record) => {
    return (
      <Menu
        className="ccc-dropdown-menu"
        onClick={(key) => handleMenuOption(record, key)}
      >
        <Menu.Item key="0">
          <EditOutlined /> Alterar
        </Menu.Item>
        <Menu.Divider />
        <Menu.Item key="1">
          <PicLeftOutlined /> Detalhar
        </Menu.Item>
        <Menu.Divider />
        <Menu.Item className="ccc-delete-item" key="2">
          <DeleteOutlined /> Excluir
        </Menu.Item>
      </Menu>
    );
  };

  const columns = [
    {
      title: "Nome",
      dataIndex: "name",
      key: "name",
      width: "30%",
      sorter: (a, b) => a.name.length - b.name.length,
      ellipsis: true,
    },
    {
      title: "Nome de Utilizador",
      dataIndex: "username",
      key: "username",
      width: "30%",
      sorter: (a, b) => a.username.length - b.username.length,
      ellipsis: true,
    },
    {
      title: "Perfil de Utilizador",
      dataIndex: "user_profile_id",
      key: "user_profile_id",
      width: "30%",
      sorter: (a, b) => a.user_profile_id.length - b.user_profile_id.length,
      ellipsis: true,
    },
    {
      title: "",
      dataIndex: "accoes",
      key: "accoes",
      render: (text, record) => {
        //console.log(record.key);
        return (
          <Dropdown overlay={menu(record)} trigger={["click"]}>
            <Button className="ccc-btn-sec" onClick={(e) => e.preventDefault()}>
              Acções <DownOutlined />
            </Button>
          </Dropdown>
        );
      },
    },
  ];

  const generateUsers = (users) => {
    const generatedUsers =
      users &&
      users.map((item) => {
        return {
          key: item.id,
          name: item.name,
          username: item.username,
          user_profile_id: item.userProfileId,
          accoes: "",
        };
      });

    return generatedUsers;
  };

  const onShowSizeChange = (current, pageSize) => {
    setCurrentPageSize(pageSize);
    console.log('Current: ', currentPageSize);
    console.log(current, pageSize);
  };

  const openNotificationWithIcon = (tipo, titulo, mensagem) => {
    notification[tipo]({
      message: titulo,
      description: mensagem
    });
  };

  const getFiltredResult = (e) => {
    console.log('FILTRED VALUE: ',e);
    dispatch(getData({ page: "", searchTerm: e, size: "" }));
  };

  return (
    <Row className="ccc-layout-content-general">
      <Col span={24}>
        <Spin spinning={isDeleteProcessing.status} tip="Eliminando o Registo...">
          <PageHeader
            className="site-page-header-responsive"
            onBack={() => window.history.back()}
            title="Utilizadores"
            subTitle="Listagem de Utilizadores"
            extra={[
              <>
                <Input.Search
                  placeholder="Procurar Utilizador"
                  onSearch={getFiltredResult}
                  onChange={(e) => setSearchVal(e.target.value)}
                />
                <Button
                  type="primary"
                  onClick={() => history.push(`/novo-utilizador`)}
                >
                  Novo utilizador
                </Button>
              </>,
            ]}
            footer={<Tabs defaultActiveKey="1"></Tabs>}
          >
            <Row>
              <Col span={24}>
                <Table
                  columns={columns}
                  pagination={{
                    pageSize: currentPageSize,
                    total: allUsers.totalElements,
                    showTotal: (total) => `Total ${total} Registos`,
                    defaultCurrent: 1,
                    pageSizeOptions: [5, 10, 25, 50, 100],
                    onShowSizeChange: (defaultCurrent, pageSize) => onShowSizeChange(defaultCurrent, pageSize),
                    showSizeChanger: true,
                  }}
                  dataSource={users}
                  size="small"
                  loading={isProcessing.status}
                />
              </Col>
            </Row>
          </PageHeader>
        </Spin>
      </Col>
    </Row>
  );
};

export default Utilizadores;
