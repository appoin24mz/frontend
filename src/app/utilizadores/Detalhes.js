import React, { useEffect } from "react";
import {
  Row,
  Col,
  PageHeader,
  Button,
  Skeleton,
  Statistic,
  Descriptions,
  Modal,
  notification,
} from "antd";
import {
  getUser,
  deleteRow,
  getIsGetByIdProcessing,
  getIsDeleteProcessing,
  getByIdError,
  getDeleteError,
  getDataById,
} from "./redux";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, useParams } from "react-router-dom";
import { ExclamationCircleOutlined, } from "@ant-design/icons";


const { confirm } = Modal;

const Detalhes = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  //Local Variables 
  const SUCESSO = 'sucesso';
  let { id } = useParams();

  //Local State

  //Store State 
  const user = useSelector(getUser);

  //Processing status
  const isGetByIdProcessing = useSelector(getIsGetByIdProcessing);
  const isDeleteProcessing = useSelector(getIsDeleteProcessing);

  //Error status
  const errorDelete = useSelector(getDeleteError);
  const errorGetById = useSelector(getByIdError);

  //useEffect Operations
  useEffect(() => {
    dispatch(getDataById(id));
  }, []);

  useEffect(() => {
    if (isGetByIdProcessing.message !== null) {
      if (isGetByIdProcessing.message.toLowerCase() == 'Sucesso'.toLowerCase() && !isGetByIdProcessing.status) {
        console.info('Carregado com sucesso');
      } else {
        openNotificationWithIcon('error', 'Erro', '' + errorGetById.message);
      }
    }
  }, [isGetByIdProcessing, errorGetById]);

  //Delete Processing 
  useEffect(() => {
    if (isDeleteProcessing.message !== null) {
      if (isDeleteProcessing.message.toLowerCase() == SUCESSO && !isDeleteProcessing.status) {
        openNotificationWithIcon('success', 'Sucesso', 'Registo Eliminado com Sucesso!');
        history.push('/utilizadores');
      } else {
        openNotificationWithIcon('error', 'Erro', '' + errorDelete.message);
      }
    }
  }, [isDeleteProcessing, errorDelete]);

  //Local Functions 

  const handleUpdate = (id) => {
    history.push(`/alterar-utilizador/${id}`);
  };

  const handleShowAllUsers = () => {
    history.push(`/utilizadores`);
  };

  const openNotificationWithIcon = (tipo, titulo, mensagem) => {
    notification[tipo]({
      message: titulo,
      description: mensagem
    });
  };

  const handleDelete = (id) => {
    showDeleteConfirm(id);
  };

  const showDeleteConfirm = (id) => {
    confirm({
      title: 'Tem certeza que quer APAGAR/EXCLUIR esse Registo?',
      icon: <ExclamationCircleOutlined />,
      content: '',
      okText: 'Sim',
      okType: 'danger',
      cancelText: 'Não',
      onOk() {
        dispatch(
          deleteRow({
            code: id,
          })
        );
      },
      onCancel() {
        console.log('Cancelado');
      },
    });
  };

  return (
    <Row className="ccc-layout-content-general">
      <Col span={24}>
        <PageHeader
          className="site-page-header-responsive"
          onBack={() => window.history.back()}
          title="Detalhes do Utilizador"
          subTitle=""
          extra={[
            <>
              <Button
                type="default"
                style={{ marginLeft: "10px" }}
                onClick={() => handleShowAllUsers()}
              >
                Todos Utilizadores
              </Button>
              <Button
                type="primary"
                style={{ marginLeft: "10px" }}
                onClick={() => handleUpdate(user.userId)}
              >
                Alterar
              </Button>
              <Button
                type="primary"
                danger
                style={{ marginLeft: "10px" }}
                onClick={() => handleDelete(user.userId)}
              >
                Excluir
              </Button>
            </>,
          ]}
        >
          <Row>
            <Col span={24}>
              <Skeleton loading={isGetByIdProcessing.status} active avatar>
                <div className="content">
                  <div className="extra">
                    <div
                      style={{
                        display: "flex",
                        width: "max-content",
                        justifyContent: "flex-end",
                      }}
                    >
                      <Statistic
                        title="Nome"
                        value={user.name}
                        style={{
                          marginRight: 32,
                        }}
                      />
                      <Statistic title="Nome de Utilizador" value={user.username} />
                    </div>
                  </div>
                  <div className="main">
                    <Descriptions size="small" column={3}>
                      <Descriptions.Item label="Perfil de Utilizador">
                        {user.userProfileId}
                      </Descriptions.Item>
                      {/* <Descriptions.Item label="Mac Address">
                        {machine.macAddress}
                      </Descriptions.Item>
                      <Descriptions.Item label="Bluetooth Pin">
                        {machine.bluetoothPin}
                      </Descriptions.Item> */}
                    </Descriptions>
                  </div>
                </div>
              </Skeleton>
            </Col>
          </Row>
        </PageHeader>
      </Col>
    </Row>
  );
};

export default Detalhes;
