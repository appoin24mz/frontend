import { combineReducers } from "redux";
import API from "../../shared/api";

//Actions Creators 
//Post Request
export const saveData = ({ composition, description, format, groupId, image, manufacturerId, name, referencePrice}) => async (dispatch) => {
  dispatch({ type: CREATE_MEDICINES_REQUEST });
  try {
    const response = await API.post("/medicines/register", { composition, description, format, groupId, image, manufacturerId, name, referencePrice });
    dispatch({ type: CREATE_MEDICINES_SUCCESS, payload: response });
  } catch (error) {
    dispatch({ type: CREATE_MEDICINES_ERROR, payload: error });
  }
};

//Update Request
export const updateRow = ({ code, composition, description, format, groupId, image, manufacturerId, name, referencePrice }) => async (dispatch) => {
  dispatch({ type: UPDATE_MEDICINES_REQUEST });
  try {
    const response = await API.post(`/medicines/${code}/update`, { composition, description, format, groupId, image, manufacturerId, name, referencePrice });
    dispatch({ type: UPDATE_MEDICINES_SUCCESS, payload: response });
  } catch (error) {
    dispatch({ type: UPDATE_MEDICINES_ERROR, payload: error });
  }
};

//Delete Request
export const deleteRow = ({ code }) => async (dispatch) => {
  dispatch({ type: DELETE_MEDICINES_REQUEST });

  try {
    const response = await API.post(`/medicines/${code}/delete`);
    dispatch({ type: DELETE_MEDICINES_SUCCESS, payload: response });
  } catch (error) {
    dispatch({ type: DELETE_MEDICINES_ERROR, payload: error });
  }
};

//Get Request
export const getData = ({ page, searchTerm, size }) => async (dispatch) => {
  dispatch({ type: GET_MEDICINES_REQUEST });

  try {
    const response = await API.get("/medicines", { page, searchTerm, size });
    dispatch({ type: GET_MEDICINES_SUCCESS, payload: response });
  } catch (error) {
    dispatch({ type: GET_MEDICINES_ERROR, payload: error });
  }
};

//Get by ID Request
export const getDataById = (code) => async (dispatch) => {
  dispatch({ type: GET_MEDICINES_BY_ID_REQUEST });

  try {
    const response = await API.get(`/medicines/${code}`);
    dispatch({ type: GET_MEDICINES_BY_ID_SUCCESS, payload: response });
  } catch (error) {
    dispatch({ type: GET_MEDICINES_BY_ID_ERROR, payload: error });
  }
};

//Selectors 
export const getAllMedicines = state => state.medicines.allMedicines;
export const getMedicine = state => state.medicines.medicine;
export const getIsDeleteProcessing = state => state.medicines.isDeleteProcessing;
export const getIsGetProcessing = state => state.medicines.isGetProcessing;
export const getIsGetByIdProcessing = state => state.medicines.isGetByIdProcessing;
export const getIsCreateProcessing = state => state.medicines.isCreateProcessing;
export const getIsUpdateProcessing = state => state.medicines.isUpdateProcessing;
export const getError = state => state.medicines.errorGet;
export const getByIdError = state => state.medicines.errorGetById;
export const getCreateError = state => state.medicines.errorCreate;
export const getDeleteError = state => state.medicines.errorDelete;
export const getUpdateError = state => state.medicines.errorUpdate;

// ---------------------------
//Accoes
//Requests
const GET_MEDICINES_REQUEST = "GET_MEDICINES_REQUEST";
const GET_MEDICINES_BY_ID_REQUEST = "GET_MEDICINES_BY_ID_REQUEST";
const CREATE_MEDICINES_REQUEST = "CREATE_MEDICINES_REQUEST";
const DELETE_MEDICINES_REQUEST = "DELETE_MEDICINES_REQUEST";
const UPDATE_MEDICINES_REQUEST = "UPDATE_MEDICINES_REQUEST";
//Erros
const GET_MEDICINES_ERROR = "GET_MEDICINES_ERROR";
const GET_MEDICINES_BY_ID_ERROR = "GET_MEDICINES_BY_ID_ERROR";
const CREATE_MEDICINES_ERROR = "CREATE_MEDICINES_ERROR";
const UPDATE_MEDICINES_ERROR = "UPDATE_MEDICINES_ERROR";
const DELETE_MEDICINES_ERROR = "DELETE_MEDICINES_ERROR";

//Sucesso
const CREATE_MEDICINES_SUCCESS = "CREATE_MEDICINES_SUCCESS";
const GET_MEDICINES_SUCCESS = "GET_MEDICINES_SUCCESS";
const GET_MEDICINES_BY_ID_SUCCESS = "GET_MEDICINES_BY_ID_SUCCESS";
const DELETE_MEDICINES_SUCCESS = "DELETE_MEDICINES_SUCCESS";
const UPDATE_MEDICINES_SUCCESS = "UPDATE_MEDICINES_SUCCESS";

// ---------------------------

//Reducers -> Mensagens de Erro

const errorCreate = (state = null, action) => {
  const { type, payload } = action;

  switch (type) {
    case CREATE_MEDICINES_ERROR:
      return { message: payload.message, httpcode: payload.httpcode };
    default:
      return state;
  }
};

const errorUpdate = (state = null, action) => {
  const { type, payload } = action;

  switch (type) {
    case UPDATE_MEDICINES_ERROR:
      return { message: payload.message, httpcode: payload.httpcode };
    default:
      return state;
  }
};

const errorDelete = (state = null, action) => {
  const { type, payload } = action;

  switch (type) {
    case DELETE_MEDICINES_ERROR:
      return { message: payload.message, httpcode: payload.httpcode };
    default:
      return state;
  }
};

const errorGet = (state = null, action) => {
  const { type, payload } = action;

  switch (type) {
    case GET_MEDICINES_ERROR:
      return { message: payload.message, httpcode: payload.httpcode };
    default:
      return state;
  }
};

const errorGetById = (state = null, action) => {
  const { type, payload } = action;

  switch (type) {
    case GET_MEDICINES_BY_ID_ERROR:
      return { message: payload.message, httpcode: payload.httpcode };
    default:
      return state;
  }
};

const isGetProcessing = (state = { status: false, message: null }, action) => {
  const { type } = action;

  switch (type) {
    case GET_MEDICINES_REQUEST:
      return {
        status: true,
        message: null
      };
    case UPDATE_MEDICINES_REQUEST:
    case DELETE_MEDICINES_REQUEST:
    case GET_MEDICINES_BY_ID_REQUEST:
    case CREATE_MEDICINES_REQUEST:
      return {
        status: false,
        message: null
      };
    case GET_MEDICINES_SUCCESS:
      return {
        status: false,
        message: 'Sucesso'
      };
    case GET_MEDICINES_ERROR:
      return {
        status: false,
        message: 'Erro'
      };
    default:
      return state;
  }
};

const isGetByIdProcessing = (state = { status: false, message: null }, action) => {
  const { type } = action;

  switch (type) {
    case GET_MEDICINES_BY_ID_REQUEST:
      return {
        status: true,
        message: null
      };
    case UPDATE_MEDICINES_REQUEST:
    case DELETE_MEDICINES_REQUEST:
    case GET_MEDICINES_REQUEST:
    case CREATE_MEDICINES_REQUEST:
      return {
        status: false,
        message: null
      };
    case GET_MEDICINES_BY_ID_SUCCESS:
      return {
        status: false,
        message: 'Sucesso'
      };
    case GET_MEDICINES_BY_ID_ERROR:
      return {
        status: false,
        message: 'Erro'
      };
    default:
      return state;
  }
};

const isCreateProcessing = (state = { status: false, message: null }, action) => {
  const { type } = action;

  switch (type) {
    case CREATE_MEDICINES_REQUEST:
      return {
        status: true,
        message: null
      };
    case DELETE_MEDICINES_REQUEST:
    case GET_MEDICINES_BY_ID_REQUEST:
    case GET_MEDICINES_REQUEST:
    case UPDATE_MEDICINES_REQUEST:
      return {
        status: false,
        message: null
      };
    case CREATE_MEDICINES_SUCCESS:
      return {
        status: false,
        message: 'Sucesso'
      };
    case CREATE_MEDICINES_ERROR:
      return {
        status: false,
        message: 'Erro'
      };
    default:
      return state;
  }
};

const isUpdateProcessing = (state = { status: false, message: null }, action) => {
  const { type } = action;

  switch (type) {
    case UPDATE_MEDICINES_REQUEST:
      return {
        status: true,
        message: null
      };
    case DELETE_MEDICINES_REQUEST:
    case GET_MEDICINES_BY_ID_REQUEST:
    case GET_MEDICINES_REQUEST:
    case CREATE_MEDICINES_REQUEST:
      return {
        status: false,
        message: null
      };
    case UPDATE_MEDICINES_SUCCESS:
      return {
        status: false,
        message: 'Sucesso'
      };
    case UPDATE_MEDICINES_ERROR:
      return {
        status: false,
        message: 'Erro'
      };
    default:
      return state;
  }
};

const isDeleteProcessing = (state = { status: false, message: null }, action) => {
  const { type } = action;

  switch (type) {
    case DELETE_MEDICINES_REQUEST:
      return {
        status: true,
        message: null
      };
    case CREATE_MEDICINES_REQUEST:
    case GET_MEDICINES_BY_ID_REQUEST:
    case GET_MEDICINES_REQUEST:
    case UPDATE_MEDICINES_REQUEST:
      return {
        status: false,
        message: null
      }
    case DELETE_MEDICINES_SUCCESS:
      return {
        status: false,
        message: 'sucesso'
      };
    case DELETE_MEDICINES_ERROR:
      return {
        status: false,
        message: 'erro'
      };
    default:
      return state;
  }
};

//Reducers -> Manipulacao do estado
const allMedicines = (state = { data: [] }, action) => {
  const { type, payload } = action;

  switch (type) {
    case GET_MEDICINES_SUCCESS:
      return {
        data: payload.list,
        totalPages: payload.totalPages,
        currentPage: payload.currentPage,
        totalElements: payload.totalElements
      };
    default:
      return state;
  }
};

//Reducers -> Manipulacao do estado
const medicine = (state = { data: [] }, action) => {
  const { type, payload } = action;

  switch (type) {
    case GET_MEDICINES_BY_ID_SUCCESS:
      return {
        name: payload.name,
        composition: payload.composition,
        description: payload.description,
        format: payload.format,
        groupId: payload.groupId,
        image: payload.image,
        manufacturerId: payload.manufacturerId,
        referencePrice: payload.referencePrice,
      };
    default:
      return state;
  }
};

//Export All Reducers 
export default combineReducers({
  allMedicines,
  medicine,
  errorCreate,
  errorUpdate,
  errorDelete,
  errorGet,
  errorGetById,
  isGetProcessing,
  isCreateProcessing,
  isDeleteProcessing,
  isUpdateProcessing,
  isGetByIdProcessing
});

