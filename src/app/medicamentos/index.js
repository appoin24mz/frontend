import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  Row,
  Col,
  Table,
  Input,
  Button,
  Menu,
  Modal,
  Dropdown,
  PageHeader,
  Tabs,
  Spin,
  notification,
} from "antd";
import {
  EditOutlined,
  DeleteOutlined,
  PicLeftOutlined,
  DownOutlined,
  ExclamationCircleOutlined,
} from "@ant-design/icons";
import {
  useHistory,
} from "react-router-dom";

import {
  getData,
  deleteRow,
  getAllMedicines,
  getError,
  getDeleteError,
  getIsDeleteProcessing,
  getIsGetProcessing,
} from "./redux";

const { confirm } = Modal;

const Medicamentos = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  //Local State
  const [currentPageSize, setCurrentPageSize] = useState(10);
  const [medicines, setMedicines] = useState();
  const [searchVal, setSearchVal] = useState(null);

  //Store State 
  const allMedicines = useSelector(getAllMedicines);

  //Processing status
  const isProcessing = useSelector(getIsGetProcessing);
  const isDeleteProcessing = useSelector(getIsDeleteProcessing);

  //Error status
  const errorGet = useSelector(getError);
  const errorDelete = useSelector(getDeleteError);

  //Local Variables 
  const SUCESSO = 'sucesso';

  //useEffect Operations
  useEffect(() => {
    dispatch(getData({ page: "", searchTerm: "", size: "" }));
  }, []);

  //Get Processing 
  useEffect(() => {
    if (allMedicines !== undefined) {
      setMedicines(generateData(allMedicines.data));
    }
  }, [isProcessing, errorGet, allMedicines]);

  //Delete Processing 
  useEffect(() => {
    if (isDeleteProcessing.message !== null) {
      if (isDeleteProcessing.message.toLowerCase() == SUCESSO && !isDeleteProcessing.status) {
        openNotificationWithIcon('success', 'Sucesso', 'Registo Eliminado com Sucesso!');
        dispatch(getData({ page: "", searchTerm: "", size: "" }));
      } else {
        openNotificationWithIcon('error', 'Erro', '' + errorDelete.message);
      }
    }
  }, [isDeleteProcessing, errorDelete]);

  //Local Functions 
  const handleMenuOption = (record, key) => {
    if (key.key === "0") {
      handleUpdate(record.key);
    } else if (key.key === "1") {
      handleDetails(record.key);
      // console.log(record);
    } else if (key.key === "2") {
      handleDelete(record.key);
    }
  };

  const handleUpdate = (id) => {
    history.push(`/app/alterar-medicamento/${id}`);
  };

  const handleDetails = (id) => {
    history.push(`/app/detalhes-medicamento/${id}`);
  };

  const handleDelete = (id) => {
    showDeleteConfirm(id);
  };

  const showDeleteConfirm = (id) => {
    confirm({
      title: 'Tem certeza que quer APAGAR/EXCLUIR esse Registo?',
      icon: <ExclamationCircleOutlined />,
      content: '',
      okText: 'Sim',
      okType: 'danger',
      cancelText: 'Não',
      onOk() {
        console.log('OK');
        dispatch(
          deleteRow({
            code: id,
          })
        );
      },
      onCancel() {
        console.log('Cancelado');
      },
    });
  }

  const menu = (record) => {
    return (
      <Menu
        className="ccc-dropdown-menu"
        onClick={(key) => handleMenuOption(record, key)}
      >
        <Menu.Item key="0">
          <EditOutlined /> Alterar
        </Menu.Item>
        <Menu.Divider />
        <Menu.Item key="1">
          <PicLeftOutlined /> Detalhar
        </Menu.Item>
        <Menu.Divider />
        <Menu.Item className="ccc-delete-item" key="2">
          <DeleteOutlined /> Excluir
        </Menu.Item>
      </Menu>
    );
  };

  const columns = [
    {
      title: "Nome",
      dataIndex: "generic_name",
      key: "generic_name",
      width: "20%",
      sorter: (a, b) => a.generic_name.length - b.generic_name.length,
      ellipsis: true,
    },
    {
      title: "Forma",
      dataIndex: "dosage_form",
      key: "dosage_form",
      width: "20%",
      sorter: (a, b) => a.dosage_form.length - b.dosage_form.length,
      ellipsis: true,
    },
    {
      title: "Dosagem",
      dataIndex: "volume",
      key: "volume",
      width: "20%",
      sorter: (a, b) => a.volume.length - b.volume.length,
      ellipsis: true,
    },
    {
      title: "Grupo",
      dataIndex: "group_name",
      key: "group_name",
      width: "15%",
      sorter: (a, b) => a.group_name.length - b.group_name.length,
      ellipsis: true,
    },
    {
      title: "Preço de Referência",
      dataIndex: "reference_price",
      key: "reference_price",
      width: "15%",
      sorter: (a, b) => a.reference_price.length - b.reference_price.length,
      ellipsis: true,
    },
    {
      title: "",
      dataIndex: "accoes",
      key: "accoes",
      render: (text, record) => {
        //console.log(record.key);
        return (
          <Dropdown overlay={menu(record)} trigger={["click"]}>
            <Button className="ccc-btn-sec" onClick={(e) => e.preventDefault()}>
              Acções <DownOutlined />
            </Button>
          </Dropdown>
        );
      },
    },
  ];

  const generateData = (data) => {
    const generatedData =
    data &&
    data.map((item) => {
        return {
          key: item.id,
          generic_name: item.genericName,
          dosage_form: item.dosageForm,
          volume: item.volume,
          group_name: item.group.name,
          reference_price: item.referencePrice.amount + ' ' + item.referencePrice.currency,
          accoes: "",
        };
      });

    return generatedData;
  };

  const onShowSizeChange = (current, pageSize) => {
    setCurrentPageSize(pageSize);
    console.log('Current: ', currentPageSize);
    console.log(current, pageSize);
  };

  const openNotificationWithIcon = (tipo, titulo, mensagem) => {
    notification[tipo]({
      message: titulo,
      description: mensagem
    });
  };

  const getFiltredResult = (e) => {
    console.log('FILTRED VALUE: ',e);
    dispatch(getData({ page: "", searchTerm: e, size: "" }));
  };

  return (
    <Row className="ccc-layout-content-general">
      <Col span={24}>
        <Spin spinning={isDeleteProcessing.status} tip="Eliminando o Registo...">
          <PageHeader
            className="site-page-header-responsive"
            onBack={() => window.history.back()}
            title="Medicamentos"
            subTitle="Listagem de Medicamentos"
            extra={[
              <>
                <Input.Search
                  placeholder="Procurar Medicamento"
                  onSearch={getFiltredResult}
                  onChange={(e) => setSearchVal(e.target.value)}
                />
                <Button
                  type="primary"
                  onClick={() => history.push(`/app/novo-medicamento`)}
                >
                  Novo Medicamento
                </Button>
              </>,
            ]}
            footer={<Tabs defaultActiveKey="1"></Tabs>}
          >
            <Row>
              <Col span={24}>
                <Table
                  columns={columns}
                  pagination={{
                    pageSize: currentPageSize,
                    total: allMedicines.totalElements,
                    showTotal: (total) => `Total ${total} Registos`,
                    defaultCurrent: 1,
                    pageSizeOptions: [5, 10, 25, 50, 100],
                    onShowSizeChange: (defaultCurrent, pageSize) => onShowSizeChange(defaultCurrent, pageSize),
                    showSizeChanger: true,
                  }}
                  dataSource={medicines}
                  size="small"
                  loading={isProcessing.status}
                />
              </Col>
            </Row>
          </PageHeader>
        </Spin>
      </Col>
    </Row>
  );
};

export default Medicamentos;
