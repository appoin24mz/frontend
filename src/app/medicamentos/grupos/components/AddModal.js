import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  Input,
  Row,
  Col,
  Button,
  Form,
  Spin,
  notification,
  Modal,
  Skeleton,
  Switch,
  Select,
} from "antd";
import { CloseOutlined, SaveOutlined } from "@ant-design/icons";
import {
  saveData,
  getIsCreateProcessing,
  getCreateError,
  getData,
} from "../redux";
import styled from "styled-components";

const { Option } = Select;

const AddModal = ({ isAddModalVisible, handleCancel }) => {
  const dispatch = useDispatch();

  const isSaveProcessing = useSelector(getIsCreateProcessing);
  const errorCreate = useSelector(getCreateError);

  //Local State
  const [confirmLoading, setConfirmLoading] = useState(false);

  const onFinish = (formValues) => {
    setConfirmLoading(true);
    console.log("Valores: ", formValues);
    dispatch(
      saveData({
        name: formValues.name,
        description: formValues.description,
      })
    );
  };

  useEffect(() => {
    if (isSaveProcessing.message !== null) {
      if (
        isSaveProcessing.message.toLowerCase() == "Sucesso".toLowerCase() &&
        !isSaveProcessing.status
      ) {
        openNotificationWithIcon("success", "Sucesso", "Inserido com Sucesso!");
        setConfirmLoading(false);
        handleCancel();
        dispatch(getData({ page: "", searchTerm: "", size: "" }));
      } else {
        openNotificationWithIcon("error", "Erro", "" + errorCreate);
      }
    }
  }, [isSaveProcessing, errorCreate]);

  const openNotificationWithIcon = (tipo, titulo, mensagem) => {
    notification[tipo]({
      message: titulo,
      description: mensagem,
    });
  };

  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 8 },
      md: { span: 8 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 16 },
      md: { span: 16 },
    },
  };

  return (
    <Modal
      width={900}
      title={"Adicionar Grupo de Medicamentos"}
      closable={false}
      visible={isAddModalVisible}
      confirmLoading={confirmLoading}
      footer={null}
    >
      <Skeleton loading={false} active>
        <Row>
          <Col span={24}>
            <Spin
              spinning={isSaveProcessing.status}
              delay={500}
              size="large"
              tip="Guardando os Dados..."
            >
              <FormStyled className="ccc-form">
                <Form
                  {...formItemLayout}
                  name="add-groups"
                  initialValues={{ remember: true }}
                  onFinish={onFinish}
                >
                  <Row gutter={48}>
                    <Col span={12} offset={4}>
                      <Form.Item
                        name="name"
                        label="Nome"
                        rules={[
                          {
                            required: true,
                            message: "Por favor informe o Nome do Grupo!",
                          },
                        ]}
                      >
                        <Input
                          placeholder="Nome do Grupo"
                          className="text-center"
                        />
                      </Form.Item>

                      <Form.Item
                        name="description"
                        label="Descrição"
                        rules={[
                          {
                            required: true,
                            message: "Por favor informe a Descrição!",
                          },
                        ]}
                      >
                        <Input
                          placeholder="Descrição"
                          className="text-center"
                        />
                      </Form.Item>
                    </Col>
                  </Row>

                  <Row>
                    <Col span={24}>
                      <div className="separator"></div>
                    </Col>
                  </Row>
                  <Row>
                    <Col span={8} offset={8}>
                      <Form.Item className="botoes">
                        <Button
                          type="primary"
                          htmlType="button"
                          className=""
                          icon={<CloseOutlined />}
                          danger
                          onClick={handleCancel}
                        >
                          Cancelar
                        </Button>
                        <Button
                          type="primary"
                          htmlType="submit"
                          className=""
                          icon={<SaveOutlined />}
                          loading={isSaveProcessing.status}
                        >
                          Gravar
                        </Button>
                      </Form.Item>
                    </Col>
                  </Row>
                </Form>
              </FormStyled>
            </Spin>
          </Col>
        </Row>
      </Skeleton>
    </Modal>
  );
};

const FormStyled = styled.div`
  .botoes .ant-form-item-control-input-content {
    display: flex;
    justify-content: center;
    align-items: center;
  }

  button {
    margin-left: 20px;
  }

  .separator {
    border-bottom: 1px solid #e4e2e2;
    margin: 20px 0px;
  }

  .upload-column {
    display: flex;
    justify-content: center;
    align-items: center;
  }

  .ant-upload-picture-card-wrapper {
    width: auto;
  }

  .ant-upload.ant-upload-select-picture-card {
    text-align: center;
    width: 100%;
    height: 100%;
    vertical-align: top;
    background-color: unset;
    border: none;
  }

  .ant-upload.ant-upload-select-picture-card > .ant-upload {
    flex-direction: column;
  }

  .preview-uploaded-image {
    height: 150px;
    width: 150px;
    border: 1px solid #bbb6b6;
    border-radius: 6px;
    margin: 20px;
    overflow: hidden;
  }
`;

export default AddModal;
