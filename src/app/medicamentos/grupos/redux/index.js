import { combineReducers } from "redux";
import API from "../../../shared/api";

//Actions Creators 
//Post Request
export const saveData = ({ name, description}) => async (dispatch) => {
  dispatch({ type: CREATE_MEDICINES_GROUP_REQUEST });
  try {
    const response = await API.post("/medicine/groups/register", { name, description });
    dispatch({ type: CREATE_MEDICINES_GROUP_SUCCESS, payload: response });
  } catch (error) {
    dispatch({ type: CREATE_MEDICINES_GROUP_ERROR, payload: error });
  }
};

//Update Request
export const updateRow = ({ code, name, description, status }) => async (dispatch) => {
  dispatch({ type: UPDATE_MEDICINES_GROUP_REQUEST });
  try {
    const response = await API.post(`/medicine/groups/${code}/update`, { name, description, status });
    dispatch({ type: UPDATE_MEDICINES_GROUP_SUCCESS, payload: response });
  } catch (error) {
    dispatch({ type: UPDATE_MEDICINES_GROUP_ERROR, payload: error });
  }
};

//Delete Request
export const deleteRow = ({ code }) => async (dispatch) => {
  dispatch({ type: DELETE_MEDICINES_GROUP_REQUEST });

  try {
    const response = await API.post(`/medicine/groups/${code}/delete`);
    dispatch({ type: DELETE_MEDICINES_GROUP_SUCCESS, payload: response });
  } catch (error) {
    dispatch({ type: DELETE_MEDICINES_GROUP_ERROR, payload: error });
  }
};

//Get Request
export const getData = ({ page, searchTerm, size }) => async (dispatch) => {
  dispatch({ type: GET_MEDICINES_GROUP_REQUEST });

  try {
    const response = await API.get("/medicine/groups", { page, searchTerm, size });
    dispatch({ type: GET_MEDICINES_GROUP_SUCCESS, payload: response });
  } catch (error) {
    dispatch({ type: GET_MEDICINES_GROUP_ERROR, payload: error });
  }
};

//Get by ID Request
export const getDataById = (code) => async (dispatch) => {
  dispatch({ type: GET_MEDICINES_GROUP_BY_ID_REQUEST });

  try {
    const response = await API.get(`/medicine/groups/${code}`);
    dispatch({ type: GET_MEDICINES_GROUP_BY_ID_SUCCESS, payload: response });
  } catch (error) {
    dispatch({ type: GET_MEDICINES_GROUP_BY_ID_ERROR, payload: error });
  }
};

//Selectors 
export const getAllMedicinesGroup = state => state.medicineGroups.allMedicinesGroup;
export const getMedicineGroup = state => state.medicineGroups.medicineGroup;
export const getIsDeleteProcessing = state => state.medicineGroups.isDeleteProcessing;
export const getIsGetProcessing = state => state.medicineGroups.isGetProcessing;
export const getIsGetByIdProcessing = state => state.medicineGroups.isGetByIdProcessing;
export const getIsCreateProcessing = state => state.medicineGroups.isCreateProcessing;
export const getIsUpdateProcessing = state => state.medicineGroups.isUpdateProcessing;
export const getError = state => state.medicineGroups.errorGet;
export const getByIdError = state => state.medicineGroups.errorGetById;
export const getCreateError = state => state.medicineGroups.errorCreate;
export const getDeleteError = state => state.medicineGroups.errorDelete;
export const getUpdateError = state => state.medicineGroups.errorUpdate;

// ---------------------------
//Accoes
//Requests
const GET_MEDICINES_GROUP_REQUEST = "GET_MEDICINES_GROUP_REQUEST";
const GET_MEDICINES_GROUP_BY_ID_REQUEST = "GET_MEDICINES_GROUP_BY_ID_REQUEST";
const CREATE_MEDICINES_GROUP_REQUEST = "CREATE_MEDICINES_GROUP_REQUEST";
const DELETE_MEDICINES_GROUP_REQUEST = "DELETE_MEDICINES_GROUP_REQUEST";
const UPDATE_MEDICINES_GROUP_REQUEST = "UPDATE_MEDICINES_GROUP_REQUEST";
//Erros
const GET_MEDICINES_GROUP_ERROR = "GET_MEDICINES_GROUP_ERROR";
const GET_MEDICINES_GROUP_BY_ID_ERROR = "GET_MEDICINES_GROUP_BY_ID_ERROR";
const CREATE_MEDICINES_GROUP_ERROR = "CREATE_MEDICINES_GROUP_ERROR";
const UPDATE_MEDICINES_GROUP_ERROR = "UPDATE_MEDICINES_GROUP_ERROR";
const DELETE_MEDICINES_GROUP_ERROR = "DELETE_MEDICINES_GROUP_ERROR";

//Sucesso
const CREATE_MEDICINES_GROUP_SUCCESS = "CREATE_MEDICINES_GROUP_SUCCESS";
const GET_MEDICINES_GROUP_SUCCESS = "GET_MEDICINES_GROUP_SUCCESS";
const GET_MEDICINES_GROUP_BY_ID_SUCCESS = "GET_MEDICINES_GROUP_BY_ID_SUCCESS";
const DELETE_MEDICINES_GROUP_SUCCESS = "DELETE_MEDICINES_GROUP_SUCCESS";
const UPDATE_MEDICINES_GROUP_SUCCESS = "UPDATE_MEDICINES_GROUP_SUCCESS";

// ---------------------------

//Reducers -> Mensagens de Erro

const errorCreate = (state = null, action) => {
  const { type, payload } = action;

  switch (type) {
    case CREATE_MEDICINES_GROUP_ERROR:
      return { message: payload.message, httpcode: payload.httpcode };
    default:
      return state;
  }
};

const errorUpdate = (state = null, action) => {
  const { type, payload } = action;

  switch (type) {
    case UPDATE_MEDICINES_GROUP_ERROR:
      return { message: payload.message, httpcode: payload.httpcode };
    default:
      return state;
  }
};

const errorDelete = (state = null, action) => {
  const { type, payload } = action;

  switch (type) {
    case DELETE_MEDICINES_GROUP_ERROR:
      return { message: payload.message, httpcode: payload.httpcode };
    default:
      return state;
  }
};

const errorGet = (state = null, action) => {
  const { type, payload } = action;

  switch (type) {
    case GET_MEDICINES_GROUP_ERROR:
      return { message: payload.message, httpcode: payload.httpcode };
    default:
      return state;
  }
};

const errorGetById = (state = null, action) => {
  const { type, payload } = action;

  switch (type) {
    case GET_MEDICINES_GROUP_BY_ID_ERROR:
      return { message: payload.message, httpcode: payload.httpcode };
    default:
      return state;
  }
};

const isGetProcessing = (state = { status: false, message: null }, action) => {
  const { type } = action;

  switch (type) {
    case GET_MEDICINES_GROUP_REQUEST:
      return {
        status: true,
        message: null
      };
    case UPDATE_MEDICINES_GROUP_REQUEST:
    case DELETE_MEDICINES_GROUP_REQUEST:
    case GET_MEDICINES_GROUP_BY_ID_REQUEST:
    case CREATE_MEDICINES_GROUP_REQUEST:
      return {
        status: false,
        message: null
      };
    case GET_MEDICINES_GROUP_SUCCESS:
      return {
        status: false,
        message: 'Sucesso'
      };
    case GET_MEDICINES_GROUP_ERROR:
      return {
        status: false,
        message: 'Erro'
      };
    default:
      return state;
  }
};

const isGetByIdProcessing = (state = { status: false, message: null }, action) => {
  const { type } = action;

  switch (type) {
    case GET_MEDICINES_GROUP_BY_ID_REQUEST:
      return {
        status: true,
        message: null
      };
    case UPDATE_MEDICINES_GROUP_REQUEST:
    case DELETE_MEDICINES_GROUP_REQUEST:
    case GET_MEDICINES_GROUP_REQUEST:
    case CREATE_MEDICINES_GROUP_REQUEST:
      return {
        status: false,
        message: null
      };
    case GET_MEDICINES_GROUP_BY_ID_SUCCESS:
      return {
        status: false,
        message: 'Sucesso'
      };
    case GET_MEDICINES_GROUP_BY_ID_ERROR:
      return {
        status: false,
        message: 'Erro'
      };
    default:
      return state;
  }
};

const isCreateProcessing = (state = { status: false, message: null }, action) => {
  const { type } = action;

  switch (type) {
    case CREATE_MEDICINES_GROUP_REQUEST:
      return {
        status: true,
        message: null
      };
    case DELETE_MEDICINES_GROUP_REQUEST:
    case GET_MEDICINES_GROUP_BY_ID_REQUEST:
    case GET_MEDICINES_GROUP_REQUEST:
    case UPDATE_MEDICINES_GROUP_REQUEST:
      return {
        status: false,
        message: null
      };
    case CREATE_MEDICINES_GROUP_SUCCESS:
      return {
        status: false,
        message: 'Sucesso'
      };
    case CREATE_MEDICINES_GROUP_ERROR:
      return {
        status: false,
        message: 'Erro'
      };
    default:
      return state;
  }
};

const isUpdateProcessing = (state = { status: false, message: null }, action) => {
  const { type } = action;

  switch (type) {
    case UPDATE_MEDICINES_GROUP_REQUEST:
      return {
        status: true,
        message: null
      };
    case DELETE_MEDICINES_GROUP_REQUEST:
    case GET_MEDICINES_GROUP_BY_ID_REQUEST:
    case GET_MEDICINES_GROUP_REQUEST:
    case CREATE_MEDICINES_GROUP_REQUEST:
      return {
        status: false,
        message: null
      };
    case UPDATE_MEDICINES_GROUP_SUCCESS:
      return {
        status: false,
        message: 'Sucesso'
      };
    case UPDATE_MEDICINES_GROUP_ERROR:
      return {
        status: false,
        message: 'Erro'
      };
    default:
      return state;
  }
};

const isDeleteProcessing = (state = { status: false, message: null }, action) => {
  const { type } = action;

  switch (type) {
    case DELETE_MEDICINES_GROUP_REQUEST:
      return {
        status: true,
        message: null
      };
    case CREATE_MEDICINES_GROUP_REQUEST:
    case GET_MEDICINES_GROUP_BY_ID_REQUEST:
    case GET_MEDICINES_GROUP_REQUEST:
    case UPDATE_MEDICINES_GROUP_REQUEST:
      return {
        status: false,
        message: null
      }
    case DELETE_MEDICINES_GROUP_SUCCESS:
      return {
        status: false,
        message: 'sucesso'
      };
    case DELETE_MEDICINES_GROUP_ERROR:
      return {
        status: false,
        message: 'erro'
      };
    default:
      return state;
  }
};

//Reducers -> Manipulacao do estado
const allMedicinesGroup = (state = { data: [] }, action) => {
  const { type, payload } = action;

  switch (type) {
    case GET_MEDICINES_GROUP_SUCCESS:
      return {
        data: payload.list,
        totalPages: payload.totalPages,
        currentPage: payload.currentPage,
        totalElements: payload.totalElements
      };
    default:
      return state;
  }
};

//Reducers -> Manipulacao do estado
const medicineGroup = (state = { metadata: [] }, action) => {
  const { type, payload } = action;

  switch (type) {
    case GET_MEDICINES_GROUP_BY_ID_SUCCESS:
      return {
        id: payload.id,
        name: payload.name,
        description: payload.description,
        status: payload.status,
        metadata: payload.metadata,
      };
    default:
      return state;
  }
};

//Export All Reducers 
export default combineReducers({
  allMedicinesGroup,
  medicineGroup,
  errorCreate,
  errorUpdate,
  errorDelete,
  errorGet,
  errorGetById,
  isGetProcessing,
  isCreateProcessing,
  isDeleteProcessing,
  isUpdateProcessing,
  isGetByIdProcessing
});

