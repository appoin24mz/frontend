import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  Row,
  Col,
  Form,
  notification,
  Modal,
  Skeleton,
  Statistic,
  Descriptions
} from "antd";
import {
  CloseOutlined,
  SaveOutlined,
  PieChartOutlined,
  HddOutlined,
  TeamOutlined,
  SettingOutlined,
  UsergroupAddOutlined,
  SyncOutlined,
  LineChartOutlined,
} from "@ant-design/icons";
import {
  getMenu,
  getDataById,
  getIsGetByIdProcessing,
  getByIdError,
} from "../redux";

const DetailsModal = ({ isDetailModalVisible, handleCancel, id }) => {
  const dispatch = useDispatch();

  const [form] = Form.useForm();

  //Local Variables 
  const SUCESSO = 'sucesso';

  //Store State 
  const menu = useSelector(getMenu);

  //Processing status
  const isGetByIdProcessing = useSelector(getIsGetByIdProcessing);

  //Error status
  const errorGetById = useSelector(getByIdError);

  //useEffect Operations
  useEffect(() => {
    dispatch(getDataById(id));
  }, []);

  useEffect(() => {
    if (isGetByIdProcessing.message !== null) {
      if (isGetByIdProcessing.message.toLowerCase() == SUCESSO && !isGetByIdProcessing.status) {
        console.info('Carregado com sucesso');
      } else {
        openNotificationWithIcon('error', 'Erro', '' + errorGetById.message);
      }
    }
  }, [isGetByIdProcessing, errorGetById]);

  //Local Functions
  const openNotificationWithIcon = (tipo, titulo, mensagem) => {
    notification[tipo]({
      message: titulo,
      description: mensagem,
    });
  };

  const drawIcon = (icon) =>{
    let iconFormed = "<"+icon+" />";
    return iconFormed;
  }

  return (
    <Modal
      title={"Detalhes do Menu"}
      closable={true}
      onCancel={handleCancel}
      visible={isDetailModalVisible}
      footer={null}
    >
      <Skeleton loading={isGetByIdProcessing.status} active>
        <Row>
          <Col span={24}>
          <Skeleton loading={isGetByIdProcessing.status} active>
                <div className="content">
                  <div className="extra">
                    <div
                      style={{
                        display: "flex",
                        width: "max-content",
                        justifyContent: "flex-end",
                      }}
                    >
                      <Statistic
                        title="Menu"
                        value={menu.title}
                        style={{
                          marginRight: 32,
                        }}
                      />
                      <Statistic title="Icon" prefix={ drawIcon(menu.icon) } value={ menu.icon } />
                    </div>
                  </div>
                  <div className="main">
                    <Descriptions size="small" column={1}>
                      <Descriptions.Item label="Caminho do Menu">
                        {menu.path}
                      </Descriptions.Item>
                      <Descriptions.Item label="Menu Raiz">
                        {menu.parentId}
                      </Descriptions.Item>
                      <Descriptions.Item label="Tem Pai?">
                        {menu.hasParent}
                      </Descriptions.Item>
                    </Descriptions>
                  </div>
                </div>
              </Skeleton>
          </Col>
        </Row>
      </Skeleton>
    </Modal>
  );
};

export default DetailsModal;
