import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  Row,
  Col,
  Table,
  Input,
  Button,
  Menu,
  Modal,
  Dropdown,
  PageHeader,
  Tabs,
  Spin,
  notification,
} from "antd";
import {
  EditOutlined,
  DeleteOutlined,
  PicLeftOutlined,
  DownOutlined,
  ExclamationCircleOutlined,
} from "@ant-design/icons";
import { useHistory } from "react-router-dom";

import {
  getData,
  deleteRow,
  getAllMenus,
  getError,
  getDeleteError,
  getIsDeleteProcessing,
  getIsGetProcessing,
} from "./redux";
import AddModal from "./components/AddModal";
import EditModal from "./components/EditModal";
import DetailsModal from "./components/DetailsModal";

const { confirm } = Modal;

const Menus = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  //Local State
  const [currentPageSize, setCurrentPageSize] = useState(5);
  const [menus, setMenus] = useState();
  const [searchVal, setSearchVal] = useState(null);
  const [isDetailModalVisible, setIsDetailModalVisible] = useState(false);
  const [isEditModalVisible, setIsEditModalVisible] = useState(false);
  const [isAddModalVisible, setIsAddModalVisible] = useState(false);

  //Store State
  const allMenus = useSelector(getAllMenus);

  //Processing status
  const isProcessing = useSelector(getIsGetProcessing);
  const isDeleteProcessing = useSelector(getIsDeleteProcessing);

  //Error status
  const errorGet = useSelector(getError);
  const errorDelete = useSelector(getDeleteError);

  //Local Variables
  const SUCESSO = "sucesso";
  const ADD = "ADD";
  const EDIT = "EDIT";
  const DETAILS = "DETAILS";

  //useEffect Operations
  useEffect(() => {
    dispatch(getData({ page: "", searchTerm: "", size: "" }));
  }, []);

  //Get Processing
  useEffect(() => {
    if (isProcessing.message !== null) {
      if (
        isProcessing.message.toLowerCase() == SUCESSO &&
        !isProcessing.status
      ) {
        console.info("Carregado com sucesso");
        setMenus(generateMenu(allMenus.data));
        console.log("Data: ", generateMenu(allMenus.data));
      } else {
        openNotificationWithIcon("error", "Erro", "" + errorGet.message);
      }
    }
  }, [isProcessing, errorGet]);

  //Delete Processing
  useEffect(() => {
    if (isDeleteProcessing.message !== null) {
      if (
        isDeleteProcessing.message.toLowerCase() == SUCESSO &&
        !isDeleteProcessing.status
      ) {
        openNotificationWithIcon(
          "success",
          "Sucesso",
          "Registo Eliminado com Sucesso!"
        );
        dispatch(getData({ page: "", searchTerm: "", size: "" }));
      } else {
        openNotificationWithIcon("error", "Erro", "" + errorDelete.message);
      }
    }
  }, [isDeleteProcessing, errorDelete]);

  //Local Functions
  const handleMenuOption = (record, key) => {
    if (key.key === "0") {
      handleUpdate(record.key);
    } else if (key.key === "1") {
      handleDetails(record.key);
      // console.log(record);
    } else if (key.key === "2") {
      handleDelete(record.key);
    }
  };

  const handleUpdate = (id) => {
    showModal(EDIT);
  };

  const handleDetails = (id) => {
    showModal(DETAILS);
  };

  const handleDelete = (id) => {
    showDeleteConfirm(id);
  };

  const showDeleteConfirm = (id) => {
    confirm({
      title: "Tem certeza que quer APAGAR/EXCLUIR esse Registo?",
      icon: <ExclamationCircleOutlined />,
      content: "",
      okText: "Sim",
      okType: "danger",
      cancelText: "Não",
      onOk() {
        console.log("OK");
        dispatch(
          deleteRow({
            code: id,
          })
        );
      },
      onCancel() {
        console.log("Cancelado");
      },
    });
  };

  const menu = (record) => {
    return (
      <Menu
        className="ccc-dropdown-menu"
        onClick={(key) => handleMenuOption(record, key)}
      >
        <Menu.Item key="0">
          <EditOutlined /> Alterar
        </Menu.Item>
        <Menu.Divider />
        <Menu.Item key="1">
          <PicLeftOutlined /> Detalhar
        </Menu.Item>
        <Menu.Divider />
        <Menu.Item className="ccc-delete-item" key="2">
          <DeleteOutlined /> Excluir
        </Menu.Item>
      </Menu>
    );
  };

  const columns = [
    {
      title: "Menu",
      dataIndex: "title",
      key: "title",
      width: "25%",
      sorter: (a, b) => a.title.length - b.title.length,
      ellipsis: true,
    },
    {
      title: "Caminho",
      dataIndex: "path",
      key: "path",
      width: "30%",
      sorter: (a, b) => a.path.length - b.path.length,
      ellipsis: true,
    },
    {
      title: "Principal",
      dataIndex: "has_parent",
      key: "has_parent",
      width: "10%",
      sorter: (a, b) => a.has_parent.length - b.has_parent.length,
      ellipsis: true,
    },
    {
      title: "Menu Raiz",
      dataIndex: "parent_id",
      key: "parent_id",
      width: "25%",
      sorter: (a, b) => a.parent_id.length - b.parent_id.length,
      ellipsis: true,
    },
    {
      title: "",
      dataIndex: "accoes",
      key: "accoes",
      render: (text, record) => {
        //console.log(record.key);
        return (
          <Dropdown overlay={menu(record)} trigger={["click"]}>
            <Button className="ccc-btn-sec" onClick={(e) => e.preventDefault()}>
              Acções <DownOutlined />
            </Button>
          </Dropdown>
        );
      },
    },
  ];

  const generateMenu = (menus) => {
    const generatedMenus =
      menus &&
      menus.map((item) => {
        return {
          key: item.id,
          has_parent: item.hasParent ? "SIM" : "NÃO",
          parent_id: item.parentId != null ? "Sem Menu Raiz" : item.parentId,
          title: item.title,
          path: item.path,
          accoes: "",
        };
      });

    return generatedMenus;
  };

  const onShowSizeChange = (current, pageSize) => {
    setCurrentPageSize(pageSize);
    console.log("Current: ", currentPageSize);
    console.log(current, pageSize);
  };

  const openNotificationWithIcon = (tipo, titulo, mensagem) => {
    notification[tipo]({
      message: titulo,
      description: mensagem,
    });
  };

  const getFiltredResult = (e) => {
    console.log("FILTRED VALUE: ", e);
    dispatch(getData({ page: "", searchTerm: e, size: "" }));
  };

  const showModal = (type) => {
    console.log('Tipo: ', type);
    //dispatch(getDataById(id));
    if (type == EDIT) {
      setIsEditModalVisible(true);
    } else if (type == ADD) {
      setIsAddModalVisible(true);
    } else if (type == DETAILS) {
      setIsDetailModalVisible(true);
    }
  };

  const handleCancel = () => {
    setIsDetailModalVisible(false);
    setIsAddModalVisible(false);
    setIsEditModalVisible(false);
  };

  return (
    <Row className="ccc-layout-content-general">
      <Col span={24}>
        <Spin
          spinning={isDeleteProcessing.status}
          tip="Eliminando o Registo..."
        >
          <PageHeader
            className="site-page-header-responsive"
            onBack={() => window.history.back()}
            title="Menus"
            subTitle="Listagem de Menus"
            extra={[
              <>
                <Input.Search
                  placeholder="Procurar Menu"
                  onSearch={getFiltredResult}
                  onChange={(e) => setSearchVal(e.target.value)}
                />
                <Button type="primary" onClick={() => showModal(ADD)}>
                  Novo Menu
                </Button>
              </>,
            ]}
            footer={<Tabs defaultActiveKey="1"></Tabs>}
          >
            <Row>
              <Col span={24}>
                <Table
                  columns={columns}
                  pagination={{
                    pageSize: currentPageSize,
                    total: allMenus.totalElements,
                    showTotal: (total) => `Total ${total} Registos`,
                    defaultCurrent: 1,
                    pageSizeOptions: [5, 10, 25, 50, 100],
                    onShowSizeChange: (defaultCurrent, pageSize) =>
                      onShowSizeChange(defaultCurrent, pageSize),
                    showSizeChanger: true,
                  }}
                  dataSource={menus}
                  size="small"
                  loading={isProcessing.status}
                />
              </Col>
              <Col>
                <AddModal
                  handleCancel={handleCancel}
                  menus={menus}
                  isAddModalVisible={isAddModalVisible}
                />
                <EditModal
                  handleCancel={handleCancel}
                  id="3ff0ed2a-30a9-4877-b596-ec1a63e7ce35"
                  menus={menus}
                  isEditModalVisible={isEditModalVisible}
                />
                <DetailsModal
                  handleCancel={handleCancel}
                  id="3ff0ed2a-30a9-4877-b596-ec1a63e7ce35"
                  isDetailModalVisible={isDetailModalVisible}
                />
              </Col>
            </Row>
          </PageHeader>
        </Spin>
      </Col>
    </Row>
  );
};

export default Menus;
