import { combineReducers } from "redux";
import API from "../../../shared/api";

//Actions Creators 
//Post Request
export const saveData = ({ iconForm, parentIdForm, path, title }) => async (dispatch) => {
  dispatch({ type: CREATE_MENU_REQUEST });
  let parentId = parentIdForm == undefined ? 1 : parentIdForm;
  let icon = iconForm == undefined ? null : iconForm;
  try {
    const response = await API.post("/menus/create", { icon, parentId, path, title });
    dispatch({ type: CREATE_MENU_SUCCESS, payload: response });
  } catch (error) {
    dispatch({ type: CREATE_MENU_ERROR, payload: error });
  }
};

//Update Request
export const updateRow = ({ code, iconForm, parentIdForm, path, title }) => async (dispatch) => {
  dispatch({ type: UPDATE_MENU_REQUEST });
  let parentId = parentIdForm == undefined ? 1 : parentIdForm;
  let icon = iconForm == undefined ? null : iconForm;
  try {
    const response = await API.post(`/menus/${code}/update`, { icon, parentId, path, title });
    dispatch({ type: UPDATE_MENU_SUCCESS, payload: response });
  } catch (error) {
    dispatch({ type: UPDATE_MENU_ERROR, payload: error });
  }
};

//Delete Request
export const deleteRow = ({ code }) => async (dispatch) => {
  dispatch({ type: DELETE_MENU_REQUEST });

  try {
    const response = await API.post(`/menus/${code}/delete`);
    dispatch({ type: DELETE_MENU_SUCCESS, payload: response });
  } catch (error) {
    dispatch({ type: DELETE_MENU_ERROR, payload: error });
  }
};

//Get Request
export const getData = ({ page, searchTerm, size }) => async (dispatch) => {
  dispatch({ type: GET_MENU_REQUEST });

  try {
    const response = await API.get("/menus", { page, searchTerm, size });
    dispatch({ type: GET_MENU_SUCCESS, payload: response });
  } catch (error) {
    dispatch({ type: GET_MENU_ERROR, payload: error });
  }
};

//Get by ID Request
export const getDataById = (code) => async (dispatch) => {
  dispatch({ type: GET_MENU_BY_ID_REQUEST });

  try {
    const response = await API.get(`/menus/${code}`);
    dispatch({ type: GET_MENU_BY_ID_SUCCESS, payload: response });
  } catch (error) {
    dispatch({ type: GET_MENU_BY_ID_ERROR, payload: error });
  }
};

//Selectors 
export const getAllMenus = state => state.menus.allMenus;
export const getMenu = state => state.menus.menu;
export const getIsDeleteProcessing = state => state.menus.isDeleteProcessing;
export const getIsGetProcessing = state => state.menus.isGetProcessing;
export const getIsGetByIdProcessing = state => state.menus.isGetByIdProcessing;
export const getIsCreateProcessing = state => state.menus.isCreateProcessing;
export const getIsUpdateProcessing = state => state.menus.isUpdateProcessing;
export const getError = state => state.menus.errorGet;
export const getByIdError = state => state.menus.errorGetById;
export const getCreateError = state => state.menus.errorCreate;
export const getDeleteError = state => state.menus.errorDelete;
export const getUpdateError = state => state.menus.errorUpdate;

// ---------------------------
//Accoes
//Requests
const GET_MENU_REQUEST = "GET_MENU_REQUEST";
const GET_MENU_BY_ID_REQUEST = "GET_MENU_BY_ID_REQUEST";
const CREATE_MENU_REQUEST = "CREATE_MENU_REQUEST";
const DELETE_MENU_REQUEST = "DELETE_MENU_REQUEST";
const UPDATE_MENU_REQUEST = "UPDATE_MENU_REQUEST";
//Erros
const GET_MENU_ERROR = "GET_MENU_ERROR";
const GET_MENU_BY_ID_ERROR = "GET_MENU_BY_ID_ERROR";
const CREATE_MENU_ERROR = "CREATE_MENU_ERROR";
const UPDATE_MENU_ERROR = "UPDATE_MENU_ERROR";
const DELETE_MENU_ERROR = "DELETE_MENU_ERROR";

//Sucesso
const CREATE_MENU_SUCCESS = "CREATE_MENU_SUCCESS";
const GET_MENU_SUCCESS = "GET_MENU_SUCCESS";
const GET_MENU_BY_ID_SUCCESS = "GET_MENU_BY_ID_SUCCESS";
const DELETE_MENU_SUCCESS = "DELETE_MENU_SUCCESS";
const UPDATE_MENU_SUCCESS = "UPDATE_MENU_SUCCESS";

// ---------------------------

//Reducers -> Mensagens de Erro

const errorCreate = (state = null, action) => {
  const { type, payload } = action;

  switch (type) {
    case CREATE_MENU_ERROR:
      return { message: payload.message, httpcode: payload.httpcode };
    default:
      return state;
  }
};

const errorUpdate = (state = null, action) => {
  const { type, payload } = action;

  switch (type) {
    case UPDATE_MENU_ERROR:
      return { message: payload.message, httpcode: payload.httpcode };
    default:
      return state;
  }
};

const errorDelete = (state = null, action) => {
  const { type, payload } = action;

  switch (type) {
    case DELETE_MENU_ERROR:
      return { message: payload.message, httpcode: payload.httpcode };
    default:
      return state;
  }
};

const errorGet = (state = null, action) => {
  const { type, payload } = action;

  switch (type) {
    case GET_MENU_ERROR:
      return { message: payload.message, httpcode: payload.httpcode };
    default:
      return state;
  }
};

const errorGetById = (state = null, action) => {
  const { type, payload } = action;

  switch (type) {
    case GET_MENU_BY_ID_ERROR:
      return { message: payload.message, httpcode: payload.httpcode };
    default:
      return state;
  }
};

const isGetProcessing = (state = { status: false, message: null }, action) => {
  const { type } = action;

  switch (type) {
    case GET_MENU_REQUEST:
      return {
        status: true,
        message: null
      };
    case UPDATE_MENU_REQUEST:
    case DELETE_MENU_REQUEST:
    case GET_MENU_BY_ID_REQUEST:
    case CREATE_MENU_REQUEST:
      return {
        status: false,
        message: null
      };
    case GET_MENU_SUCCESS:
      return {
        status: false,
        message: 'Sucesso'
      };
    case GET_MENU_ERROR:
      return {
        status: false,
        message: 'Erro'
      };
    default:
      return state;
  }
};

const isGetByIdProcessing = (state = { status: false, message: null }, action) => {
  const { type } = action;

  switch (type) {
    case GET_MENU_BY_ID_REQUEST:
      return {
        status: true,
        message: null
      };
    case UPDATE_MENU_REQUEST:
    case DELETE_MENU_REQUEST:
    case GET_MENU_REQUEST:
    case CREATE_MENU_REQUEST:
      return {
        status: false,
        message: null
      };
    case GET_MENU_BY_ID_SUCCESS:
      return {
        status: false,
        message: 'Sucesso'
      };
    case GET_MENU_BY_ID_ERROR:
      return {
        status: false,
        message: 'Erro'
      };
    default:
      return state;
  }
};

const isCreateProcessing = (state = { status: false, message: null }, action) => {
  const { type } = action;

  switch (type) {
    case CREATE_MENU_REQUEST:
      return {
        status: true,
        message: null
      };
    case DELETE_MENU_REQUEST:
    case GET_MENU_BY_ID_REQUEST:
    case GET_MENU_REQUEST:
    case UPDATE_MENU_REQUEST:
      return {
        status: false,
        message: null
      };
    case CREATE_MENU_SUCCESS:
      return {
        status: false,
        message: 'Sucesso'
      };
    case CREATE_MENU_ERROR:
      return {
        status: false,
        message: 'Erro'
      };
    default:
      return state;
  }
};

const isUpdateProcessing = (state = { status: false, message: null }, action) => {
  const { type } = action;

  switch (type) {
    case UPDATE_MENU_REQUEST:
      return {
        status: true,
        message: null
      };
    case DELETE_MENU_REQUEST:
    case GET_MENU_BY_ID_REQUEST:
    case GET_MENU_REQUEST:
    case CREATE_MENU_REQUEST:
      return {
        status: false,
        message: null
      };
    case UPDATE_MENU_SUCCESS:
      return {
        status: false,
        message: 'Sucesso'
      };
    case UPDATE_MENU_ERROR:
      return {
        status: false,
        message: 'Erro'
      };
    default:
      return state;
  }
};

const isDeleteProcessing = (state = { status: false, message: null }, action) => {
  const { type } = action;

  switch (type) {
    case DELETE_MENU_REQUEST:
      return {
        status: true,
        message: null
      };
    case CREATE_MENU_REQUEST:
    case GET_MENU_BY_ID_REQUEST:
    case GET_MENU_REQUEST:
    case UPDATE_MENU_REQUEST:
      return {
        status: false,
        message: null
      }
    case DELETE_MENU_SUCCESS:
      return {
        status: false,
        message: 'sucesso'
      };
    case DELETE_MENU_ERROR:
      return {
        status: false,
        message: 'erro'
      };
    default:
      return state;
  }
};

//Reducers -> Manipulacao do estado
const allMenus = (state = { data: [] }, action) => {
  const { type, payload } = action;

  switch (type) {
    case GET_MENU_SUCCESS:
      return {
        data: payload.list,
        totalPages: payload.totalPages,
        currentPage: payload.currentPage,
        totalElements: payload.totalElements
      };
    default:
      return state;
  }
};

//Reducers -> Manipulacao do estado
const menu = (state = { metadata: [] }, action) => {
  const { type, payload } = action;

  switch (type) {
    case GET_MENU_BY_ID_SUCCESS:
      return {
        menuId: payload.id,
        hasParent: payload.hasParent,
        parentId: payload.parentId,
        icon: payload.icon,
        title: payload.title,
        path: payload.path,
      };
    default:
      return state;
  }
};

//Export All Reducers 
export default combineReducers({
  allMenus,
  menu,
  errorCreate,
  errorUpdate,
  errorDelete,
  errorGet,
  errorGetById,
  isGetProcessing,
  isCreateProcessing,
  isDeleteProcessing,
  isUpdateProcessing,
  isGetByIdProcessing
});

