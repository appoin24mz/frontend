import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import {
  Input,
  Row,
  Col,
  Button,
  Form,
  Spin,
  notification,
  Modal,
  Skeleton,
  Switch,
  Select,
} from "antd";
import {
  CloseOutlined,
  SaveOutlined,
  PieChartOutlined,
  HddOutlined,
  TeamOutlined,
  SettingOutlined,
  UsergroupAddOutlined,
  SyncOutlined,
  LineChartOutlined,
} from "@ant-design/icons";
import {
  getUserProfile,
  getDataById,
  updateRow,
  getIsUpdateProcessing,
  getUpdateError,
  getIsGetByIdProcessing,
  getByIdError,
  getData,
} from "../redux";
import styled from "styled-components";

const { Option } = Select;

const EditModal = ({ isEditModalVisible, handleCancel, menus, id }) => {
  const dispatch = useDispatch();

  const [form] = Form.useForm();

  //Local Variables 
  const SUCESSO = 'sucesso';

  //Store State 
  const userProfile = useSelector(getUserProfile);

  //Processing status
  const isGetByIdProcessing = useSelector(getIsGetByIdProcessing);
  const isUpdateProcessing = useSelector(getIsUpdateProcessing);

  //Error status
  const errorGetById = useSelector(getByIdError);
  const errorUpdate = useSelector(getUpdateError);

  //Local State
  const [isDisable, setIsDisable] = useState(false);
  const [confirmLoading, setConfirmLoading] = useState(false);

  const onFinish = (formValues) => {
    setConfirmLoading(true);
    dispatch(
      updateRow({
        code: id,
        iconForm: formValues.icon,
        parentIdForm: formValues.parent_id,
        title: formValues.title,
        path: formValues.path,
      })
    );
  };

  //useEffect Operations
  useEffect(() => {
    dispatch(getDataById(id));
  }, []);

  useEffect(() => {
    if (isGetByIdProcessing.message !== null) {
      if (isGetByIdProcessing.message.toLowerCase() == 'Sucesso'.toLowerCase() && !isGetByIdProcessing.status) {
        console.info('Carregado com sucesso');
        onFill();
      } else {
        openNotificationWithIcon('error', 'Erro', '' + errorGetById.message);
      }
    }
  }, [isGetByIdProcessing, errorGetById]);

  //Update Processing 
  useEffect(() => {
    if (isUpdateProcessing.message !== null) {
      if (isUpdateProcessing.message.toLowerCase() == SUCESSO && !isUpdateProcessing.status) {
        openNotificationWithIcon('success', 'Sucesso', 'Registo Actualizado com Sucesso!');
        setConfirmLoading(false);
        dispatch(getData({ page: "", searchTerm: "", size: "" }));
      } else {
        openNotificationWithIcon('error', 'Erro', '' + errorUpdate.message);
      }
    }
  }, [isUpdateProcessing, errorUpdate]);

  //Local Functions
  const onFill = () => {
    form.setFieldsValue({
        iconForm: userProfile.icon,
        parentIdForm: userProfile.parentId,
        title: userProfile.title,
        path: userProfile.path,
    });
  };

  const openNotificationWithIcon = (tipo, titulo, mensagem) => {
    notification[tipo]({
      message: titulo,
      description: mensagem,
    });
  };

  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 8 },
      md: { span: 8 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 16 },
      md: { span: 16 },
    },
  };

  const handleSwitch = () => {
    setIsDisable(isDisable ? false : true);
    console.log("isDisable: ", isDisable);
  };

  const generateMenu = (menus) => {
    const generatedMenus =
      menus &&
      menus.map((item) => {
        return <Option value={item.title}>{item.title}</Option>;
      });

    return generatedMenus;
  };

  return (
    <Modal
      width={900}
      title={"Alterar Menu"}
      closable={false}
      visible={isEditModalVisible}
      confirmLoading={confirmLoading}
      footer={null}
    >
      <Skeleton loading={isGetByIdProcessing.status} active>
        {onFill()}
        <Row>
          <Col span={24}>
            <Skeleton
              loading={isUpdateProcessing.status}
            >
              <FormStyled className="ccc-form">
                <Form
                  {...formItemLayout}
                  name="edit-menu"
                  form={form}
                  initialValues={{ remember: true }}
                  onFinish={onFinish}
                >
                  <Row gutter={48}>
                    <Col span={12} offset={4}>
                      <Form.Item
                        name="title"
                        label="Menu"
                        rules={[
                          {
                            required: true,
                            message: "Por favor informe o Menu!",
                          },
                        ]}
                      >
                        <Input placeholder="Menu" className="text-center" />
                      </Form.Item>

                      <Form.Item
                        name="path"
                        label="Caminho do Menu"
                        rules={[
                          {
                            required: true,
                            message: "Por favor informe o Caminho do Menu!",
                          },
                        ]}
                      >
                        <Input placeholder="/menu" className="text-center" />
                      </Form.Item>

                      <Form.Item label="É Menu Principal?">
                        <Switch
                          checkedChildren="Sim"
                          unCheckedChildren="Não"
                          defaultUnChecked
                          checked={isDisable}
                          onChange={handleSwitch}
                        />
                      </Form.Item>

                      <Form.Item
                        name="parent_id"
                        label="Menu Raiz"
                        className={isDisable ? "hidden" : "visible"}
                      >
                        <Select
                          showSearch
                          placeholder="Seleccione o Menu Raiz"
                          optionFilterProp="children"
                          filterOption={(input, option) =>
                            option.children
                              .toLowerCase()
                              .indexOf(input.toLowerCase()) >= 0
                          }
                          filterSort={(optionA, optionB) =>
                            optionA.children
                              .toLowerCase()
                              .localeCompare(optionB.children.toLowerCase())
                          }
                        >
                          {generateMenu(menus)}
                        </Select>
                      </Form.Item>

                      <Form.Item
                        name="icon"
                        label="Icon"
                        className={isDisable ? "visible" : "hidden"}
                      >
                        <Select placeholder="Seleccione o Icon">
                          <Option value="PieChartOutlined">
                            <PieChartOutlined
                              style={{ paddingRight: "15px" }}
                            />
                            Dashboard
                          </Option>
                          <Option value="HddOutlined">
                            <HddOutlined style={{ paddingRight: "15px" }} />
                            Maquinas
                          </Option>
                          <Option value="TeamOutlined">
                            <TeamOutlined style={{ paddingRight: "15px" }} />
                            Clientes
                          </Option>
                          <Option value="UsergroupAddOutlined">
                            <UsergroupAddOutlined
                              style={{ paddingRight: "15px" }}
                            />
                            Utilizadores
                          </Option>
                          <Option value="SettingOutlined">
                            <SettingOutlined style={{ paddingRight: "15px" }} />
                            Configurações
                          </Option>
                          <Option value="SyncOutlined">
                            <SyncOutlined style={{ paddingRight: "15px" }} />
                            OPerações
                          </Option>
                          <Option value="LineChartOutlined">
                            <LineChartOutlined
                              style={{ paddingRight: "15px" }}
                            />
                            Relatórios
                          </Option>
                        </Select>
                      </Form.Item>
                    </Col>
                  </Row>

                  <Row>
                    <Col span={24}>
                      <div className="separator"></div>
                    </Col>
                  </Row>
                  <Row>
                    <Col span={8} offset={8}>
                      <Form.Item className="botoes">
                        <Button
                          type="primary"
                          htmlType="button"
                          className=""
                          icon={<CloseOutlined />}
                          danger
                          onClick={handleCancel}
                        >
                          Cancelar
                        </Button>
                        <Button
                          type="primary"
                          htmlType="submit"
                          className=""
                          icon={<SaveOutlined />}
                          loading={isUpdateProcessing.status}
                        >
                          Gravar
                        </Button>
                      </Form.Item>
                    </Col>
                  </Row>
                </Form>
              </FormStyled>
            </Skeleton>
          </Col>
        </Row>
      </Skeleton>
    </Modal>
  );
};

const FormStyled = styled.div`
  .botoes .ant-form-item-control-input-content {
    display: flex;
    justify-content: center;
    align-items: center;
  }

  button {
    margin-left: 20px;
  }

  .separator {
    border-bottom: 1px solid #e4e2e2;
    margin: 20px 0px;
  }

  .upload-column {
    display: flex;
    justify-content: center;
    align-items: center;
  }

  .ant-upload-picture-card-wrapper {
    width: auto;
  }

  .ant-upload.ant-upload-select-picture-card {
    text-align: center;
    width: 100%;
    height: 100%;
    vertical-align: top;
    background-color: unset;
    border: none;
  }

  .ant-upload.ant-upload-select-picture-card > .ant-upload {
    flex-direction: column;
  }

  .preview-uploaded-image {
    height: 150px;
    width: 150px;
    border: 1px solid #bbb6b6;
    border-radius: 6px;
    margin: 20px;
    overflow: hidden;
  }
`;

export default EditModal;
