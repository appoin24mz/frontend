import { combineReducers } from "redux";
import API from "../../../shared/api";

//Actions Creators 
//Post Request
export const saveData = ({ iconForm, parentIdForm, path, title }) => async (dispatch) => {
  dispatch({ type: CREATE_USERPROFILE_REQUEST });
  let parentId = parentIdForm == undefined ? 1 : parentIdForm;
  let icon = iconForm == undefined ? null : iconForm;
  try {
    const response = await API.post("/userprofiles/create", { icon, parentId, path, title });
    dispatch({ type: CREATE_USERPROFILE_SUCCESS, payload: response });
  } catch (error) {
    dispatch({ type: CREATE_USERPROFILE_ERROR, payload: error });
  }
};

//Update Request
export const updateRow = ({ code, iconForm, parentIdForm, path, title }) => async (dispatch) => {
  dispatch({ type: UPDATE_USERPROFILE_REQUEST });
  let parentId = parentIdForm == undefined ? 1 : parentIdForm;
  let icon = iconForm == undefined ? null : iconForm;
  try {
    const response = await API.post(`/userprofiles/${code}/update`, { icon, parentId, path, title });
    dispatch({ type: UPDATE_USERPROFILE_SUCCESS, payload: response });
  } catch (error) {
    dispatch({ type: UPDATE_USERPROFILE_ERROR, payload: error });
  }
};

//Delete Request
export const deleteRow = ({ code }) => async (dispatch) => {
  dispatch({ type: DELETE_USERPROFILE_REQUEST });

  try {
    const response = await API.post(`/userprofiles/${code}/delete`);
    dispatch({ type: DELETE_USERPROFILE_SUCCESS, payload: response });
  } catch (error) {
    dispatch({ type: DELETE_USERPROFILE_ERROR, payload: error });
  }
};

//Get Request
export const getData = ({ page, searchTerm, size }) => async (dispatch) => {
  dispatch({ type: GET_USERPROFILE_REQUEST });

  try {
    const response = await API.get("/userprofiles", { page, searchTerm, size });
    dispatch({ type: GET_USERPROFILE_SUCCESS, payload: response });
  } catch (error) {
    dispatch({ type: GET_USERPROFILE_ERROR, payload: error });
  }
};

//Get by ID Request
export const getDataById = (code) => async (dispatch) => {
  dispatch({ type: GET_USERPROFILE_BY_ID_REQUEST });

  try {
    const response = await API.get(`/userprofiles/${code}`);
    dispatch({ type: GET_USERPROFILE_BY_ID_SUCCESS, payload: response });
  } catch (error) {
    dispatch({ type: GET_USERPROFILE_BY_ID_ERROR, payload: error });
  }
};

//Selectors 
export const getAllUserProfiles = state => state.userProfiles.allUserProfiles;
export const getUserProfile = state => state.userProfiles.userProfile;
export const getIsDeleteProcessing = state => state.userProfiles.isDeleteProcessing;
export const getIsGetProcessing = state => state.userProfiles.isGetProcessing;
export const getIsGetByIdProcessing = state => state.userProfiles.isGetByIdProcessing;
export const getIsCreateProcessing = state => state.userProfiles.isCreateProcessing;
export const getIsUpdateProcessing = state => state.userProfiles.isUpdateProcessing;
export const getError = state => state.userProfiles.errorGet;
export const getByIdError = state => state.userProfiles.errorGetById;
export const getCreateError = state => state.userProfiles.errorCreate;
export const getDeleteError = state => state.userProfiles.errorDelete;
export const getUpdateError = state => state.userProfiles.errorUpdate;

// ---------------------------
//Accoes
//Requests
const GET_USERPROFILE_REQUEST = "GET_USERPROFILE_REQUEST";
const GET_USERPROFILE_BY_ID_REQUEST = "GET_USERPROFILE_BY_ID_REQUEST";
const CREATE_USERPROFILE_REQUEST = "CREATE_USERPROFILE_REQUEST";
const DELETE_USERPROFILE_REQUEST = "DELETE_USERPROFILE_REQUEST";
const UPDATE_USERPROFILE_REQUEST = "UPDATE_USERPROFILE_REQUEST";
//Erros
const GET_USERPROFILE_ERROR = "GET_USERPROFILE_ERROR";
const GET_USERPROFILE_BY_ID_ERROR = "GET_USERPROFILE_BY_ID_ERROR";
const CREATE_USERPROFILE_ERROR = "CREATE_USERPROFILE_ERROR";
const UPDATE_USERPROFILE_ERROR = "UPDATE_USERPROFILE_ERROR";
const DELETE_USERPROFILE_ERROR = "DELETE_USERPROFILE_ERROR";

//Sucesso
const CREATE_USERPROFILE_SUCCESS = "CREATE_USERPROFILE_SUCCESS";
const GET_USERPROFILE_SUCCESS = "GET_USERPROFILE_SUCCESS";
const GET_USERPROFILE_BY_ID_SUCCESS = "GET_USERPROFILE_BY_ID_SUCCESS";
const DELETE_USERPROFILE_SUCCESS = "DELETE_USERPROFILE_SUCCESS";
const UPDATE_USERPROFILE_SUCCESS = "UPDATE_USERPROFILE_SUCCESS";

// ---------------------------

//Reducers -> Mensagens de Erro

const errorCreate = (state = null, action) => {
  const { type, payload } = action;

  switch (type) {
    case CREATE_USERPROFILE_ERROR:
      return { message: payload.message, httpcode: payload.httpcode };
    default:
      return state;
  }
};

const errorUpdate = (state = null, action) => {
  const { type, payload } = action;

  switch (type) {
    case UPDATE_USERPROFILE_ERROR:
      return { message: payload.message, httpcode: payload.httpcode };
    default:
      return state;
  }
};

const errorDelete = (state = null, action) => {
  const { type, payload } = action;

  switch (type) {
    case DELETE_USERPROFILE_ERROR:
      return { message: payload.message, httpcode: payload.httpcode };
    default:
      return state;
  }
};

const errorGet = (state = null, action) => {
  const { type, payload } = action;

  switch (type) {
    case GET_USERPROFILE_ERROR:
      return { message: payload.message, httpcode: payload.httpcode };
    default:
      return state;
  }
};

const errorGetById = (state = null, action) => {
  const { type, payload } = action;

  switch (type) {
    case GET_USERPROFILE_BY_ID_ERROR:
      return { message: payload.message, httpcode: payload.httpcode };
    default:
      return state;
  }
};

const isGetProcessing = (state = { status: false, message: null }, action) => {
  const { type } = action;

  switch (type) {
    case GET_USERPROFILE_REQUEST:
      return {
        status: true,
        message: null
      };
    case UPDATE_USERPROFILE_REQUEST:
    case DELETE_USERPROFILE_REQUEST:
    case GET_USERPROFILE_BY_ID_REQUEST:
    case CREATE_USERPROFILE_REQUEST:
      return {
        status: false,
        message: null
      };
    case GET_USERPROFILE_SUCCESS:
      return {
        status: false,
        message: 'Sucesso'
      };
    case GET_USERPROFILE_ERROR:
      return {
        status: false,
        message: 'Erro'
      };
    default:
      return state;
  }
};

const isGetByIdProcessing = (state = { status: false, message: null }, action) => {
  const { type } = action;

  switch (type) {
    case GET_USERPROFILE_BY_ID_REQUEST:
      return {
        status: true,
        message: null
      };
    case UPDATE_USERPROFILE_REQUEST:
    case DELETE_USERPROFILE_REQUEST:
    case GET_USERPROFILE_REQUEST:
    case CREATE_USERPROFILE_REQUEST:
      return {
        status: false,
        message: null
      };
    case GET_USERPROFILE_BY_ID_SUCCESS:
      return {
        status: false,
        message: 'Sucesso'
      };
    case GET_USERPROFILE_BY_ID_ERROR:
      return {
        status: false,
        message: 'Erro'
      };
    default:
      return state;
  }
};

const isCreateProcessing = (state = { status: false, message: null }, action) => {
  const { type } = action;

  switch (type) {
    case CREATE_USERPROFILE_REQUEST:
      return {
        status: true,
        message: null
      };
    case DELETE_USERPROFILE_REQUEST:
    case GET_USERPROFILE_BY_ID_REQUEST:
    case GET_USERPROFILE_REQUEST:
    case UPDATE_USERPROFILE_REQUEST:
      return {
        status: false,
        message: null
      };
    case CREATE_USERPROFILE_SUCCESS:
      return {
        status: false,
        message: 'Sucesso'
      };
    case CREATE_USERPROFILE_ERROR:
      return {
        status: false,
        message: 'Erro'
      };
    default:
      return state;
  }
};

const isUpdateProcessing = (state = { status: false, message: null }, action) => {
  const { type } = action;

  switch (type) {
    case UPDATE_USERPROFILE_REQUEST:
      return {
        status: true,
        message: null
      };
    case DELETE_USERPROFILE_REQUEST:
    case GET_USERPROFILE_BY_ID_REQUEST:
    case GET_USERPROFILE_REQUEST:
    case CREATE_USERPROFILE_REQUEST:
      return {
        status: false,
        message: null
      };
    case UPDATE_USERPROFILE_SUCCESS:
      return {
        status: false,
        message: 'Sucesso'
      };
    case UPDATE_USERPROFILE_ERROR:
      return {
        status: false,
        message: 'Erro'
      };
    default:
      return state;
  }
};

const isDeleteProcessing = (state = { status: false, message: null }, action) => {
  const { type } = action;

  switch (type) {
    case DELETE_USERPROFILE_REQUEST:
      return {
        status: true,
        message: null
      };
    case CREATE_USERPROFILE_REQUEST:
    case GET_USERPROFILE_BY_ID_REQUEST:
    case GET_USERPROFILE_REQUEST:
    case UPDATE_USERPROFILE_REQUEST:
      return {
        status: false,
        message: null
      }
    case DELETE_USERPROFILE_SUCCESS:
      return {
        status: false,
        message: 'sucesso'
      };
    case DELETE_USERPROFILE_ERROR:
      return {
        status: false,
        message: 'erro'
      };
    default:
      return state;
  }
};

//Reducers -> Manipulacao do estado
const allUserProfiles = (state = { data: [] }, action) => {
  const { type, payload } = action;

  switch (type) {
    case GET_USERPROFILE_SUCCESS:
      return {
        data: payload.list,
        totalPages: payload.totalPages,
        currentPage: payload.currentPage,
        totalElements: payload.totalElements
      };
    default:
      return state;
  }
};

//Reducers -> Manipulacao do estado
const userProfile = (state = { metadata: [] }, action) => {
  const { type, payload } = action;

  switch (type) {
    case GET_USERPROFILE_BY_ID_SUCCESS:
      return {
        USERPROFILEId: payload.id,
        hasParent: payload.hasParent,
        parentId: payload.parentId,
        icon: payload.icon,
        title: payload.title,
        path: payload.path,
      };
    default:
      return state;
  }
};

//Export All Reducers 
export default combineReducers({
  allUserProfiles,
  userProfile,
  errorCreate,
  errorUpdate,
  errorDelete,
  errorGet,
  errorGetById,
  isGetProcessing,
  isCreateProcessing,
  isDeleteProcessing,
  isUpdateProcessing,
  isGetByIdProcessing
});

