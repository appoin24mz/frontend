import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  Row,
  Col,
  Input,
  Card,
  PageHeader,
  Tabs,
} from "antd";
import {
  TeamOutlined,
  MenuOutlined,
  ShopOutlined,
  SettingOutlined
} from "@ant-design/icons";
import {
  useHistory,
  Link
} from "react-router-dom";

const { Meta } = Card;

const Configuracoes = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  return (
    <Row className="edoc-layout-content-general">
      <Col span={24}>
        <PageHeader
          className="site-page-header-responsive"
          onBack={() => window.history.back()}
          title="Configurações"
          subTitle="Menu de Configurações"
          extra={[
            <>
              <Input.Search
                placeholder="Procurar Configuração"
                onSearch={(value) => console.log(value)}
              />
            </>,
          ]}
          footer={<Tabs defaultActiveKey="1"></Tabs>}
        >
          <Row>
            <Col span={6}>
              <Link
                to="/perfis"
              >
                <Card
                  hoverable
                  className = "ccc-card-menu"
                  style={{ width: "98%" }}
                  cover={<TeamOutlined />}
                >
                  <Meta title="Perfis" description="Configurar Perfis dos Utilizadores" />
                </Card>
              </Link>
            </Col>

            <Col span={6}>
              <Link
                to="/menu"
              >
                <Card
                  hoverable
                  className = "ccc-card-menu"
                  style={{ width: "98%" }}
                  cover={<MenuOutlined />}
                >
                  <Meta title="Menu" description="Configurar Menu" />
                </Card>
              </Link>
            </Col>

            <Col span={6}>
              <Link
                to="/configuracoes/agencias"
              >
                <Card
                  hoverable
                  className = "ccc-card-menu"
                  style={{ width: "98%" }}
                  cover={<ShopOutlined />}
                >
                  <Meta title="Agências" description="Configurar Agências" />
                </Card>
              </Link>
            </Col>

            <Col span={6}>
              <Link
                to="/configuracoes/geral"
              >
                <Card
                  hoverable
                  className = "ccc-card-menu"
                  style={{ width: "98%" }}
                  cover={<SettingOutlined />}
                >
                  <Meta title="Gerais" description="Configurar outros" />
                </Card>
              </Link>
            </Col>
          </Row>
        </PageHeader>
      </Col>
    </Row>
  );
};

export default Configuracoes;
