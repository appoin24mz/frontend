import React, { useState } from "react";
import { Route, Switch } from "react-router-dom";
import styled from "styled-components";

// ***** ANT.DESIGN ******
import { Layout, Row, Col, Button } from "antd";

// ***** SHARED FILES *****
import Header from "./shared/components/Header";
import Sidebar from "./shared/components/Sidebar";

// ***** DASHBOARD
import Dashboard from "./dashboard";

// ***** UTILIZAODRS
import Utilizadores from "./utilizadores";
import NovoUtilizador from "./utilizadores/Novo";
import DetalhesUtilizador from "./utilizadores/Detalhes";
import AlterarUtilizador from "./utilizadores/Alterar";

// ***** CLIENTES


// ***** MAQUINAS
import Maquinas from "./maquinas";
import NovaMaquina from "./maquinas/Nova";
import DetalhesMaquina from "./maquinas/Detalhes";
import AlterarMaquina from "./maquinas/Alterar";

// ***** MEDICAMENTOS
import Medicamentos from "./medicamentos";
import NovoMedicamento from "./medicamentos/Novo";
import DetalhesMedicamento from "./medicamentos/Detalhes";
import AlterarMedicamento from "./medicamentos/Alterar";
import GrupoMedicamento from "./medicamentos/grupos";
import FabricantesMedicamentos from "./medicamentos/fabricantes";

// ***** MEDICAMENTOS
import Farmacias from "./farmacias";
import NovaFarmacia from "./farmacias/Novo";
import DetalhesFarmacia from "./farmacias/Detalhes";


// ***** OPERAÇÕES
import Operacoes from "./operacoes";

// ***** RELATÓRIOS


// ***** CONFIGURAÇÕES
import Configuracoes from "./configuracoes";
import Perfis from "./configuracoes/perfis";
import Menus from "./configuracoes/menus";

const App = () => {
  const [collapsed, toggleSidebar] = useState(false);

  const toggle = () => {
    toggleSidebar(!collapsed);
  };

  return (
    <Layout style={{ height: "100%" }}>
      <Header toggler={toggle} collapsed={collapsed} />
      <Layout>
        <Row style={{ height: "100%" }}>
          <Col span={18} offset={3} className="default-layout">
            <Layout>
              <Layout.Content>
                <Switch>
                  {/* DASHBOARD */}
                  <Route exact path="/app">
                    <Dashboard />
                  </Route>

                  <Route path={`/app/system-dashboard`}>
                    <Dashboard />
                  </Route>

                  {/* GESTÃO DE MEDICAMENTOS */}
                  <Route path={`/app/medicamentos`}>
                    <Medicamentos />
                  </Route>

                  <Route path={`/app/novo-medicamento`}>
                    <NovoMedicamento />
                  </Route>

                  <Route path={`/app/grupo-medicamentos`}>
                    <GrupoMedicamento />
                  </Route>

                  <Route path={`/app/fabricante-medicamentos`}>
                    <FabricantesMedicamentos />
                  </Route>

                  <Route path={`/app/detalhes-medicamento/:id`}>
                    <DetalhesMedicamento />
                  </Route>

                  <Route path={`/app/alterar-maquina/:id`}>
                    <AlterarMedicamento />
                  </Route>

                   {/* GESTÃO DE FARMACIAS */}
                   <Route path={`/app/farmacias`}>
                    <Farmacias />
                  </Route>

                  <Route path={`/app/nova-farmacia`}>
                    <NovaFarmacia />
                  </Route>

                  <Route path={`/app/detalhes-farmacia/:id`}>
                    <DetalhesFarmacia />
                  </Route>

                  {/* GESTÃO DE UTILIZADORES */}
                  <Route path={`/utilizadores`}>
                    <Utilizadores />
                  </Route>

                  <Route path={`/novo-utilizador`}>
                    <NovoUtilizador />
                  </Route>

                  <Route path={`/credenciais-utilizadores-ccc`}>
                    <NovoUtilizador />
                  </Route>

                  <Route path={`/detalhes-utilizador/:id`}>
                    <DetalhesUtilizador />
                  </Route>

                  <Route path={`/alterar-utilizador/:id`}>
                    <AlterarUtilizador />
                  </Route>
                  
                  {/* GESTÃO DE MAQUINAS */}
                  <Route path={`/maquinas`}>
                    <Maquinas />
                  </Route>

                  <Route path={`/nova-maquina`}>
                    <NovaMaquina />
                  </Route>

                  <Route path={`/detalhes-maquina/:id`}>
                    <DetalhesMaquina />
                  </Route>

                  <Route path={`/alterar-maquina/:id`}>
                    <AlterarMaquina />
                  </Route>

                  <Route path={`/associar-maquina`}>
                    <NovoUtilizador />
                  </Route>
                  
                  {/* OPERAÇÕES */}
                  <Route path={`/operacoes`}>
                    <Operacoes />
                  </Route>

                  {/* RELATORIOS */}
                  <Route path={`/relatorios-ganhos`}>
                    <Utilizadores />
                  </Route>

                  <Route path={`/relatorios-ussd`}>
                    <NovoUtilizador />
                  </Route>

                  <Route path={`/relatorios-despesas`}>
                    <DetalhesUtilizador />
                  </Route>

                  {/* CONFIGURACOES */}
                  <Route path={`/configuracoes`}>
                    <Configuracoes />
                  </Route>
                  <Route path={`/perfis`}>
                      <Perfis />
                  </Route>
                  <Route path={`/menu`}>
                      <Menus />
                  </Route>
                </Switch>
              </Layout.Content>
            </Layout>
          </Col>
        </Row>
      </Layout>
    </Layout>
  );
};

const Separator = styled.div`
  margin: 0 25px;
  border-bottom-width: thin;
  border-bottom: 1px solid #9e9e9e;
`;

export default App;
