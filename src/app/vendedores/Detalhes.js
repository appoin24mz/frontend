import React, { useEffect } from "react";
import {
  Row,
  Col,
  PageHeader,
  Button,
  Skeleton,
  Statistic,
  Descriptions,
} from "antd";
import {
  getError,
  getMachine,
  isRequestProcessing,
  getDataById,
} from "./redux";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, useParams } from "react-router-dom";

const Detalhes = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  const machine = useSelector(getMachine);
  const isProcessing = useSelector(isRequestProcessing);
  const hasError = useSelector(getError);
  let { id } = useParams();

  useEffect(() => {
    dispatch(getDataById(id));
  }, []);

  const handleAction = (id) => {
    console.log("Association - Action: ", id);
  };

  const handleDelete = (id) => {
    console.log("DELETE - Action: ", id);
  };

  const handleShowAllMachines = () => {
    history.push(`/maquinas`);
  };

  return (
    <Row className="edoc-layout-content-general">
      <Col span={24}>
        <PageHeader
          className="site-page-header-responsive"
          onBack={() => window.history.back()}
          title="Detalhes da Maquina"
          subTitle=""
          extra={[
            <>
              <Button
                type="default"
                style={{ marginLeft: "10px" }}
                onClick={() => handleAction(machine.machineId)}
              >
                Associar a Entidade
              </Button>
              <Button
                type="default"
                style={{ marginLeft: "10px" }}
                onClick={() => handleShowAllMachines()}
              >
                Todas Maquinas
              </Button>
              <Button
                type="primary"
                danger
                style={{ marginLeft: "10px" }}
                onClick={() => handleDelete(machine.machineId)}
              >
                Excluir
              </Button>
            </>,
          ]}
        >
          <Row>
            <Col span={24}>
              <Skeleton loading={isProcessing} active avatar>
                <div className="content">
                  <div className="extra">
                    <div
                      style={{
                        display: "flex",
                        width: "max-content",
                        justifyContent: "flex-end",
                      }}
                    >
                      <Statistic
                        title="Marca"
                        value={machine.brand}
                        style={{
                          marginRight: 32,
                        }}
                      />
                      <Statistic title="Modelo" value={machine.model} />
                    </div>
                  </div>
                  <div className="main">
                    <Descriptions size="small" column={3}>
                      <Descriptions.Item label="Número de Série">
                        {machine.serialNumber}
                      </Descriptions.Item>
                      <Descriptions.Item label="Mac Address">
                        {machine.macAddress}
                      </Descriptions.Item>
                      <Descriptions.Item label="Bluetooth Pin">
                        {machine.bluetoothPin}
                      </Descriptions.Item>
                      {/* <Descriptions.Item label="Data de Criação">{allMachines.metadata.createdAt}</Descriptions.Item>
                    <Descriptions.Item label="Estado">{allMachines.metadata.active}</Descriptions.Item> */}
                    </Descriptions>
                  </div>
                </div>
              </Skeleton>
            </Col>
          </Row>
        </PageHeader>
      </Col>
    </Row>
  );
};

export default Detalhes;
