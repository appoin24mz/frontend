import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  Row,
  Col,
  Table,
  Input,
  Button,
  Menu,
  Modal,
  Dropdown,
  PageHeader,
  Tabs,
  Spin,
  notification,
} from "antd";
import {
  EditOutlined,
  DeleteOutlined,
  PicLeftOutlined,
  DownOutlined,
  ExclamationCircleOutlined,
} from "@ant-design/icons";
import { useHistory } from "react-router-dom";

import {
  getData,
  deleteRow,
  getAllPharmacies,
  getError,
  getDeleteError,
  getIsDeleteProcessing,
  getIsGetProcessing,
} from "./redux";

const { confirm } = Modal;

const Farmacias = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  //Local State
  const [currentPageSize, setCurrentPageSize] = useState(10);
  const [pharmacies, setPharmacies] = useState();
  const [searchVal, setSearchVal] = useState(null);

  //Store State
  const allPharmacies = useSelector(getAllPharmacies);

  //Processing status
  const isProcessing = useSelector(getIsGetProcessing);
  const isDeleteProcessing = useSelector(getIsDeleteProcessing);

  //Error status
  const errorGet = useSelector(getError);
  const errorDelete = useSelector(getDeleteError);

  //Local Variables
  const SUCESSO = "sucesso";

  //useEffect Operations
  useEffect(() => {
    dispatch(getData({ page: "", searchTerm: "", size: "" }));
  }, []);

  //Get Processing
  useEffect(() => {
    if (allPharmacies !== undefined) {
      setPharmacies(generateData(allPharmacies.data));
    }
  }, [isProcessing, errorGet, allPharmacies]);

  //Delete Processing
  useEffect(() => {
    if (isDeleteProcessing.message !== null) {
      if (
        isDeleteProcessing.message.toLowerCase() == SUCESSO &&
        !isDeleteProcessing.status
      ) {
        openNotificationWithIcon(
          "success",
          "Sucesso",
          "Registo Eliminado com Sucesso!"
        );
        dispatch(getData({ page: "", searchTerm: "", size: "" }));
      } else {
        openNotificationWithIcon("error", "Erro", "" + errorDelete.message);
      }
    }
  }, [isDeleteProcessing, errorDelete]);

  //Local Functions
  const handleMenuOption = (record, key) => {
    if (key.key === "0") {
      handleUpdate(record.key);
    } else if (key.key === "1") {
      handleDetails(record.key);
      // console.log(record);
    } else if (key.key === "2") {
      handleDelete(record.key);
    }
  };

  const handleUpdate = (id) => {
    // history.push(`/app/alterar-medicamento/${id}`);
  };

  const handleDetails = (id) => {
    history.push(`/app/detalhes-farmacia/${id}`);
  };

  const handleDelete = (id) => {
    showDeleteConfirm(id);
  };

  const showDeleteConfirm = (id) => {
    confirm({
      title: "Tem certeza que quer APAGAR/EXCLUIR esse Registo?",
      icon: <ExclamationCircleOutlined />,
      content: "",
      okText: "Sim",
      okType: "danger",
      cancelText: "Não",
      onOk() {
        console.log("OK");
        dispatch(
          deleteRow({
            code: id,
          })
        );
      },
      onCancel() {
        console.log("Cancelado");
      },
    });
  };

  const menu = (record) => {
    return (
      <Menu
        className="ccc-dropdown-menu"
        onClick={(key) => handleMenuOption(record, key)}
      >
        {/* <Menu.Item key="0">
          <EditOutlined /> Alterar
        </Menu.Item> */}
        <Menu.Divider />
        <Menu.Item key="1">
          <PicLeftOutlined /> Detalhar
        </Menu.Item>
        <Menu.Divider />
        <Menu.Item className="ccc-delete-item" key="2">
          <DeleteOutlined /> Excluir
        </Menu.Item>
      </Menu>
    );
  };

  const columns = [
    {
      title: "Farmácias",
      dataIndex: "farmacia",
      key: "farmacia",
      width: "25%",
      sorter: (a, b) => a.farmacia.length - b.farmacia.length,
      ellipsis: true,
    },
    {
      title: "Alvará",
      dataIndex: "alvara",
      key: "alvara",
      width: "15%",
      sorter: (a, b) => a.alvara.length - b.alvara.length,
      ellipsis: true,
    },
    {
      title: "Email",
      dataIndex: "email",
      key: "email",
      width: "20%",
      sorter: (a, b) => a.email.length - b.email.length,
      ellipsis: true,
    },
    {
      title: "Telefone",
      dataIndex: "phone_number",
      key: "phone_number",
      width: "10%",
      sorter: (a, b) => a.phone_number.length - b.phone_number.length,
      ellipsis: true,
    },
    {
      title: "Endereço",
      dataIndex: "address",
      key: "address",
      width: "20%",
      sorter: (a, b) => a.address.length - b.address.length,
      ellipsis: true,
    },
    {
      title: "",
      dataIndex: "accoes",
      key: "accoes",
      render: (text, record) => {
        //console.log(record.key);
        return (
          <Dropdown overlay={menu(record)} trigger={["click"]}>
            <Button className="ccc-btn-sec" onClick={(e) => e.preventDefault()}>
              Acções <DownOutlined />
            </Button>
          </Dropdown>
        );
      },
    },
  ];

  const generateData = (data) => {
    const generatedData =
      data &&
      data.map((item) => {
        return {
          key: item.id,
          farmacia: item.name,
          alvara: item.alvara,
          email: item.contact.email,
          phone_number: item.contact.phoneNumber,
          address: item.location.address,
          accoes: "",
        };
      });

    return generatedData;
  };

  // const generateIrregularPharmacies = (data) => {
  //   let newData = data.filter(el => el.alvara.toLowerCase() === 'N/A'.toLocaleLowerCase());
  //   setTotalFarmaciasIrregulares(newData.length);
  //   console.log('NEW DATA: ',newData.length)
  //   console.log('DATA: ',data)
  //   return generateData(newData);
  // };

  const onShowSizeChange = (current, pageSize) => {
    setCurrentPageSize(pageSize);
    console.log("Current: ", currentPageSize);
    console.log(current, pageSize);
  };

  const openNotificationWithIcon = (tipo, titulo, mensagem) => {
    notification[tipo]({
      message: titulo,
      description: mensagem,
    });
  };

  const getFiltredResult = (e) => {
    console.log("FILTRED VALUE: ", e);
    dispatch(getData({ page: "", searchTerm: e, size: "" }));
  };

  return (
    <Row className="ccc-layout-content-general">
      <Col span={24}>
        <Spin
          spinning={isDeleteProcessing.status}
          tip="Eliminando o Registo..."
        >
          <PageHeader
            className="site-page-header-responsive"
            onBack={() => window.history.back()}
            title="Farmácias"
            subTitle="Listagem de Farmácias"
            extra={[
              <>
                <Input.Search
                  placeholder="Procurar Farmácia"
                  onSearch={getFiltredResult}
                  onChange={(e) => setSearchVal(e.target.value)}
                />
              </>,
            ]}
            footer={<Tabs defaultActiveKey="1"></Tabs>}
          >
            <Row>
              <Col span={24}>
                <Table
                  columns={columns}
                  pagination={{
                    pageSize: currentPageSize,
                    total: allPharmacies.totalElements,
                    showTotal: (total) => `Total ${total} Registos`,
                    defaultCurrent: 1,
                    pageSizeOptions: [5, 10, 25, 50, 100],
                    onShowSizeChange: (defaultCurrent, pageSize) =>
                      onShowSizeChange(defaultCurrent, pageSize),
                    showSizeChanger: true,
                  }}
                  dataSource={pharmacies}
                  size="small"
                  loading={isProcessing.status}
                />
              </Col>
            </Row>
          </PageHeader>
        </Spin>
      </Col>
    </Row>
  );
};

export default Farmacias;
