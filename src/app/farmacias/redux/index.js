import { combineReducers } from "redux";
import API from "../../shared/api";

//Actions Creators
//Post Request
export const saveData = ({
  address,
  alvara,
  alvaraDate,
  city,
  district,
  email,
  isBranch,
  isOpen24Hours,
  latitude,
  longitude,
  name,
  phoneNumber,
  province,
  secondaryPhoneNumber,
}) => async (dispatch) => {
  dispatch({ type: CREATE_PHARMACIES_REQUEST });
  try {
    const response = await API.post("/pharmacies/register", {
      address,
      alvara,
      alvaraDate,
      city,
      district,
      email,
      isBranch,
      isOpen24Hours,
      latitude,
      longitude,
      name,
      phoneNumber,
      province,
      secondaryPhoneNumber,
    });
    dispatch({ type: CREATE_PHARMACIES_SUCCESS, payload: response });
  } catch (error) {
    dispatch({ type: CREATE_PHARMACIES_ERROR, payload: error });
  }
};

//Update Request
export const updateRow = ({
  code,
  address,
  alvara,
  alvaraDate,
  city,
  district,
  email,
  isBranch,
  isOpen24Hours,
  latitude,
  longitude,
  name,
  phoneNumber,
  province,
  secondaryPhoneNumber,
}) => async (dispatch) => {
  dispatch({ type: UPDATE_PHARMACIES_REQUEST });
  try {
    const response = await API.post(`/pharmacies/${code}/update`, {
      address,
      alvara,
      alvaraDate,
      city,
      district,
      email,
      isBranch,
      isOpen24Hours,
      latitude,
      longitude,
      name,
      phoneNumber,
      province,
      secondaryPhoneNumber,
    });
    dispatch({ type: UPDATE_PHARMACIES_SUCCESS, payload: response });
  } catch (error) {
    dispatch({ type: UPDATE_PHARMACIES_ERROR, payload: error });
  }
};

//Delete Request
export const deleteRow = ({ code }) => async (dispatch) => {
  dispatch({ type: DELETE_PHARMACIES_REQUEST });

  try {
    const response = await API.post(`/pharmacies/${code}/delete`);
    dispatch({ type: DELETE_PHARMACIES_SUCCESS, payload: response });
  } catch (error) {
    dispatch({ type: DELETE_PHARMACIES_ERROR, payload: error });
  }
};

//Get Request
export const getData = ({ page, searchTerm, size }) => async (dispatch) => {
  dispatch({ type: GET_PHARMACIES_REQUEST });

  try {
    const response = await API.get("/pharmacies", { page, searchTerm, size });
    dispatch({ type: GET_PHARMACIES_SUCCESS, payload: response });
  } catch (error) {
    dispatch({ type: GET_PHARMACIES_ERROR, payload: error });
  }
};

//Get by ID Request
export const getDataById = (code) => async (dispatch) => {
  dispatch({ type: GET_PHARMACIES_BY_ID_REQUEST });

  try {
    const response = await API.get(`/pharmacies/${code}`);
    dispatch({ type: GET_PHARMACIES_BY_ID_SUCCESS, payload: response });
  } catch (error) {
    dispatch({ type: GET_PHARMACIES_BY_ID_ERROR, payload: error });
  }
};

//Selectors
export const getAllPharmacies = (state) => state.pharmacies.allPharmacies;
export const getPharmacie = (state) => state.pharmacies.pharmacie;
export const getIsDeleteProcessing = (state) =>
  state.pharmacies.isDeleteProcessing;
export const getIsGetProcessing = (state) => state.pharmacies.isGetProcessing;
export const getIsGetByIdProcessing = (state) =>
  state.pharmacies.isGetByIdProcessing;
export const getIsCreateProcessing = (state) =>
  state.pharmacies.isCreateProcessing;
export const getIsUpdateProcessing = (state) =>
  state.pharmacies.isUpdateProcessing;
export const getError = (state) => state.pharmacies.errorGet;
export const getByIdError = (state) => state.pharmacies.errorGetById;
export const getCreateError = (state) => state.pharmacies.errorCreate;
export const getDeleteError = (state) => state.pharmacies.errorDelete;
export const getUpdateError = (state) => state.pharmacies.errorUpdate;

// ---------------------------
//Accoes
//Requests
const GET_PHARMACIES_REQUEST = "GET_PHARMACIES_REQUEST";
const GET_PHARMACIES_BY_ID_REQUEST = "GET_PHARMACIES_BY_ID_REQUEST";
const CREATE_PHARMACIES_REQUEST = "CREATE_PHARMACIES_REQUEST";
const DELETE_PHARMACIES_REQUEST = "DELETE_PHARMACIES_REQUEST";
const UPDATE_PHARMACIES_REQUEST = "UPDATE_PHARMACIES_REQUEST";
//Erros
const GET_PHARMACIES_ERROR = "GET_PHARMACIES_ERROR";
const GET_PHARMACIES_BY_ID_ERROR = "GET_PHARMACIES_BY_ID_ERROR";
const CREATE_PHARMACIES_ERROR = "CREATE_PHARMACIES_ERROR";
const UPDATE_PHARMACIES_ERROR = "UPDATE_PHARMACIES_ERROR";
const DELETE_PHARMACIES_ERROR = "DELETE_PHARMACIES_ERROR";

//Sucesso
const CREATE_PHARMACIES_SUCCESS = "CREATE_PHARMACIES_SUCCESS";
const GET_PHARMACIES_SUCCESS = "GET_PHARMACIES_SUCCESS";
const GET_PHARMACIES_BY_ID_SUCCESS = "GET_PHARMACIES_BY_ID_SUCCESS";
const DELETE_PHARMACIES_SUCCESS = "DELETE_PHARMACIES_SUCCESS";
const UPDATE_PHARMACIES_SUCCESS = "UPDATE_PHARMACIES_SUCCESS";

// ---------------------------

//Reducers -> Mensagens de Erro

const errorCreate = (state = null, action) => {
  const { type, payload } = action;

  switch (type) {
    case CREATE_PHARMACIES_ERROR:
      return { message: payload.message, httpcode: payload.httpcode };
    default:
      return state;
  }
};

const errorUpdate = (state = null, action) => {
  const { type, payload } = action;

  switch (type) {
    case UPDATE_PHARMACIES_ERROR:
      return { message: payload.message, httpcode: payload.httpcode };
    default:
      return state;
  }
};

const errorDelete = (state = null, action) => {
  const { type, payload } = action;

  switch (type) {
    case DELETE_PHARMACIES_ERROR:
      return { message: payload.message, httpcode: payload.httpcode };
    default:
      return state;
  }
};

const errorGet = (state = null, action) => {
  const { type, payload } = action;

  switch (type) {
    case GET_PHARMACIES_ERROR:
      return { message: payload.message, httpcode: payload.httpcode };
    default:
      return state;
  }
};

const errorGetById = (state = null, action) => {
  const { type, payload } = action;

  switch (type) {
    case GET_PHARMACIES_BY_ID_ERROR:
      return { message: payload.message, httpcode: payload.httpcode };
    default:
      return state;
  }
};

const isGetProcessing = (state = { status: false, message: null }, action) => {
  const { type } = action;

  switch (type) {
    case GET_PHARMACIES_REQUEST:
      return {
        status: true,
        message: null,
      };
    case UPDATE_PHARMACIES_REQUEST:
    case DELETE_PHARMACIES_REQUEST:
    case GET_PHARMACIES_BY_ID_REQUEST:
    case CREATE_PHARMACIES_REQUEST:
      return {
        status: false,
        message: null,
      };
    case GET_PHARMACIES_SUCCESS:
      return {
        status: false,
        message: "Sucesso",
      };
    case GET_PHARMACIES_ERROR:
      return {
        status: false,
        message: "Erro",
      };
    default:
      return state;
  }
};

const isGetByIdProcessing = (
  state = { status: false, message: null },
  action
) => {
  const { type } = action;

  switch (type) {
    case GET_PHARMACIES_BY_ID_REQUEST:
      return {
        status: true,
        message: null,
      };
    case UPDATE_PHARMACIES_REQUEST:
    case DELETE_PHARMACIES_REQUEST:
    case GET_PHARMACIES_REQUEST:
    case CREATE_PHARMACIES_REQUEST:
      return {
        status: false,
        message: null,
      };
    case GET_PHARMACIES_BY_ID_SUCCESS:
      return {
        status: false,
        message: "Sucesso",
      };
    case GET_PHARMACIES_BY_ID_ERROR:
      return {
        status: false,
        message: "Erro",
      };
    default:
      return state;
  }
};

const isCreateProcessing = (
  state = { status: false, message: null },
  action
) => {
  const { type } = action;

  switch (type) {
    case CREATE_PHARMACIES_REQUEST:
      return {
        status: true,
        message: null,
      };
    case DELETE_PHARMACIES_REQUEST:
    case GET_PHARMACIES_BY_ID_REQUEST:
    case GET_PHARMACIES_REQUEST:
    case UPDATE_PHARMACIES_REQUEST:
      return {
        status: false,
        message: null,
      };
    case CREATE_PHARMACIES_SUCCESS:
      return {
        status: false,
        message: "Sucesso",
      };
    case CREATE_PHARMACIES_ERROR:
      return {
        status: false,
        message: "Erro",
      };
    default:
      return state;
  }
};

const isUpdateProcessing = (
  state = { status: false, message: null },
  action
) => {
  const { type } = action;

  switch (type) {
    case UPDATE_PHARMACIES_REQUEST:
      return {
        status: true,
        message: null,
      };
    case DELETE_PHARMACIES_REQUEST:
    case GET_PHARMACIES_BY_ID_REQUEST:
    case GET_PHARMACIES_REQUEST:
    case CREATE_PHARMACIES_REQUEST:
      return {
        status: false,
        message: null,
      };
    case UPDATE_PHARMACIES_SUCCESS:
      return {
        status: false,
        message: "Sucesso",
      };
    case UPDATE_PHARMACIES_ERROR:
      return {
        status: false,
        message: "Erro",
      };
    default:
      return state;
  }
};

const isDeleteProcessing = (
  state = { status: false, message: null },
  action
) => {
  const { type } = action;

  switch (type) {
    case DELETE_PHARMACIES_REQUEST:
      return {
        status: true,
        message: null,
      };
    case CREATE_PHARMACIES_REQUEST:
    case GET_PHARMACIES_BY_ID_REQUEST:
    case GET_PHARMACIES_REQUEST:
    case UPDATE_PHARMACIES_REQUEST:
      return {
        status: false,
        message: null,
      };
    case DELETE_PHARMACIES_SUCCESS:
      return {
        status: false,
        message: "sucesso",
      };
    case DELETE_PHARMACIES_ERROR:
      return {
        status: false,
        message: "erro",
      };
    default:
      return state;
  }
};

//Reducers -> Manipulacao do estado
const allPharmacies = (state = { data: [] }, action) => {
  const { type, payload } = action;

  switch (type) {
    case GET_PHARMACIES_SUCCESS:
      return {
        data: payload.list,
        totalPages: payload.totalPages,
        currentPage: payload.currentPage,
        totalElements: payload.totalElements,
      };
    default:
      return state;
  }
};

//Reducers -> Manipulacao do estado
const pharmacie = (state = { data: [], metadata: [], location: [], contact: [] }, action) => {
  const { type, payload } = action;

  switch (type) {
    case GET_PHARMACIES_BY_ID_SUCCESS:
      return {
        id: payload.id,
        name: payload.name,
        description: payload.description,
        alvara: payload.alvara,
        alvaraDate: payload.alvaraDate,
        isOpen24Hours: payload.isOpen24Hours,
        isBranch: payload.isBranch,
        status: payload.status,
        metadata: payload.metadata,
        location: payload.location,
        contact: payload.contact,
      };
    default:
      return state;
  }
};

//Export All Reducers
export default combineReducers({
  allPharmacies,
  pharmacie,
  errorCreate,
  errorUpdate,
  errorDelete,
  errorGet,
  errorGetById,
  isGetProcessing,
  isCreateProcessing,
  isDeleteProcessing,
  isUpdateProcessing,
  isGetByIdProcessing,
});
