import React, { useEffect, useState } from "react";
import {
  Input,
  Row,
  Col,
  Button,
  Form,
  PageHeader,
  Spin,
  notification,
  Select,
} from "antd";
import { SaveOutlined } from "@ant-design/icons";
import styled from "styled-components";
import { saveData, getIsCreateProcessing, getCreateError } from "./redux";
import {
  getAllManufacturers,
  getData as getManufacturerData,
  getError,
  getIsGetProcessing,
} from "./fabricantes/redux";
import {
  getAllMedicinesGroup,
  getData,
  getError as groupError,
  getIsGetProcessing as isGroupProcessing,
} from "./grupos/redux";

import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";

const { Option } = Select;

const Novo = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const isSaveProcessing = useSelector(getIsCreateProcessing);
  const errorCreate = useSelector(getCreateError);

  const [medicineGroups, setMedicineGroups] = useState();
  const [manufacturers, setManufacturers] = useState();

  //Store State
  const allManufacturers = useSelector(getAllManufacturers);
  const allMedicineGroups = useSelector(getAllMedicinesGroup);

  //Processing status
  const isProcessing = useSelector(getIsGetProcessing);
  const isGetGroupProcessing = useSelector(isGroupProcessing);

  //Error status
  const errorGet = useSelector(getError);
  const errorGetGroups = useSelector(groupError);

  //Local Variables
  const SUCESSO = "sucesso";

  //useEffect Operations
  useEffect(() => {
    dispatch(getManufacturerData({ page: "", searchTerm: "", size: "" }));
  }, []);

  useEffect(() => {
    if (allManufacturers !== undefined) {
      setManufacturers(allManufacturers.data);
    }
  }, [allManufacturers, isProcessing, errorGet]);

  //useEffect Operations
  useEffect(() => {
    dispatch(getData({ page: "", searchTerm: "", size: "" }));
  }, []);

  useEffect(() => {
    if (allMedicineGroups !== undefined) {
      setMedicineGroups(allMedicineGroups.data);
    }
  }, [allMedicineGroups, isGroupProcessing, groupError]);

  const onFinish = (formValues) => {
    dispatch(
      saveData({
        composition: formValues.composition,
        description: formValues.description,
        format: formValues.format,
        groupId: formValues.group_id,
        image: '',
        manufacturerId: formValues.manufacturer_id,
        name: formValues.name,
        referencePrice: formValues.reference_price,
      })
    );
  };

  useEffect(() => {
    if (isSaveProcessing.message !== null) {
      if (
        isSaveProcessing.message.toLowerCase() == "Sucesso".toLowerCase() &&
        !isSaveProcessing.status
      ) {
        openNotificationWithIcon("success", "Sucesso", "Inserido com Sucesso!");
        history.push("/app/medicamentos");
      } else {
        openNotificationWithIcon("error", "Erro", "" + errorCreate);
      }
    }
  }, [isSaveProcessing, errorCreate]);

  const openNotificationWithIcon = (tipo, titulo, mensagem) => {
    notification[tipo]({
      message: titulo,
      description: mensagem,
    });
  };

  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 8 },
      md: { span: 8 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 16 },
      md: { span: 16 },
    },
  };

  const generateGroups = (data) => {
    const generatedOptions =
      data &&
      data.map((item) => {
        return <Option value={item.id}>{item.name}</Option>;
      });

    return generatedOptions;
  };

  const generateManufacturers = (data) => {
    const generatedOptions =
      data &&
      data.map((item) => {
        return <Option value={item.id}>{item.name}</Option>;
      });

    return generatedOptions;
  };

  return (
    <Row className="ccc-layout-content-general">
      <Col span={24}>
        <PageHeader
          className="site-page-header-responsive"
          onBack={() => window.history.back()}
          title="Adiconar Nova Maquina"
          subTitle=""
        >
          <Row>
            <Col span={24}>
              <Spin
                spinning={isSaveProcessing.status}
                delay={500}
                size="large"
                tip="Guardando os Dados..."
              >
                <FormStyled className="ccc-form">
                  <Form
                    {...formItemLayout}
                    name="add-medicamento"
                    initialValues={{ remember: true }}
                    onFinish={onFinish}
                  >
                    <Row gutter={48}>
                      <Col span={12} offset={4}>
                        <Form.Item
                          name="name"
                          label="Nome"
                          rules={[
                            {
                              required: true,
                              message:
                                "Por favor informe a Designação do Medicameto!",
                            },
                          ]}
                        >
                          <Input
                            placeholder="Designação do Medicameto"
                            className="text-center"
                          />
                        </Form.Item>

                        <Form.Item
                          name="description"
                          label="Descrição"
                          rules={[
                            {
                              required: true,
                              message: "Por favor informe a Descrição!",
                            },
                          ]}
                        >
                          <Input
                            placeholder="Descrição"
                            className="text-center"
                          />
                        </Form.Item>

                        <Form.Item
                          name="format"
                          label="Forma"
                          rules={[
                            {
                              required: true,
                              message: "Por favor informe a Forma!",
                            },
                          ]}
                        >
                          <Input placeholder="Forma" className="text-center" />
                        </Form.Item>

                        <Form.Item
                          name="composition"
                          label="Composição"
                          rules={[
                            {
                              required: true,
                              message: "Por favor informe a Composição!",
                            },
                          ]}
                        >
                          <Input
                            placeholder="Composição"
                            className="text-center"
                          />
                        </Form.Item>

                        <Form.Item name="group_id" label="Grupo">
                          <Select
                            showSearch
                            placeholder="Seleccione o Grupo"
                            optionFilterProp="children"
                            filterOption={(input, option) =>
                              option.children
                                .toLowerCase()
                                .indexOf(input.toLowerCase()) >= 0
                            }
                            filterSort={(optionA, optionB) =>
                              optionA.children
                                .toLowerCase()
                                .localeCompare(optionB.children.toLowerCase())
                            }
                          >
                            {generateGroups(medicineGroups)}
                          </Select>
                        </Form.Item>

                        <Form.Item name="manufacturer_id" label="Fabricante">
                          <Select
                            showSearch
                            placeholder="Seleccione o Fabricante"
                            optionFilterProp="children"
                            filterOption={(input, option) =>
                              option.children
                                .toLowerCase()
                                .indexOf(input.toLowerCase()) >= 0
                            }
                            filterSort={(optionA, optionB) =>
                              optionA.children
                                .toLowerCase()
                                .localeCompare(optionB.children.toLowerCase())
                            }
                          >
                            {generateManufacturers(manufacturers)}
                          </Select>
                        </Form.Item>

                        <Form.Item
                          name="reference_price"
                          label="Preço de Referência"
                          rules={[
                            {
                              required: true,
                              message:
                                "Por favor informe o Preço de Referência!",
                            },
                          ]}
                        >
                          <Input
                            type="number"
                            placeholder="Preço de Referência"
                            className="text-center"
                          />
                        </Form.Item>
                      </Col>
                    </Row>

                    <Row>
                      <Col span={24}>
                        <div className="separator"></div>
                      </Col>
                    </Row>
                    <Row>
                      <Col span={8} offset={8}>
                        <Form.Item className="botoes">
                          <Button
                            type="primary"
                            htmlType="submit"
                            className="edoc-btn-sec"
                            icon={<SaveOutlined />}
                            loading={isSaveProcessing.status}
                          >
                            Gravar
                          </Button>
                        </Form.Item>
                      </Col>
                    </Row>
                  </Form>
                </FormStyled>
              </Spin>
            </Col>
          </Row>
        </PageHeader>
      </Col>
    </Row>
  );
};

const FormStyled = styled.div`
  .botoes .ant-form-item-control-input-content {
    display: flex;
    justify-content: center;
    align-items: center;
  }

  button {
    margin-left: 20px;
  }

  .separator {
    border-bottom: 1px solid #e4e2e2;
    margin: 20px 0px;
  }

  .upload-column {
    display: flex;
    justify-content: center;
    align-items: center;
  }

  .ant-upload-picture-card-wrapper {
    width: auto;
  }

  .ant-input:placeholder-shown {
    text-align: left;
  }

  .ant-upload.ant-upload-select-picture-card {
    text-align: center;
    width: 100%;
    height: 100%;
    vertical-align: top;
    background-color: unset;
    border: none;
  }

  .ant-upload.ant-upload-select-picture-card > .ant-upload {
    flex-direction: column;
  }

  .preview-uploaded-image {
    height: 150px;
    width: 150px;
    border: 1px solid #bbb6b6;
    border-radius: 6px;
    margin: 20px;
    overflow: hidden;
  }
`;

export default Novo;
