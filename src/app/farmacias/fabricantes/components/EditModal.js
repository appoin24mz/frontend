import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import {
  Input,
  Row,
  Col,
  Button,
  Form,
  Spin,
  notification,
  Modal,
  Skeleton,
  Select,
} from "antd";
import { CloseOutlined, SaveOutlined } from "@ant-design/icons";
import {
  getManufacturer,
  getDataById,
  updateRow,
  getIsUpdateProcessing,
  getUpdateError,
  getIsGetByIdProcessing,
  getByIdError,
  getData,
} from "../redux";
import styled from "styled-components";

const { Option } = Select;

const EditModal = ({ isEditModalVisible, handleCancel, id }) => {
  const dispatch = useDispatch();

  const [form] = Form.useForm();

  //Local Variables
  const SUCESSO = "sucesso";

  //Store State
  const manufacturer = useSelector(getManufacturer);

  //Processing status
  const isGetByIdProcessing = useSelector(getIsGetByIdProcessing);
  const isUpdateProcessing = useSelector(getIsUpdateProcessing);

  //Error status
  const errorGetById = useSelector(getByIdError);
  const errorUpdate = useSelector(getUpdateError);

  //Local State
  const [isDisable, setIsDisable] = useState(false);
  const [confirmLoading, setConfirmLoading] = useState(false);

  const onFinish = (formValues) => {
    setConfirmLoading(true);
    dispatch(
      updateRow({
        code: id,
        name: formValues.name,
        description: formValues.description,
        occupationArea: formValues.occupation_area,
        speciality: formValues.speciality,
        logo: formValues.logo,
        status: formValues.status,
      })
    );
  };

  //useEffect Operations
  useEffect(() => {
    if (id !== undefined) {
      dispatch(getDataById(id));
    }
  }, [id]);

  useEffect(() => {
    if (manufacturer !== undefined) {
      onFill();
      console.log("DATA: ", manufacturer);
    }
  }, [isGetByIdProcessing, manufacturer, dispatch]);

  //Update Processing
  useEffect(() => {
    if (isUpdateProcessing.message !== null) {
      if (
        isUpdateProcessing.message.toLowerCase() == SUCESSO &&
        !isUpdateProcessing.status
      ) {
        openNotificationWithIcon(
          "success",
          "Sucesso",
          "Registo Actualizado com Sucesso!"
        );
        setConfirmLoading(false);
        dispatch(getData({ page: "", searchTerm: "", size: "" }));
      } else {
        openNotificationWithIcon("error", "Erro", "" + errorUpdate.message);
      }
    }
  }, [isUpdateProcessing, errorUpdate]);

  //Local Functions
  const onFill = () => {
    form.setFieldsValue({
      name: manufacturer.name,
      description: manufacturer.description,
      status: manufacturer.status,
      logo: manufacturer.logo,
      speciality: manufacturer.speciality,
      occupation_area: manufacturer.occupationArea,
    });
  };

  const openNotificationWithIcon = (tipo, titulo, mensagem) => {
    notification[tipo]({
      message: titulo,
      description: mensagem,
    });
  };

  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 8 },
      md: { span: 8 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 16 },
      md: { span: 16 },
    },
  };

  return (
    <Modal
      width={900}
      title={"Alterar dados do Fabricante"}
      closable={false}
      visible={isEditModalVisible}
      confirmLoading={confirmLoading}
      footer={null}
    >
      <Skeleton loading={isGetByIdProcessing.status} active>
        {onFill()}
        <Row>
          <Col span={24}>
            <Skeleton loading={isUpdateProcessing.status}>
              <FormStyled className="ccc-form">
                <Form
                  {...formItemLayout}
                  name="edit-manufacturer"
                  form={form}
                  initialValues={{ remember: true }}
                  onFinish={onFinish}
                >
                  <Row gutter={48}>
                    <Col span={12} offset={4}>
                      <Form.Item
                        name="name"
                        label="Nome do Grupo"
                        rules={[
                          {
                            required: true,
                            message: "Por favor informe o Nome do Grupo!",
                          },
                        ]}
                      >
                        <Input
                          placeholder="Nome do Grupo"
                          className="text-center"
                        />
                      </Form.Item>

                      <Form.Item
                        name="description"
                        label="Descrição"
                        rules={[
                          {
                            required: true,
                            message: "Por favor informe a Descrição!",
                          },
                        ]}
                      >
                        <Input
                          placeholder="Descrição"
                          className="text-center"
                        />
                      </Form.Item>

                      <Form.Item
                        name="speciality"
                        label="Especialidade"
                        rules={[
                          {
                            required: true,
                            message: "Por favor informe a Especialidade!",
                          },
                        ]}
                      >
                        <Input
                          placeholder="Especialidade"
                          className="text-center"
                        />
                      </Form.Item>

                      <Form.Item
                        name="occupation_area"
                        label="Area de Ocupação"
                        rules={[
                          {
                            required: true,
                            message: "Por favor informe a Area de Opcupacao!",
                          },
                        ]}
                      >
                        <Input
                          placeholder="Area de Ocupacao"
                          className="text-center"
                        />
                      </Form.Item>

                      <Form.Item name="status" label="Estado">
                        <Select
                          showSearch
                          placeholder="Seleccione o Estado"
                          optionFilterProp="children"
                          filterOption={(input, option) =>
                            option.children
                              .toLowerCase()
                              .indexOf(input.toLowerCase()) >= 0
                          }
                          filterSort={(optionA, optionB) =>
                            optionA.children
                              .toLowerCase()
                              .localeCompare(optionB.children.toLowerCase())
                          }
                        >
                          <Option value="Disponivel">Disponivel</Option>
                          <Option value="Indisponivel">Indisponivel</Option>
                        </Select>
                      </Form.Item>
                    </Col>
                  </Row>

                  <Row>
                    <Col span={24}>
                      <div className="separator"></div>
                    </Col>
                  </Row>
                  <Row>
                    <Col span={8} offset={8}>
                      <Form.Item className="botoes">
                        <Button
                          type="primary"
                          htmlType="button"
                          className=""
                          icon={<CloseOutlined />}
                          danger
                          onClick={handleCancel}
                        >
                          Cancelar
                        </Button>
                        <Button
                          type="primary"
                          htmlType="submit"
                          className=""
                          icon={<SaveOutlined />}
                          loading={isUpdateProcessing.status}
                        >
                          Gravar
                        </Button>
                      </Form.Item>
                    </Col>
                  </Row>
                </Form>
              </FormStyled>
            </Skeleton>
          </Col>
        </Row>
      </Skeleton>
    </Modal>
  );
};

const FormStyled = styled.div`
  .botoes .ant-form-item-control-input-content {
    display: flex;
    justify-content: center;
    align-items: center;
  }

  button {
    margin-left: 20px;
  }

  .separator {
    border-bottom: 1px solid #e4e2e2;
    margin: 20px 0px;
  }

  .upload-column {
    display: flex;
    justify-content: center;
    align-items: center;
  }

  .ant-upload-picture-card-wrapper {
    width: auto;
  }

  .ant-upload.ant-upload-select-picture-card {
    text-align: center;
    width: 100%;
    height: 100%;
    vertical-align: top;
    background-color: unset;
    border: none;
  }

  .ant-upload.ant-upload-select-picture-card > .ant-upload {
    flex-direction: column;
  }

  .preview-uploaded-image {
    height: 150px;
    width: 150px;
    border: 1px solid #bbb6b6;
    border-radius: 6px;
    margin: 20px;
    overflow: hidden;
  }
`;

export default EditModal;
