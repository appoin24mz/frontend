import { combineReducers } from "redux";
import API from "../../../shared/api";

//Actions Creators 
//Post Request
export const saveData = ({ logo, name, occupationArea, speciality}) => async (dispatch) => {
  dispatch({ type: CREATE_MANUFACTURER_REQUEST });
  try {
    const response = await API.post("/medicine/manufacturers/register", { logo, name, occupationArea, speciality });
    dispatch({ type: CREATE_MANUFACTURER_SUCCESS, payload: response });
  } catch (error) {
    dispatch({ type: CREATE_MANUFACTURER_ERROR, payload: error });
  }
};

//Update Request
export const updateRow = ({ code, logo, name, occupationArea, speciality, status }) => async (dispatch) => {
  dispatch({ type: UPDATE_MANUFACTURER_REQUEST });
  try {
    const response = await API.post(`/medicine/manufacturers/${code}/update`, { logo, name, occupationArea, speciality, status });
    dispatch({ type: UPDATE_MANUFACTURER_SUCCESS, payload: response });
  } catch (error) {
    dispatch({ type: UPDATE_MANUFACTURER_ERROR, payload: error });
  }
};

//Delete Request
export const deleteRow = ({ code }) => async (dispatch) => {
  dispatch({ type: DELETE_MANUFACTURER_REQUEST });

  try {
    const response = await API.post(`/medicine/manufacturers/${code}/delete`);
    dispatch({ type: DELETE_MANUFACTURER_SUCCESS, payload: response });
  } catch (error) {
    dispatch({ type: DELETE_MANUFACTURER_ERROR, payload: error });
  }
};

//Get Request
export const getData = ({ page, searchTerm, size }) => async (dispatch) => {
  dispatch({ type: GET_MANUFACTURER_REQUEST });

  try {
    const response = await API.get("/medicine/manufacturers", { page, searchTerm, size });
    dispatch({ type: GET_MANUFACTURER_SUCCESS, payload: response });
  } catch (error) {
    dispatch({ type: GET_MANUFACTURER_ERROR, payload: error });
  }
};

//Get by ID Request
export const getDataById = (code) => async (dispatch) => {
  dispatch({ type: GET_MANUFACTURER_BY_ID_REQUEST });

  try {
    const response = await API.get(`/medicine/manufacturers/${code}`);
    dispatch({ type: GET_MANUFACTURER_BY_ID_SUCCESS, payload: response });
  } catch (error) {
    dispatch({ type: GET_MANUFACTURER_BY_ID_ERROR, payload: error });
  }
};

//Selectors 
export const getAllManufacturers = state => state.manufacturers.allManufacturers;
export const getManufacturer = state => state.manufacturers.manufacturer;
export const getIsDeleteProcessing = state => state.manufacturers.isDeleteProcessing;
export const getIsGetProcessing = state => state.manufacturers.isGetProcessing;
export const getIsGetByIdProcessing = state => state.manufacturers.isGetByIdProcessing;
export const getIsCreateProcessing = state => state.manufacturers.isCreateProcessing;
export const getIsUpdateProcessing = state => state.manufacturers.isUpdateProcessing;
export const getError = state => state.manufacturers.errorGet;
export const getByIdError = state => state.manufacturers.errorGetById;
export const getCreateError = state => state.manufacturers.errorCreate;
export const getDeleteError = state => state.manufacturers.errorDelete;
export const getUpdateError = state => state.manufacturers.errorUpdate;

// ---------------------------
//Accoes
//Requests
const GET_MANUFACTURER_REQUEST = "GET_MANUFACTURER_REQUEST";
const GET_MANUFACTURER_BY_ID_REQUEST = "GET_MANUFACTURER_BY_ID_REQUEST";
const CREATE_MANUFACTURER_REQUEST = "CREATE_MANUFACTURER_REQUEST";
const DELETE_MANUFACTURER_REQUEST = "DELETE_MANUFACTURER_REQUEST";
const UPDATE_MANUFACTURER_REQUEST = "UPDATE_MANUFACTURER_REQUEST";
//Erros
const GET_MANUFACTURER_ERROR = "GET_MANUFACTURER_ERROR";
const GET_MANUFACTURER_BY_ID_ERROR = "GET_MANUFACTURER_BY_ID_ERROR";
const CREATE_MANUFACTURER_ERROR = "CREATE_MANUFACTURER_ERROR";
const UPDATE_MANUFACTURER_ERROR = "UPDATE_MANUFACTURER_ERROR";
const DELETE_MANUFACTURER_ERROR = "DELETE_MANUFACTURER_ERROR";

//Sucesso
const CREATE_MANUFACTURER_SUCCESS = "CREATE_MANUFACTURER_SUCCESS";
const GET_MANUFACTURER_SUCCESS = "GET_MANUFACTURER_SUCCESS";
const GET_MANUFACTURER_BY_ID_SUCCESS = "GET_MANUFACTURER_BY_ID_SUCCESS";
const DELETE_MANUFACTURER_SUCCESS = "DELETE_MANUFACTURER_SUCCESS";
const UPDATE_MANUFACTURER_SUCCESS = "UPDATE_MANUFACTURER_SUCCESS";

// ---------------------------

//Reducers -> Mensagens de Erro

const errorCreate = (state = null, action) => {
  const { type, payload } = action;

  switch (type) {
    case CREATE_MANUFACTURER_ERROR:
      return { message: payload.message, httpcode: payload.httpcode };
    default:
      return state;
  }
};

const errorUpdate = (state = null, action) => {
  const { type, payload } = action;

  switch (type) {
    case UPDATE_MANUFACTURER_ERROR:
      return { message: payload.message, httpcode: payload.httpcode };
    default:
      return state;
  }
};

const errorDelete = (state = null, action) => {
  const { type, payload } = action;

  switch (type) {
    case DELETE_MANUFACTURER_ERROR:
      return { message: payload.message, httpcode: payload.httpcode };
    default:
      return state;
  }
};

const errorGet = (state = null, action) => {
  const { type, payload } = action;

  switch (type) {
    case GET_MANUFACTURER_ERROR:
      return { message: payload.message, httpcode: payload.httpcode };
    default:
      return state;
  }
};

const errorGetById = (state = null, action) => {
  const { type, payload } = action;

  switch (type) {
    case GET_MANUFACTURER_BY_ID_ERROR:
      return { message: payload.message, httpcode: payload.httpcode };
    default:
      return state;
  }
};

const isGetProcessing = (state = { status: false, message: null }, action) => {
  const { type } = action;

  switch (type) {
    case GET_MANUFACTURER_REQUEST:
      return {
        status: true,
        message: null
      };
    case UPDATE_MANUFACTURER_REQUEST:
    case DELETE_MANUFACTURER_REQUEST:
    case GET_MANUFACTURER_BY_ID_REQUEST:
    case CREATE_MANUFACTURER_REQUEST:
      return {
        status: false,
        message: null
      };
    case GET_MANUFACTURER_SUCCESS:
      return {
        status: false,
        message: 'Sucesso'
      };
    case GET_MANUFACTURER_ERROR:
      return {
        status: false,
        message: 'Erro'
      };
    default:
      return state;
  }
};

const isGetByIdProcessing = (state = { status: false, message: null }, action) => {
  const { type } = action;

  switch (type) {
    case GET_MANUFACTURER_BY_ID_REQUEST:
      return {
        status: true,
        message: null
      };
    case UPDATE_MANUFACTURER_REQUEST:
    case DELETE_MANUFACTURER_REQUEST:
    case GET_MANUFACTURER_REQUEST:
    case CREATE_MANUFACTURER_REQUEST:
      return {
        status: false,
        message: null
      };
    case GET_MANUFACTURER_BY_ID_SUCCESS:
      return {
        status: false,
        message: 'Sucesso'
      };
    case GET_MANUFACTURER_BY_ID_ERROR:
      return {
        status: false,
        message: 'Erro'
      };
    default:
      return state;
  }
};

const isCreateProcessing = (state = { status: false, message: null }, action) => {
  const { type } = action;

  switch (type) {
    case CREATE_MANUFACTURER_REQUEST:
      return {
        status: true,
        message: null
      };
    case DELETE_MANUFACTURER_REQUEST:
    case GET_MANUFACTURER_BY_ID_REQUEST:
    case GET_MANUFACTURER_REQUEST:
    case UPDATE_MANUFACTURER_REQUEST:
      return {
        status: false,
        message: null
      };
    case CREATE_MANUFACTURER_SUCCESS:
      return {
        status: false,
        message: 'Sucesso'
      };
    case CREATE_MANUFACTURER_ERROR:
      return {
        status: false,
        message: 'Erro'
      };
    default:
      return state;
  }
};

const isUpdateProcessing = (state = { status: false, message: null }, action) => {
  const { type } = action;

  switch (type) {
    case UPDATE_MANUFACTURER_REQUEST:
      return {
        status: true,
        message: null
      };
    case DELETE_MANUFACTURER_REQUEST:
    case GET_MANUFACTURER_BY_ID_REQUEST:
    case GET_MANUFACTURER_REQUEST:
    case CREATE_MANUFACTURER_REQUEST:
      return {
        status: false,
        message: null
      };
    case UPDATE_MANUFACTURER_SUCCESS:
      return {
        status: false,
        message: 'Sucesso'
      };
    case UPDATE_MANUFACTURER_ERROR:
      return {
        status: false,
        message: 'Erro'
      };
    default:
      return state;
  }
};

const isDeleteProcessing = (state = { status: false, message: null }, action) => {
  const { type } = action;

  switch (type) {
    case DELETE_MANUFACTURER_REQUEST:
      return {
        status: true,
        message: null
      };
    case CREATE_MANUFACTURER_REQUEST:
    case GET_MANUFACTURER_BY_ID_REQUEST:
    case GET_MANUFACTURER_REQUEST:
    case UPDATE_MANUFACTURER_REQUEST:
      return {
        status: false,
        message: null
      }
    case DELETE_MANUFACTURER_SUCCESS:
      return {
        status: false,
        message: 'sucesso'
      };
    case DELETE_MANUFACTURER_ERROR:
      return {
        status: false,
        message: 'erro'
      };
    default:
      return state;
  }
};

//Reducers -> Manipulacao do estado
const allManufacturers = (state = { data: [] }, action) => {
  const { type, payload } = action;

  switch (type) {
    case GET_MANUFACTURER_SUCCESS:
      return {
        data: payload.list,
        totalPages: payload.totalPages,
        currentPage: payload.currentPage,
        totalElements: payload.totalElements
      };
    default:
      return state;
  }
};

//Reducers -> Manipulacao do estado
const manufacturer = (state = { metadata: [] }, action) => {
  const { type, payload } = action;

  switch (type) {
    case GET_MANUFACTURER_BY_ID_SUCCESS:
      return {
        id: payload.id,
        name: payload.name,
        description: payload.description,
        logo: payload.logo,
        occupationArea: payload.occupationArea,
        speciality: payload.speciality,
        status: payload.status,
        metadata: payload.metadata,
      };
    default:
      return state;
  }
};

//Export All Reducers 
export default combineReducers({
  allManufacturers,
  manufacturer,
  errorCreate,
  errorUpdate,
  errorDelete,
  errorGet,
  errorGetById,
  isGetProcessing,
  isCreateProcessing,
  isDeleteProcessing,
  isUpdateProcessing,
  isGetByIdProcessing
});

