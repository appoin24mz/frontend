import React, { useEffect } from "react";
import {
  Row,
  Col,
  PageHeader,
  Button,
  Skeleton,
  Statistic,
  Descriptions,
  Modal,
  Tag,
  notification,
} from "antd";
import {
  getPharmacie,
  deleteRow,
  getIsGetByIdProcessing,
  getIsDeleteProcessing,
  getByIdError,
  getDeleteError,
  getDataById,
} from "./redux";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, useParams } from "react-router-dom";
import {
  ExclamationCircleOutlined,
  CheckCircleOutlined,
  CloseCircleOutlined,
} from "@ant-design/icons";

const { confirm } = Modal;

const Detalhes = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  //Local Variables
  const SUCESSO = "sucesso";
  let { id } = useParams();

  //Local State

  //Store State
  const pharmacie = useSelector(getPharmacie);

  //Processing status
  const isGetByIdProcessing = useSelector(getIsGetByIdProcessing);
  const isDeleteProcessing = useSelector(getIsDeleteProcessing);

  //Error status
  const errorDelete = useSelector(getDeleteError);
  const errorGetById = useSelector(getByIdError);

  //useEffect Operations
  useEffect(() => {
    dispatch(getDataById(id));
  }, []);

  useEffect(() => {
    if (isGetByIdProcessing.message !== null) {
      if (
        isGetByIdProcessing.message.toLowerCase() == "Sucesso".toLowerCase() &&
        !isGetByIdProcessing.status
      ) {
        console.info("Carregado com sucesso");
      } else {
        openNotificationWithIcon("error", "Erro", "" + errorGetById.message);
      }
    }
  }, [isGetByIdProcessing, errorGetById]);

  //Delete Processing
  useEffect(() => {
    if (isDeleteProcessing.message !== null) {
      if (
        isDeleteProcessing.message.toLowerCase() == SUCESSO &&
        !isDeleteProcessing.status
      ) {
        openNotificationWithIcon(
          "success",
          "Sucesso",
          "Registo Eliminado com Sucesso!"
        );
        history.push("/app/farmacias");
      } else {
        openNotificationWithIcon("error", "Erro", "" + errorDelete.message);
      }
    }
  }, [isDeleteProcessing, errorDelete]);

  //Local Functions

  // const handleUpdate = (id) => {
  //   history.push(`/app/alterar-medicamento/${id}`);
  // };

  const handleShowAllpharmacies = () => {
    history.push("/app/farmacias");
  };

  const openNotificationWithIcon = (tipo, titulo, mensagem) => {
    notification[tipo]({
      message: titulo,
      description: mensagem,
    });
  };

  const handleDelete = (id) => {
    showDeleteConfirm(id);
  };

  const showDeleteConfirm = (id) => {
    confirm({
      title: "Tem certeza que quer APAGAR/EXCLUIR esse Registo?",
      icon: <ExclamationCircleOutlined />,
      content: "",
      okText: "Sim",
      okType: "danger",
      cancelText: "Não",
      onOk() {
        dispatch(
          deleteRow({
            code: id,
          })
        );
      },
      onCancel() {
        console.log("Cancelado");
      },
    });
  };

  return (
    <Row className="ccc-layout-content-general">
      <Col span={24}>
        <PageHeader
          className="site-page-header-responsive"
          onBack={() => window.history.back()}
          title="Detalhes da Farmácia"
          subTitle=""
          extra={[
            <>
              <Button
                type="default"
                style={{ marginLeft: "10px" }}
                onClick={() => handleShowAllpharmacies()}
              >
                Todas Farmácias
              </Button>
              <Button
                type="primary"
                danger
                style={{ marginLeft: "10px" }}
                onClick={() => handleDelete(pharmacie.id)}
              >
                Excluir
              </Button>
            </>,
          ]}
        >
          <Row>
            <Col span={24}>
              <Skeleton loading={isGetByIdProcessing.status} active>
                <div className="content">
                  <div className="extra">
                    <div
                      style={{
                        display: "flex",
                        width: "max-content",
                        justifyContent: "flex-end",
                      }}
                    >
                      <Statistic
                        title="Nome"
                        value={pharmacie.name}
                        style={{
                          marginRight: 32,
                        }}
                      />
                      <Statistic title="Estado" value={pharmacie.status} />
                    </div>
                  </div>
                  <div className="main">
                    <Descriptions size="small" column={2}>
                      <Descriptions.Item label="Telefone">
                        {pharmacie.contact.phoneNumber}
                      </Descriptions.Item>
                      <Descriptions.Item label="Email">
                        {pharmacie.contact.email}
                      </Descriptions.Item>
                      <Descriptions.Item label="Telefone Alternativo">
                        {pharmacie.contact.secondaryPhoneNumber}
                      </Descriptions.Item>
                      <Descriptions.Item label="Endereço">
                        {pharmacie.location.address}
                      </Descriptions.Item>
                      <Descriptions.Item label="Provincia">
                        {pharmacie.location.province}
                      </Descriptions.Item>
                      <Descriptions.Item label="Distrito">
                        {pharmacie.location.district}
                      </Descriptions.Item>
                      <Descriptions.Item label="Descrição">
                        {pharmacie.description}
                      </Descriptions.Item>
                      <Descriptions.Item label="Alvará">
                        {pharmacie.alvara}
                      </Descriptions.Item>
                      <Descriptions.Item label="Data do Alvará">
                        {pharmacie.alvaraDate}
                      </Descriptions.Item>
                      <Descriptions.Item label="Aberto 24h?">
                        {pharmacie.isOpen24Hours ? (
                          <Tag icon={<CheckCircleOutlined />} color="success">
                            SIM
                          </Tag>
                        ) : (
                          <Tag icon={<CloseCircleOutlined />} color="error">
                            NÃO
                          </Tag>
                        )}
                      </Descriptions.Item>
                      <Descriptions.Item label="Data de Registo no Sistema">
                        {pharmacie.metadata.createdAt}
                      </Descriptions.Item>
                      <Descriptions.Item label="Farmácia Activa no Sistema?">
                        {pharmacie.metadata.isActive ? (
                          <Tag icon={<CheckCircleOutlined />} color="success">
                            SIM
                          </Tag>
                        ) : (
                          <Tag icon={<CloseCircleOutlined />} color="error">
                            NÃO
                          </Tag>
                        )}
                      </Descriptions.Item>
                    </Descriptions>
                  </div>
                </div>
              </Skeleton>
            </Col>
          </Row>
        </PageHeader>
      </Col>
    </Row>
  );
};

export default Detalhes;
