import React, { useEffect } from "react";
import {
  Row,
  Col,
  PageHeader,
  Button,
  Spin,
  Form,
  Modal,
  Input,
  Skeleton,
  notification,
} from "antd";
import {
  getMedicine,
  getIsGetByIdProcessing,
  getIsUpdateProcessing,
  getIsDeleteProcessing,
  getByIdError,
  getUpdateError,
  getDeleteError,
  getDataById,
  updateRow,
  deleteRow,
} from "./redux";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, useParams } from "react-router-dom";
import styled from "styled-components";
import { SaveOutlined, ExclamationCircleOutlined, } from "@ant-design/icons";

const { confirm } = Modal;

const Alterar = () => {
  const dispatch = useDispatch();
  const [form] = Form.useForm();
  const history = useHistory();

  //Local Variables 
  const SUCESSO = 'sucesso';
  let { id } = useParams();

  //Local State

  //Store State 
  const medicine = useSelector(getMedicine);

  //Processing status
  const isGetByIdProcessing = useSelector(getIsGetByIdProcessing);
  const isUpdateProcessing = useSelector(getIsUpdateProcessing);
  const isDeleteProcessing = useSelector(getIsDeleteProcessing);

  //Error status
  const errorDelete = useSelector(getDeleteError);
  const errorGetById = useSelector(getByIdError);
  const errorUpdate = useSelector(getUpdateError);

  //useEffect Operations
  useEffect(() => {
    dispatch(getDataById(id));
  }, []);

  useEffect(() => {
    if (isGetByIdProcessing.message !== null) {
      if (isGetByIdProcessing.message.toLowerCase() == 'Sucesso'.toLowerCase() && !isGetByIdProcessing.status) {
        console.info('Carregado com sucesso');
        onFill();
      } else {
        openNotificationWithIcon('error', 'Erro', '' + errorGetById.message);
      }
    }
  }, [isGetByIdProcessing, errorGetById]);

  //Delete Processing 
  useEffect(() => {
    if (isDeleteProcessing.message !== null) {
      if (isDeleteProcessing.message.toLowerCase() == SUCESSO && !isDeleteProcessing.status) {
        openNotificationWithIcon('success', 'Sucesso', 'Registo Eliminado com Sucesso!');
        history.push(`/app/medicamentos`);
      } else {
        openNotificationWithIcon('error', 'Erro', '' + errorDelete.message);
      }
    }
  }, [isDeleteProcessing, errorDelete]);

  //Update Processing 
  useEffect(() => {
    if (isUpdateProcessing.message !== null) {
      if (isUpdateProcessing.message.toLowerCase() == SUCESSO && !isUpdateProcessing.status) {
        openNotificationWithIcon('success', 'Sucesso', 'Registo Actualizado com Sucesso!');
        history.push(`/app/medicamentos`);
      } else {
        openNotificationWithIcon('error', 'Erro', '' + errorUpdate.message);
      }
    }
  }, [isUpdateProcessing, errorUpdate]);

  //Local Functions
  const onFill = () => {
    form.setFieldsValue({
      name: medicine.name,
      composition: medicine.composition,
      description: medicine.description,
      format: medicine.format,
      groupId: medicine.groupId,
      image: medicine.image,
      manufacturerId: medicine.manufacturerId,
      referencePrice: medicine.referencePrice,
    });
  };

  const handleShowAllmedicines = () => {
    history.push(`/app/medicamentos`);
  };

  const onFinish = (formValues) => {
    dispatch(
      updateRow({
        code: id,
        bluetoothPin: formValues.bluetooth_pin,
        brand: formValues.marca,
        macAddress: formValues.endereco_mac,
        model: formValues.modelo,
        serialNumber: formValues.numero_serie,
      })
    );
  };

  const handleDelete = (id) => {
    showDeleteConfirm(id);
  };

  const showDeleteConfirm = (id) => {
    confirm({
      title: 'Tem certeza que quer APAGAR/EXCLUIR esse Registo?',
      icon: <ExclamationCircleOutlined />,
      content: '',
      okText: 'Sim',
      okType: 'danger',
      cancelText: 'Não',
      onOk() {
        dispatch(
          deleteRow({
            code: id,
          })
        );
      },
      onCancel() {
        console.log('Cancelado');
      },
    });
  };

  const openNotificationWithIcon = (tipo, titulo, mensagem) => {
    notification[tipo]({
      message: { titulo },
      description: { mensagem }
    });
  };

  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 8 },
      md: { span: 8 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 16 },
      md: { span: 16 },
    },
  };

  return (
    <Row className="ccc-layout-content-general">
      <Col span={24}>
        <Skeleton loading={isGetByIdProcessing.status} active avatar>
          {onFill()}
          <PageHeader
            className="site-page-header-responsive"
            onBack={() => window.history.back()}
            title="Alterar Maquina"
            subTitle=""
            extra={[
              <>
                <Button
                  type="default"
                  style={{ marginLeft: "10px" }}
                  onClick={() => handleShowAllmedicines()}
                >
                  Todas Maquinas
                </Button>
                <Button
                  type="primary"
                  danger
                  style={{ marginLeft: "10px" }}
                  onClick={() => handleDelete(medicine.medicineId)}
                >
                  Excluir
                </Button>
              </>,
            ]}
          >
            <Row>
              <Col span={24}>
                <Spin
                  spinning={isGetByIdProcessing.status}
                  delay={500}
                  size="large"
                  tip="Guardando os Dados..."
                >
                  <FormStyled className="ccc-form">
                    <Form
                      name="alterar-maquina"
                      form={form}
                      {...formItemLayout}
                      initialValues={{ remember: true }}
                      onFinish={onFinish}
                    >
                      <Row gutter={48}>
                        <Col span={12} offset={4}>
                          <Form.Item
                            name="marca"
                            label="Marca"
                            rules={[
                              {
                                required: true,
                                message: "Por favor informe a Marca!",
                              },
                            ]}
                          >
                            <Input
                              placeholder="Marca"
                              className="text-center"
                            />
                          </Form.Item>

                          <Form.Item
                            name="modelo"
                            label="Modelo"
                            rules={[
                              {
                                required: true,
                                message: "Por favor informe o Modelo!",
                              },
                            ]}
                          >
                            <Input
                              placeholder="Modelo"
                              className="text-center"
                            />
                          </Form.Item>

                          <Form.Item
                            name="numero_serie"
                            label="Número de Serie"
                            rules={[
                              {
                                required: true,
                                message: "Por favor informe o Número de Serie!",
                              },
                            ]}
                          >
                            <Input
                              placeholder="Número de Serie"
                              className="text-center"
                            />
                          </Form.Item>

                          <Form.Item
                            name="endereco_mac"
                            label="Endereço Mac"
                            rules={[
                              {
                                required: true,
                                message: "Por favor informe o Endereço MAC!",
                              },
                            ]}
                          >
                            <Input
                              placeholder="Endereço MAC"
                              className="text-center"
                            />
                          </Form.Item>

                          <Form.Item
                            name="bluetooth_pin"
                            label="Bluetooth PIN"
                            rules={[
                              {
                                required: true,
                                message: "Por favor informe o Bluetooth PIN!",
                              },
                            ]}
                          >
                            <Input
                              placeholder="PIN do Bluetooth"
                              className="text-center"
                            />
                          </Form.Item>
                        </Col>
                      </Row>

                      <Row>
                        <Col span={24}>
                          <div className="separator"></div>
                        </Col>
                      </Row>
                      <Row>
                        <Col span={8} offset={8}>
                          <Form.Item className="botoes">
                            <Button
                              type="primary"
                              htmlType="submit"
                              className="edoc-btn-sec"
                              icon={<SaveOutlined />}
                            >
                              Gravar
                            </Button>
                          </Form.Item>
                        </Col>
                      </Row>
                    </Form>
                  </FormStyled>
                </Spin>
              </Col>
            </Row>
          </PageHeader>
        </Skeleton>
      </Col>
    </Row>
  );
};

const FormStyled = styled.div`
  .botoes .ant-form-item-control-input-content {
    display: flex;
    justify-content: center;
    align-items: center;
  }

  button {
    margin-left: 20px;
  }

  .separator {
    border-bottom: 1px solid #e4e2e2;
    margin: 20px 0px;
  }

  .upload-column {
    display: flex;
    justify-content: center;
    align-items: center;
  }

  .ant-upload-picture-card-wrapper {
    width: auto;
  }

  .ant-upload.ant-upload-select-picture-card {
    text-align: center;
    width: 100%;
    height: 100%;
    vertical-align: top;
    background-color: unset;
    border: none;
  }

  .ant-upload.ant-upload-select-picture-card > .ant-upload {
    flex-direction: column;
  }

  .preview-uploaded-image {
    height: 150px;
    width: 150px;
    border: 1px solid #bbb6b6;
    border-radius: 6px;
    margin: 20px;
    overflow: hidden;
  }
`;

export default Alterar;
