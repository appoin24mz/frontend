import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  Row,
  Col,
  Table,
  Input,
  Button,
  Menu,
  Modal,
  Dropdown,
  PageHeader,
  Tabs,
  Spin,
  notification,
} from "antd";
import {
  EditOutlined,
  DeleteOutlined,
  PicLeftOutlined,
  DownOutlined,
  ExclamationCircleOutlined,
} from "@ant-design/icons";
import { useHistory } from "react-router-dom";

import {
  getData,
  deleteRow,
  getAllMedicinesGroup,
  getError,
  getDeleteError,
  getIsDeleteProcessing,
  getIsGetProcessing,
} from "./redux";
import AddModal from "./components/AddModal";
import EditModal from "./components/EditModal";
import DetailsModal from "./components/DetailsModal";

const { confirm } = Modal;

const Grupos = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  //Local State
  const [currentPageSize, setCurrentPageSize] = useState(5);
  const [medicineGroups, setMedicineGroups] = useState();
  const [searchVal, setSearchVal] = useState(null);
  const [id, setId] = useState();

  const [isDetailModalVisible, setIsDetailModalVisible] = useState(false);
  const [isEditModalVisible, setIsEditModalVisible] = useState(false);
  const [isAddModalVisible, setIsAddModalVisible] = useState(false);

  //Store State
  const allMedicineGroups = useSelector(getAllMedicinesGroup);

  //Processing status
  const isProcessing = useSelector(getIsGetProcessing);
  const isDeleteProcessing = useSelector(getIsDeleteProcessing);

  //Error status
  const errorGet = useSelector(getError);
  const errorDelete = useSelector(getDeleteError);

  //Local Variables
  const SUCESSO = "sucesso";
  const ADD = "ADD";
  const EDIT = "EDIT";
  const DETAILS = "DETAILS";

  //useEffect Operations
  useEffect(() => {
    dispatch(getData({ page: "", searchTerm: "", size: "" }));
  }, []);

  //Get Processing
  useEffect(() => {
    if (allMedicineGroups !== undefined) {
      setMedicineGroups(generateData(allMedicineGroups.data));
    }
  }, [isProcessing, errorGet, allMedicineGroups]);

  //Delete Processing
  useEffect(() => {
    if (isDeleteProcessing.message !== null) {
      if (
        isDeleteProcessing.message.toLowerCase() == SUCESSO &&
        !isDeleteProcessing.status
      ) {
        openNotificationWithIcon(
          "success",
          "Sucesso",
          "Registo Eliminado com Sucesso!"
        );
        dispatch(getData({ page: "", searchTerm: "", size: "" }));
      } else {
        openNotificationWithIcon("error", "Erro", "" + errorDelete.message);
      }
    }
  }, [isDeleteProcessing, errorDelete, dispatch]);

  //Local Functions
  const handleMenuOption = (record, key) => {
    if (key.key === "0") {
      handleUpdate(record.key);
    } else if (key.key === "1") {
      handleDetails(record.key);
      // console.log(record);
    } else if (key.key === "2") {
      handleDelete(record.key);
    }
  };

  const handleUpdate = (id) => {
    setId(id);
    showModal(EDIT);
  };

  const handleDetails = (id) => {
    setId(id);
    showModal(DETAILS);
  };

  const handleDelete = (id) => {
    setId(id);
    showDeleteConfirm(id);
  };

  const showDeleteConfirm = (id) => {
    confirm({
      title: "Tem certeza que quer APAGAR/EXCLUIR esse Registo?",
      icon: <ExclamationCircleOutlined />,
      content: "",
      okText: "Sim",
      okType: "danger",
      cancelText: "Não",
      onOk() {
        console.log("OK");
        dispatch(
          deleteRow({
            code: id,
          })
        );
      },
      onCancel() {
        console.log("Cancelado");
      },
    });
  };

  const menu = (record) => {
    return (
      <Menu
        className="ccc-dropdown-menu"
        onClick={(key) => handleMenuOption(record, key)}
      >
        <Menu.Item key="0">
          <EditOutlined /> Alterar
        </Menu.Item>
        <Menu.Divider />
        <Menu.Item key="1">
          <PicLeftOutlined /> Detalhar
        </Menu.Item>
        <Menu.Divider />
        <Menu.Item className="ccc-delete-item" key="2">
          <DeleteOutlined /> Excluir
        </Menu.Item>
      </Menu>
    );
  };

  const columns = [
    {
      title: "Nome do Grupo",
      dataIndex: "name",
      key: "name",
      width: "30%",
      sorter: (a, b) => a.name.length - b.name.length,
      ellipsis: true,
    },
    {
      title: "Descrição",
      dataIndex: "description",
      key: "description",
      width: "60%",
      sorter: (a, b) => a.description.length - b.description.length,
      ellipsis: true,
    },
    {
      title: "",
      dataIndex: "accoes",
      key: "accoes",
      render: (text, record) => {
        //console.log(record.key);
        return (
          <Dropdown overlay={menu(record)} trigger={["click"]}>
            <Button className="ccc-btn-sec" onClick={(e) => e.preventDefault()}>
              Acções <DownOutlined />
            </Button>
          </Dropdown>
        );
      },
    },
  ];

  const generateData = (data) => {
    const generatedData =
      data &&
      data.map((item) => {
        return {
          key: item.id,
          name: item.name,
          description: item.description,
          accoes: "",
        };
      });

    return generatedData;
  };

  const onShowSizeChange = (current, pageSize) => {
    setCurrentPageSize(pageSize);
    console.log("Current: ", currentPageSize);
    console.log(current, pageSize);
  };

  const openNotificationWithIcon = (tipo, titulo, mensagem) => {
    notification[tipo]({
      message: titulo,
      description: mensagem,
    });
  };

  const getFiltredResult = (e) => {
    console.log("FILTRED VALUE: ", e);
    dispatch(getData({ page: "", searchTerm: e, size: "" }));
  };

  const showModal = (type) => {
    console.log('Tipo: ', type);
    //dispatch(getDataById(id));
    if (type == EDIT) {
      setIsEditModalVisible(true);
    } else if (type == ADD) {
      setIsAddModalVisible(true);
    } else if (type == DETAILS) {
      setIsDetailModalVisible(true);
    }
  };

  const handleCancel = () => {
    setIsDetailModalVisible(false);
    setIsAddModalVisible(false);
    setIsEditModalVisible(false);
  };

  return (
    <Row className="ccc-layout-content-general">
      <Col span={24}>
        <Spin
          spinning={isDeleteProcessing.status}
          tip="Eliminando o Registo..."
        >
          <PageHeader
            className="site-page-header-responsive"
            onBack={() => window.history.back()}
            title="Grupo de Medicamentos"
            subTitle="Listagem de Grupo de Medicamentos"
            extra={[
              <>
                <Input.Search
                  placeholder="Procurar Grupo de Medicamentos"
                  onSearch={getFiltredResult}
                  onChange={(e) => setSearchVal(e.target.value)}
                />
                <Button type="primary" onClick={() => showModal(ADD)}>
                  Novo Grupo de Medicamento
                </Button>
              </>,
            ]}
            footer={<Tabs defaultActiveKey="1"></Tabs>}
          >
            <Row>
              <Col span={24}>
                <Table
                  columns={columns}
                  pagination={{
                    pageSize: currentPageSize,
                    total: allMedicineGroups.totalElements,
                    showTotal: (total) => `Total ${total} Registos`,
                    defaultCurrent: 1,
                    pageSizeOptions: [5, 10, 25, 50, 100],
                    onShowSizeChange: (defaultCurrent, pageSize) =>
                      onShowSizeChange(defaultCurrent, pageSize),
                    showSizeChanger: true,
                  }}
                  dataSource={medicineGroups}
                  size="small"
                  loading={isProcessing.status}
                />
              </Col>
              <Col>
                <AddModal
                  handleCancel={handleCancel}
                  isAddModalVisible={isAddModalVisible}
                />
                <EditModal
                  handleCancel={handleCancel}
                  id={id}
                  isEditModalVisible={isEditModalVisible}
                />
                <DetailsModal
                  handleCancel={handleCancel}
                  id={id}
                  isDetailModalVisible={isDetailModalVisible}
                />
              </Col>
            </Row>
          </PageHeader>
        </Spin>
      </Col>
    </Row>
  );
};

export default Grupos;
