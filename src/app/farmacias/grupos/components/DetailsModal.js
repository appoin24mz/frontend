import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  Row,
  Col,
  Form,
  notification,
  Modal,
  Skeleton,
  Statistic,
  Descriptions,
} from "antd";
import {
  getMedicineGroup,
  getDataById,
  getIsGetByIdProcessing,
  getByIdError,
} from "../redux";

const DetailsModal = ({ isDetailModalVisible, handleCancel, id }) => {
  const dispatch = useDispatch();

  const [form] = Form.useForm();

  //Local Variables
  const SUCESSO = "sucesso";

  //Store State
  const medicineGroup = useSelector(getMedicineGroup);

  //Processing status
  const isGetByIdProcessing = useSelector(getIsGetByIdProcessing);

  //Error status
  const errorGetById = useSelector(getByIdError);

  //useEffect Operations
  useEffect(() => {
    if (id !== undefined) {
      dispatch(getDataById(id));
    }
  }, [id]);

  useEffect(() => {
    if (medicineGroup !== undefined) {
      console.log("DATA: ", medicineGroup);
    }
  }, [isGetByIdProcessing, medicineGroup, dispatch]);

  //Local Functions
  const openNotificationWithIcon = (tipo, titulo, mensagem) => {
    notification[tipo]({
      message: titulo,
      description: mensagem,
    });
  };

  return (
    <Modal
      title={"Detalhes do Grupo"}
      closable={true}
      onCancel={handleCancel}
      visible={isDetailModalVisible}
      footer={null}
    >
      <Skeleton loading={isGetByIdProcessing.status} active>
        <Row>
          <Col span={24}>
            <Skeleton loading={isGetByIdProcessing.status} active>
              <div className="content">
                <div className="extra">
                  <div
                    style={{
                      display: "flex",
                      width: "max-content",
                      justifyContent: "flex-end",
                    }}
                  >
                    <Statistic
                      title="Nome"
                      value={medicineGroup.name}
                      style={{
                        marginRight: 32,
                      }}
                    />
                  </div>
                </div>
                <div className="main">
                  <Descriptions size="small" column={1}>
                    <Descriptions.Item label="Descrição">
                      {medicineGroup.description}
                    </Descriptions.Item>
                  </Descriptions>
                </div>
              </div>
            </Skeleton>
          </Col>
        </Row>
      </Skeleton>
    </Modal>
  );
};

export default DetailsModal;
