import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  Row,
  Col,
  Table,
  Statistic,
  Button,
  Descriptions,
  Modal,
  Skeleton,
  PageHeader,
  DatePicker,
  Tabs,
  Spin,
  notification,
} from "antd";
import {
  FileSearchOutlined,
  DeleteOutlined,
  PicLeftOutlined,
  DownOutlined,
  ExclamationCircleOutlined,
} from "@ant-design/icons";
import {
  useHistory,
} from "react-router-dom";

import {
  getData,
  getDataById,
  getAllOperations,
  getOperation,
  getError,
  getByIdError,
  getIsGetProcessing,
  getIsGetByIdProcessing,
} from "./redux";

const { confirm } = Modal;

const Maquinas = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  //Local State
  const [currentPageSize, setCurrentPageSize] = useState(5);
  const [operations, setOperations] = useState();
  const [searchVal, setSearchVal] = useState(null);
  const [isModalVisible, setIsModalVisible] = useState(false);

  //Store State 
  const allOperations = useSelector(getAllOperations);
  const operation = useSelector(getOperation);

  //Processing status
  const isProcessing = useSelector(getIsGetProcessing);
  const isGetByIdProcessing = useSelector(getIsGetByIdProcessing);

  //Error status
  const errorGet = useSelector(getError);
  const errorGetById = useSelector(getByIdError);

  //Local Variables 
  const SUCESSO = 'sucesso';

  //useEffect Operations
  useEffect(() => {
    dispatch(getData({ page: "", searchTerm: "", size: "" }));
  }, []);

  //Get Processing 
  useEffect(() => {
    if (isProcessing.message !== null) {
      if (isProcessing.message.toLowerCase() == SUCESSO && !isProcessing.status) {
        console.info('Carregado com sucesso');
        console.log('Operations', generateOperations(allOperations.data));
        setOperations(generateOperations(allOperations.data));
      } else {
        openNotificationWithIcon('error', 'Erro', '' + errorGet.message);
      }
    }
  }, [isProcessing, errorGet]);


  //GetById Processing
    useEffect(() => {
      if (isGetByIdProcessing.message !== null) {
        if (isGetByIdProcessing.message.toLowerCase() == 'Sucesso'.toLowerCase() && !isGetByIdProcessing.status) {
          console.info('Carregado com sucesso');
        } else {
          openNotificationWithIcon('error', 'Erro', '' + errorGetById.message);
        }
      }
    }, [isGetByIdProcessing, errorGetById]);

  //Local Functions 

  const onChange = (date, dateString) => {
    console.log(date, dateString);
  };

  const showModal = (id) => {
    dispatch(getDataById(id));
    setIsModalVisible(true);
  };

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const handleDetails = (record) => {
    //Correr Modal com os detalhes da operação
    console.log('Record: ',record);
    showModal(record.key);
  };

  const handleOnShowSizeChange = (current, pageSize) => {
    console.log("Clicked: " + current);
  };

  const getOperations = (e) =>{
    console.log('getOperactions Action: ', e);
  };

  const columns = [
    {
      title: "Nº do Documento",
      dataIndex: "numero_documento",
      key: "numero_documento",
      width: "10%",
      sorter: (a, b) => a.numero_documento.length - b.numero_documento.length,
      ellipsis: true,
    },
    {
      title: "Inicio da Operação",
      dataIndex: "inicio",
      key: "inicio",
      width: "15%",
      sorter: (a, b) => a.inicio.length - b.inicio.length,
      ellipsis: true,
    },
    {
      title: "Fim da Operação",
      dataIndex: "fim",
      key: "fim",
      width: "15%",
      sorter: (a, b) => a.fim.length - b.fim.length,
      ellipsis: true,
    },
    {
      title: "Tipo de Dispositivo",
      dataIndex: "tipo_device",
      key: "tipo_device",
      width: "10%",
      sorter: (a, b) => a.tipo_device.length - b.tipo_device.length,
      ellipsis: true,
    },
    {
      title: "Tipo",
      dataIndex: "tipo",
      key: "tipo",
      width: "10%",
      sorter: (a, b) => a.tipo.length - b.tipo.length,
      ellipsis: true,
    },
    {
      title: "Valor",
      dataIndex: "valor",
      key: "valor",
      width: "10%",
      sorter: (a, b) => a.valor.length - b.valor.length,
      ellipsis: true,
    },
    {
      title: "Maquina",
      dataIndex: "id_maquina",
      key: "id_maquina",
      width: "10%",
      sorter: (a, b) => a.id_maquina.length - b.id_maquina.length,
      ellipsis: true,
    },
    {
      title: "Vendedor",
      dataIndex: "id_vendedor",
      key: "id_vendedor",
      width: "10%",
      sorter: (a, b) => a.id_vendedor.length - b.id_vendedor.length,
      ellipsis: true,
    },
    {
      title: "",
      dataIndex: "accoes",
      key: "accoes",
      render: (text, record) => {
        //console.log(record.key);
        return (
          <>
            <a href="#" onClick={()=>handleDetails(record)}><FileSearchOutlined /> Detalhes</a>
          </>
        );
      },
    },
  ];

  const generateOperations = (operations) => {
    const generatedOperations =
      operations &&
      operations.map((item) => {
        return {
          key: item.id,
          data: item.operationStartedAt,
          numero_documento: item.id,
          tipo: item.type,
          valor: item.amount,
          inicio: item.operationStartedAt,
          fim: item.operationFinishedAt,
          tipo_device: item.deviceType,
          id_vendedor: item.sellerId,
          id_maquina: item.machineId,
          accoes: "",
        };
      });

    return generatedOperations;
  };

  const onShowSizeChange = (current, pageSize) => {
    setCurrentPageSize(pageSize);
    console.log('Current: ', currentPageSize);
    console.log(current, pageSize);
  };

  const openNotificationWithIcon = (tipo, titulo, mensagem) => {
    notification[tipo]({
      message: titulo,
      description: mensagem
    });
  };

  const getFiltredResult = (e) => {
    console.log('FILTRED VALUE: ',e);
    dispatch(getData({ page: "", searchTerm: e, size: "" }));
  };

  return (
    <Row className="ccc-layout-content-general">
      <Col span={24}>
        <Spin spinning={false}>
          <PageHeader
            className="site-page-header-responsive"
            onBack={() => window.history.back()}
            title="Operações"
          subTitle="Listagem das Operações"
          extra={[
            <>
              <div>De</div>
              <DatePicker onChange={onChange} />
              <div> a </div>
              <DatePicker onChange={onChange} />
              <Button
                type="primary"
                onClick={() => {getOperations()}}
              >
                Pesquisar
              </Button>
            </>,
          ]}
            footer={<Tabs defaultActiveKey="1"></Tabs>}
          >
            <Row>
              <Col span={24}>
              <Table
                columns={columns}
                pagination={{
                  pageSize: 50,
                  total: allOperations.totalElements,
                  showTotal: (total) => `Total ${total} Registos`,
                  defaultPageSize: 50,
                  defaultCurrent: 1,
                  onShowSizeChange: () => handleOnShowSizeChange(),
                  showSizeChanger: true,
                }}
                dataSource={operations}
                size="small"
                loading={isProcessing.status}
              />
              </Col>
            </Row>
          </PageHeader>
        </Spin>
      </Col>
      <Col span={24}>
        <Modal width={900} title={"Detalhes da Operação - "+operation.id} visible={isModalVisible} onOk={handleOk} onCancel={handleCancel}>
          <Skeleton loading={isGetByIdProcessing.status} active>
          <div className="content">
                  <div className="extra">
                    <div
                      style={{
                        display: "flex",
                        width: "max-content",
                        justifyContent: "flex-end",
                      }}
                    >
                      <Statistic
                        title="Tipo"
                        value={operation.type}
                        style={{
                          marginRight: 32,
                        }}
                      />
                      <Statistic
                        title="Tipo de Dispositivo"
                        value={operation.deviceType}
                        style={{
                          marginRight: 32,
                        }}
                      />
                      <Statistic title="Valor" value={operation.amount} />
                    </div>
                  </div>
                  <div className="main">
                    <Descriptions size="small" column={2}>
                      <Descriptions.Item label="Inicio da Operação">
                        {operation.operationStartedAt}
                      </Descriptions.Item>
                      <Descriptions.Item label="Fim da Operação">
                        {operation.operationFinishedAt}
                      </Descriptions.Item>
                      <Descriptions.Item label="Vendedor">
                        {operation.sellerId}
                      </Descriptions.Item>
                      <Descriptions.Item label="Maquina">
                        {operation.machineId}
                      </Descriptions.Item>
                    </Descriptions>
                  </div>
                </div>
          </Skeleton>
        </Modal>
      </Col>
    </Row>
  );
};

export default Maquinas;
