import { combineReducers } from "redux";
import API from "../../shared/api";

//Actions Creators 
//Get Request
export const getData = ({ page, searchTerm, size }) => async (dispatch) => {
  dispatch({ type: GET_OPERATION_REQUEST });

  try {
    const response = await API.get("/operations", { page, searchTerm, size });
    dispatch({ type: GET_OPERATION_SUCCESS, payload: response });
  } catch (error) {
    dispatch({ type: GET_OPERATION_ERROR, payload: error });
  }
};

//Get by ID Request
export const getDataById = (code) => async (dispatch) => {
  dispatch({ type: GET_OPERATION_BY_ID_REQUEST });

  try {
    const response = await API.get(`/operations/${code}`);
    dispatch({ type: GET_OPERATION_BY_ID_SUCCESS, payload: response });
  } catch (error) {
    dispatch({ type: GET_OPERATION_BY_ID_ERROR, payload: error });
  }
};

//Selectors 
export const getAllOperations = state => state.operations.allOperations;
export const getOperation = state => state.operations.operation;
export const getIsGetProcessing = state => state.operations.isGetProcessing;
export const getIsGetByIdProcessing = state => state.operations.isGetByIdProcessing;
export const getError = state => state.operations.errorGet;
export const getByIdError = state => state.operations.errorGetById;

// ---------------------------
//Accoes
//Requests
const GET_OPERATION_REQUEST = "GET_OPERATION_REQUEST";
const GET_OPERATION_BY_ID_REQUEST = "GET_OPERATION_BY_ID_REQUEST";
//Erros
const GET_OPERATION_ERROR = "GET_OPERATION_ERROR";
const GET_OPERATION_BY_ID_ERROR = "GET_OPERATION_BY_ID_ERROR";

//Sucesso
const GET_OPERATION_SUCCESS = "GET_OPERATION_SUCCESS";
const GET_OPERATION_BY_ID_SUCCESS = "GET_OPERATION_BY_ID_SUCCESS";

// ---------------------------

//Reducers -> Mensagens de Erro

const errorGet = (state = null, action) => {
  const { type, payload } = action;

  switch (type) {
    case GET_OPERATION_ERROR:
      return { message: payload.message, httpcode: payload.httpcode };
    default:
      return state;
  }
};

const errorGetById = (state = null, action) => {
  const { type, payload } = action;

  switch (type) {
    case GET_OPERATION_BY_ID_ERROR:
      return { message: payload.message, httpcode: payload.httpcode };
    default:
      return state;
  }
};

const isGetProcessing = (state = { status: false, message: null }, action) => {
  const { type } = action;

  switch (type) {
    case GET_OPERATION_REQUEST:
      return {
        status: true,
        message: null
      };
    case GET_OPERATION_BY_ID_REQUEST:
      return {
        status: false,
        message: null
      };
    case GET_OPERATION_SUCCESS:
      return {
        status: false,
        message: 'Sucesso'
      };
    case GET_OPERATION_ERROR:
      return {
        status: false,
        message: 'Erro'
      };
    default:
      return state;
  }
};

const isGetByIdProcessing = (state = { status: false, message: null }, action) => {
  const { type } = action;

  switch (type) {
    case GET_OPERATION_BY_ID_REQUEST:
      return {
        status: true,
        message: null
      };
    case GET_OPERATION_REQUEST:
      return {
        status: false,
        message: null
      };
    case GET_OPERATION_BY_ID_SUCCESS:
      return {
        status: false,
        message: 'Sucesso'
      };
    case GET_OPERATION_BY_ID_ERROR:
      return {
        status: false,
        message: 'Erro'
      };
    default:
      return state;
  }
};

//Reducers -> Manipulacao do estado
const allOperations = (state = {}, action) => {
  const { type, payload } = action;

  switch (type) {
    case GET_OPERATION_SUCCESS:
      return {
        data: payload.list,
        totalPages: payload.totalPages,
        currentPage: payload.currentPage,
        totalElements: payload.totalElements
      };
    default:
      return state;
  }
};

//Reducers -> Manipulacao do estado
const operation = (state = { metadata: [] }, action) => {
  const { type, payload } = action;

  switch (type) {
    case GET_OPERATION_BY_ID_SUCCESS:
      return {
        // metadata: payload.machine.metadata,
        machineId: payload.machineId,
        id: payload.id,
        serialNumber: payload.serialNumber,
        sellerId: payload.sellerId,
        operationStartedAt: payload.operationStartedAt,
        operationFinishedAt: payload.operationFinishedAt,
        status: payload.status,
        type: payload.type,
        amount: payload.amount,
        deviceType: payload.deviceType,
      };
    default:
      return state;
  }
};

//Export All Reducers 
export default combineReducers({
  allOperations,
  operation,
  errorGet,
  errorGetById,
  isGetProcessing,
  isGetByIdProcessing
});

