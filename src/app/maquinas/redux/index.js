import { combineReducers } from "redux";
import API from "../../shared/api";

//Actions Creators 
//Post Request
export const saveData = ({ bluetoothPin, brand, macAddress, model, serialNumber }) => async (dispatch) => {
  dispatch({ type: CREATE_REQUEST });
  try {
    const response = await API.post("/machines/create", { bluetoothPin, brand, macAddress, model, serialNumber });
    dispatch({ type: CREATE_SUCCESS, payload: response });
  } catch (error) {
    dispatch({ type: CREATE_ERROR, payload: error });
  }
};

//Update Request
export const updateRow = ({ code, bluetoothPin, brand, macAddress, model, serialNumber }) => async (dispatch) => {
  dispatch({ type: UPDATE_REQUEST });
  try {
    const response = await API.post(`/machines/${code}/update`, { bluetoothPin, brand, macAddress, model, serialNumber });
    dispatch({ type: UPDATE_SUCCESS, payload: response });
  } catch (error) {
    dispatch({ type: UPDATE_ERROR, payload: error });
  }
};

//Delete Request
export const deleteRow = ({ code }) => async (dispatch) => {
  dispatch({ type: DELETE_REQUEST });

  try {
    const response = await API.post(`/machines/${code}/delete`);
    dispatch({ type: DELETE_SUCCESS, payload: response });
  } catch (error) {
    dispatch({ type: DELETE_ERROR, payload: error });
  }
};

//Get Request
export const getData = ({ page, searchTerm, size }) => async (dispatch) => {
  dispatch({ type: GET_REQUEST });

  try {
    const response = await API.get("/machines", { page, searchTerm, size });
    dispatch({ type: GET_SUCCESS, payload: response });
  } catch (error) {
    dispatch({ type: GET_ERROR, payload: error });
  }
};

//Get by ID Request
export const getDataById = (code) => async (dispatch) => {
  dispatch({ type: GET_BY_ID_REQUEST });

  try {
    const response = await API.get(`/machines/${code}`);
    dispatch({ type: GET_BY_ID_SUCCESS, payload: response });
  } catch (error) {
    dispatch({ type: GET_BY_ID_ERROR, payload: error });
  }
};

//Selectors 
export const getAllMachines = state => state.machines.allMachines;
export const getMachine = state => state.machines.machine;
export const getIsDeleteProcessing = state => state.machines.isDeleteProcessing;
export const getIsGetProcessing = state => state.machines.isGetProcessing;
export const getIsGetByIdProcessing = state => state.machines.isGetByIdProcessing;
export const getIsCreateProcessing = state => state.machines.isCreateProcessing;
export const getIsUpdateProcessing = state => state.machines.isUpdateProcessing;
export const getError = state => state.machines.errorGet;
export const getByIdError = state => state.machines.errorGetById;
export const getCreateError = state => state.machines.errorCreate;
export const getDeleteError = state => state.machines.errorDelete;
export const getUpdateError = state => state.machines.errorUpdate;

// ---------------------------
//Accoes
//Requests
const GET_REQUEST = "GET_REQUEST";
const GET_BY_ID_REQUEST = "GET_BY_ID_REQUEST";
const CREATE_REQUEST = "CREATE_REQUEST";
const DELETE_REQUEST = "DELETE_REQUEST";
const UPDATE_REQUEST = "UPDATE_REQUEST";
//Erros
const GET_ERROR = "GET_ERROR";
const GET_BY_ID_ERROR = "GET_BY_ID_ERROR";
const CREATE_ERROR = "CREATE_ERROR";
const UPDATE_ERROR = "UPDATE_ERROR";
const DELETE_ERROR = "DELETE_ERROR";

//Sucesso
const CREATE_SUCCESS = "CREATE_SUCCESS";
const GET_SUCCESS = "GET_SUCCESS";
const GET_BY_ID_SUCCESS = "GET_BY_ID_SUCCESS";
const DELETE_SUCCESS = "DELETE_SUCCESS";
const UPDATE_SUCCESS = "UPDATE_SUCCESS";

// ---------------------------

//Reducers -> Mensagens de Erro

const errorCreate = (state = null, action) => {
  const { type, payload } = action;

  switch (type) {
    case CREATE_ERROR:
      return { message: payload.message, httpcode: payload.httpcode };
    default:
      return state;
  }
};

const errorUpdate = (state = null, action) => {
  const { type, payload } = action;

  switch (type) {
    case UPDATE_ERROR:
      return { message: payload.message, httpcode: payload.httpcode };
    default:
      return state;
  }
};

const errorDelete = (state = null, action) => {
  const { type, payload } = action;

  switch (type) {
    case DELETE_ERROR:
      return { message: payload.message, httpcode: payload.httpcode };
    default:
      return state;
  }
};

const errorGet = (state = null, action) => {
  const { type, payload } = action;

  switch (type) {
    case GET_ERROR:
      return { message: payload.message, httpcode: payload.httpcode };
    default:
      return state;
  }
};

const errorGetById = (state = null, action) => {
  const { type, payload } = action;

  switch (type) {
    case GET_BY_ID_ERROR:
      return { message: payload.message, httpcode: payload.httpcode };
    default:
      return state;
  }
};

const isGetProcessing = (state = { status: false, message: null }, action) => {
  const { type } = action;

  switch (type) {
    case GET_REQUEST:
      return {
        status: true,
        message: null
      };
    case UPDATE_REQUEST:
    case DELETE_REQUEST:
    case GET_BY_ID_REQUEST:
    case CREATE_REQUEST:
      return {
        status: false,
        message: null
      };
    case GET_SUCCESS:
      return {
        status: false,
        message: 'Sucesso'
      };
    case GET_ERROR:
      return {
        status: false,
        message: 'Erro'
      };
    default:
      return state;
  }
};

const isGetByIdProcessing = (state = { status: false, message: null }, action) => {
  const { type } = action;

  switch (type) {
    case GET_BY_ID_REQUEST:
      return {
        status: true,
        message: null
      };
    case UPDATE_REQUEST:
    case DELETE_REQUEST:
    case GET_REQUEST:
    case CREATE_REQUEST:
      return {
        status: false,
        message: null
      };
    case GET_BY_ID_SUCCESS:
      return {
        status: false,
        message: 'Sucesso'
      };
    case GET_BY_ID_ERROR:
      return {
        status: false,
        message: 'Erro'
      };
    default:
      return state;
  }
};

const isCreateProcessing = (state = { status: false, message: null }, action) => {
  const { type } = action;

  switch (type) {
    case CREATE_REQUEST:
      return {
        status: true,
        message: null
      };
    case DELETE_REQUEST:
    case GET_BY_ID_REQUEST:
    case GET_REQUEST:
    case UPDATE_REQUEST:
      return {
        status: false,
        message: null
      };
    case CREATE_SUCCESS:
      return {
        status: false,
        message: 'Sucesso'
      };
    case CREATE_ERROR:
      return {
        status: false,
        message: 'Erro'
      };
    default:
      return state;
  }
};

const isUpdateProcessing = (state = { status: false, message: null }, action) => {
  const { type } = action;

  switch (type) {
    case UPDATE_REQUEST:
      return {
        status: true,
        message: null
      };
    case DELETE_REQUEST:
    case GET_BY_ID_REQUEST:
    case GET_REQUEST:
    case CREATE_REQUEST:
      return {
        status: false,
        message: null
      };
    case UPDATE_SUCCESS:
      return {
        status: false,
        message: 'Sucesso'
      };
    case UPDATE_ERROR:
      return {
        status: false,
        message: 'Erro'
      };
    default:
      return state;
  }
};

const isDeleteProcessing = (state = { status: false, message: null }, action) => {
  const { type } = action;

  switch (type) {
    case DELETE_REQUEST:
      return {
        status: true,
        message: null
      };
    case CREATE_REQUEST:
    case GET_BY_ID_REQUEST:
    case GET_REQUEST:
    case UPDATE_REQUEST:
      return {
        status: false,
        message: null
      }
    case DELETE_SUCCESS:
      return {
        status: false,
        message: 'sucesso'
      };
    case DELETE_ERROR:
      return {
        status: false,
        message: 'erro'
      };
    default:
      return state;
  }
};

//Reducers -> Manipulacao do estado
const allMachines = (state = { data: [] }, action) => {
  const { type, payload } = action;

  switch (type) {
    case GET_SUCCESS:
      return {
        data: payload.list,
        totalPages: payload.totalPages,
        currentPage: payload.currentPage,
        totalElements: payload.totalElements
      };
    default:
      return state;
  }
};

//Reducers -> Manipulacao do estado
const machine = (state = { metadata: [] }, action) => {
  const { type, payload } = action;

  switch (type) {
    case GET_BY_ID_SUCCESS:
      return {
        // metadata: payload.machine.metadata,
        machineId: payload.id,
        serialNumber: payload.serialNumber,
        brand: payload.brand,
        model: payload.model,
        macAddress: payload.macAddress,
        bluetoothPin: payload.bluetoothPin,
      };
    default:
      return state;
  }
};

//Export All Reducers 
export default combineReducers({
  allMachines,
  machine,
  errorCreate,
  errorUpdate,
  errorDelete,
  errorGet,
  errorGetById,
  isGetProcessing,
  isCreateProcessing,
  isDeleteProcessing,
  isUpdateProcessing,
  isGetByIdProcessing
});

