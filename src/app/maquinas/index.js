import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  Row,
  Col,
  Table,
  Input,
  Button,
  Menu,
  Modal,
  Dropdown,
  PageHeader,
  Tabs,
  Spin,
  notification,
} from "antd";
import {
  EditOutlined,
  DeleteOutlined,
  PicLeftOutlined,
  DownOutlined,
  ExclamationCircleOutlined,
} from "@ant-design/icons";
import {
  useHistory,
} from "react-router-dom";

import {
  getData,
  deleteRow,
  getAllMachines,
  getError,
  getDeleteError,
  getIsDeleteProcessing,
  getIsGetProcessing,
} from "./redux";

const { confirm } = Modal;

const Maquinas = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  //Local State
  const [currentPageSize, setCurrentPageSize] = useState(5);
  const [machines, setMachines] = useState();
  const [searchVal, setSearchVal] = useState(null);

  //Store State 
  const allMachines = useSelector(getAllMachines);

  //Processing status
  const isProcessing = useSelector(getIsGetProcessing);
  const isDeleteProcessing = useSelector(getIsDeleteProcessing);

  //Error status
  const errorGet = useSelector(getError);
  const errorDelete = useSelector(getDeleteError);

  //Local Variables 
  const SUCESSO = 'sucesso';

  //useEffect Operations
  useEffect(() => {
    dispatch(getData({ page: "", searchTerm: "", size: "" }));
  }, []);

  //Get Processing 
  useEffect(() => {
    if (isProcessing.message !== null) {
      if (isProcessing.message.toLowerCase() == SUCESSO && !isProcessing.status) {
        console.info('Carregado com sucesso');
        setMachines(generateMachines(allMachines.data));
        console.log('Data: ', generateMachines(allMachines.data));
      } else {
        openNotificationWithIcon('error', 'Erro', '' + errorGet.message);
      }
    }
  }, [isProcessing, errorGet]);

  //Delete Processing 
  useEffect(() => {
    if (isDeleteProcessing.message !== null) {
      if (isDeleteProcessing.message.toLowerCase() == SUCESSO && !isDeleteProcessing.status) {
        openNotificationWithIcon('success', 'Sucesso', 'Registo Eliminado com Sucesso!');
        dispatch(getData({ page: "", searchTerm: "", size: "" }));
      } else {
        openNotificationWithIcon('error', 'Erro', '' + errorDelete.message);
      }
    }
  }, [isDeleteProcessing, errorDelete]);

  //Local Functions 
  const handleMenuOption = (record, key) => {
    if (key.key === "0") {
      handleUpdate(record.key);
    } else if (key.key === "1") {
      handleDetails(record.key);
      // console.log(record);
    } else if (key.key === "2") {
      handleDelete(record.key);
    }
  };

  const handleUpdate = (id) => {
    history.push(`/alterar-maquina/${id}`);
  };

  const handleDetails = (id) => {
    history.push(`/detalhes-maquina/${id}`);
  };

  const handleDelete = (id) => {
    showDeleteConfirm(id);
  };

  const showDeleteConfirm = (id) => {
    confirm({
      title: 'Tem certeza que quer APAGAR/EXCLUIR esse Registo?',
      icon: <ExclamationCircleOutlined />,
      content: '',
      okText: 'Sim',
      okType: 'danger',
      cancelText: 'Não',
      onOk() {
        console.log('OK');
        dispatch(
          deleteRow({
            code: id,
          })
        );
      },
      onCancel() {
        console.log('Cancelado');
      },
    });
  }

  const menu = (record) => {
    return (
      <Menu
        className="ccc-dropdown-menu"
        onClick={(key) => handleMenuOption(record, key)}
      >
        <Menu.Item key="0">
          <EditOutlined /> Alterar
        </Menu.Item>
        <Menu.Divider />
        <Menu.Item key="1">
          <PicLeftOutlined /> Detalhar
        </Menu.Item>
        <Menu.Divider />
        <Menu.Item className="ccc-delete-item" key="2">
          <DeleteOutlined /> Excluir
        </Menu.Item>
      </Menu>
    );
  };

  const columns = [
    {
      title: "Marca",
      dataIndex: "marca",
      key: "marca",
      width: "20%",
      sorter: (a, b) => a.marca.length - b.marca.length,
      ellipsis: true,
    },
    {
      title: "Modelo",
      dataIndex: "modelo",
      key: "modelo",
      width: "20%",
      sorter: (a, b) => a.modelo.length - b.modelo.length,
      ellipsis: true,
    },
    {
      title: "Número de Serie",
      dataIndex: "numero_serie",
      key: "numero_serie",
      width: "20%",
      sorter: (a, b) => a.numero_serie.length - b.numero_serie.length,
      ellipsis: true,
    },
    {
      title: "Endereço MAC",
      dataIndex: "endereco_mac",
      key: "endereco_mac",
      width: "15%",
      sorter: (a, b) => a.endereco_mac.length - b.endereco_mac.length,
      ellipsis: true,
    },
    {
      title: "PIN do Bluetooth",
      dataIndex: "bluetooth_pin",
      key: "bluetooth_pin",
      width: "15%",
      sorter: (a, b) => a.bluetooth_pin.length - b.bluetooth_pin.length,
      ellipsis: true,
    },
    {
      title: "",
      dataIndex: "accoes",
      key: "accoes",
      render: (text, record) => {
        //console.log(record.key);
        return (
          <Dropdown overlay={menu(record)} trigger={["click"]}>
            <Button className="ccc-btn-sec" onClick={(e) => e.preventDefault()}>
              Acções <DownOutlined />
            </Button>
          </Dropdown>
        );
      },
    },
  ];

  const generateMachines = (machines) => {
    const generatedMachines =
      machines &&
      machines.map((item) => {
        return {
          key: item.id,
          marca: item.brand,
          modelo: item.model,
          numero_serie: item.serialNumber,
          endereco_mac: item.macAddress,
          bluetooth_pin: item.bluetoothPin,
          accoes: "",
        };
      });

    return generatedMachines;
  };

  const onShowSizeChange = (current, pageSize) => {
    setCurrentPageSize(pageSize);
    console.log('Current: ', currentPageSize);
    console.log(current, pageSize);
  };

  const openNotificationWithIcon = (tipo, titulo, mensagem) => {
    notification[tipo]({
      message: titulo,
      description: mensagem
    });
  };

  const getFiltredResult = (e) => {
    console.log('FILTRED VALUE: ',e);
    dispatch(getData({ page: "", searchTerm: e, size: "" }));
  };

  return (
    <Row className="ccc-layout-content-general">
      <Col span={24}>
        <Spin spinning={isDeleteProcessing.status} tip="Eliminando o Registo...">
          <PageHeader
            className="site-page-header-responsive"
            onBack={() => window.history.back()}
            title="Maquinas"
            subTitle="Listagem das Maquinas"
            extra={[
              <>
                <Input.Search
                  placeholder="Procurar Maquina"
                  onSearch={getFiltredResult}
                  onChange={(e) => setSearchVal(e.target.value)}
                />
                <Button
                  type="primary"
                  onClick={() => history.push(`/nova-maquina`)}
                >
                  Nova Maquina
                </Button>
              </>,
            ]}
            footer={<Tabs defaultActiveKey="1"></Tabs>}
          >
            <Row>
              <Col span={24}>
                <Table
                  columns={columns}
                  pagination={{
                    pageSize: currentPageSize,
                    total: allMachines.totalElements,
                    showTotal: (total) => `Total ${total} Registos`,
                    defaultCurrent: 1,
                    pageSizeOptions: [5, 10, 25, 50, 100],
                    onShowSizeChange: (defaultCurrent, pageSize) => onShowSizeChange(defaultCurrent, pageSize),
                    showSizeChanger: true,
                  }}
                  dataSource={machines}
                  size="small"
                  loading={isProcessing.status}
                />
              </Col>
            </Row>
          </PageHeader>
        </Spin>
      </Col>
    </Row>
  );
};

export default Maquinas;
