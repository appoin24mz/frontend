import React, { useEffect } from "react";
import {
  Row,
  Col,
  PageHeader,
  Button,
  Skeleton,
  Statistic,
  Descriptions,
  Modal,
  notification,
} from "antd";
import {
  getMachine,
  deleteRow,
  getIsGetByIdProcessing,
  getIsDeleteProcessing,
  getByIdError,
  getDeleteError,
  getDataById,
} from "./redux";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, useParams } from "react-router-dom";
import { ExclamationCircleOutlined, } from "@ant-design/icons";


const { confirm } = Modal;

const Detalhes = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  //Local Variables 
  const SUCESSO = 'sucesso';
  let { id } = useParams();

  //Local State

  //Store State 
  const machine = useSelector(getMachine);

  //Processing status
  const isGetByIdProcessing = useSelector(getIsGetByIdProcessing);
  const isDeleteProcessing = useSelector(getIsDeleteProcessing);

  //Error status
  const errorDelete = useSelector(getDeleteError);
  const errorGetById = useSelector(getByIdError);

  //useEffect Operations
  useEffect(() => {
    dispatch(getDataById(id));
  }, []);

  useEffect(() => {
    if (isGetByIdProcessing.message !== null) {
      if (isGetByIdProcessing.message.toLowerCase() == 'Sucesso'.toLowerCase() && !isGetByIdProcessing.status) {
        console.info('Carregado com sucesso');
      } else {
        openNotificationWithIcon('error', 'Erro', '' + errorGetById.message);
      }
    }
  }, [isGetByIdProcessing, errorGetById]);

  //Delete Processing 
  useEffect(() => {
    if (isDeleteProcessing.message !== null) {
      if (isDeleteProcessing.message.toLowerCase() == SUCESSO && !isDeleteProcessing.status) {
        openNotificationWithIcon('success', 'Sucesso', 'Registo Eliminado com Sucesso!');
        history.push('/maquinas');
      } else {
        openNotificationWithIcon('error', 'Erro', '' + errorDelete.message);
      }
    }
  }, [isDeleteProcessing, errorDelete]);

  //Local Functions 
  const handleAction = (id) => {
    console.log("Association - Action: ", id);
  };

  const handleUpdate = (id) => {
    history.push(`/alterar-maquina/${id}`);
  };

  const handleShowAllMachines = () => {
    history.push(`/maquinas`);
  };

  const openNotificationWithIcon = (tipo, titulo, mensagem) => {
    notification[tipo]({
      message: titulo,
      description: mensagem
    });
  };

  const handleDelete = (id) => {
    showDeleteConfirm(id);
  };

  const showDeleteConfirm = (id) => {
    confirm({
      title: 'Tem certeza que quer APAGAR/EXCLUIR esse Registo?',
      icon: <ExclamationCircleOutlined />,
      content: '',
      okText: 'Sim',
      okType: 'danger',
      cancelText: 'Não',
      onOk() {
        dispatch(
          deleteRow({
            code: id,
          })
        );
      },
      onCancel() {
        console.log('Cancelado');
      },
    });
  };

  return (
    <Row className="ccc-layout-content-general">
      <Col span={24}>
        <PageHeader
          className="site-page-header-responsive"
          onBack={() => window.history.back()}
          title="Detalhes da Maquina"
          subTitle=""
          extra={[
            <>
              <Button
                type="default"
                style={{ marginLeft: "10px" }}
                onClick={() => handleAction(machine.machineId)}
              >
                Associar a Entidade
              </Button>
              <Button
                type="default"
                style={{ marginLeft: "10px" }}
                onClick={() => handleShowAllMachines()}
              >
                Todas Maquinas
              </Button>
              <Button
                type="primary"
                style={{ marginLeft: "10px" }}
                onClick={() => handleUpdate(machine.machineId)}
              >
                Alterar
              </Button>
              <Button
                type="primary"
                danger
                style={{ marginLeft: "10px" }}
                onClick={() => handleDelete(machine.machineId)}
              >
                Excluir
              </Button>
            </>,
          ]}
        >
          <Row>
            <Col span={24}>
              <Skeleton loading={isGetByIdProcessing.status} active>
                <div className="content">
                  <div className="extra">
                    <div
                      style={{
                        display: "flex",
                        width: "max-content",
                        justifyContent: "flex-end",
                      }}
                    >
                      <Statistic
                        title="Marca"
                        value={machine.brand}
                        style={{
                          marginRight: 32,
                        }}
                      />
                      <Statistic title="Modelo" value={machine.model} />
                    </div>
                  </div>
                  <div className="main">
                    <Descriptions size="small" column={3}>
                      <Descriptions.Item label="Número de Série">
                        {machine.serialNumber}
                      </Descriptions.Item>
                      <Descriptions.Item label="Mac Address">
                        {machine.macAddress}
                      </Descriptions.Item>
                      <Descriptions.Item label="Bluetooth Pin">
                        {machine.bluetoothPin}
                      </Descriptions.Item>
                    </Descriptions>
                  </div>
                </div>
              </Skeleton>
            </Col>
          </Row>
        </PageHeader>
      </Col>
    </Row>
  );
};

export default Detalhes;
