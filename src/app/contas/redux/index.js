import { combineReducers } from "redux";
import API from "../../shared/api";

//Actions Creators 
//Post Request
export const saveData = ({ customerCode }) => async (dispatch) => {
  dispatch({ type: CREATE_REQUEST });
  try {
    const response = await API.post("/accounts/create", { customerCode });
    dispatch({ type: CREATE_SUCCESS, payload: response });
  } catch (error) {
    dispatch({ type: CREATE_ERROR, payload: error });
  }
};

//Update Request
export const updateRow = ({ code, customerCode }) => async (dispatch) => {
  dispatch({ type: UPDATE_REQUEST });
  try {
    const response = await API.post(`/accounts/${code}/update`, { customerCode });
    dispatch({ type: UPDATE_SUCCESS, payload: response });
  } catch (error) {
    dispatch({ type: UPDATE_ERROR, payload: error });
  }
};

//Delete Request
export const deleteRow = ({ code }) => async (dispatch) => {
  dispatch({ type: DELETE_REQUEST });
  try {
    const response = await API.post(`/accounts/${code}/delete`);
    dispatch({ type: DELETE_SUCCESS, payload: response });
  } catch (error) {
    dispatch({ type: DELETE_ERROR, payload: error });
  }
};

//Get Request
export const getData = ({ page, searchTerm, size }) => async (dispatch) => {
    dispatch({ type: GET_REQUEST });
  
    try {
      const response = await API.get("/accounts", { page, searchTerm, size });
      dispatch({ type: GET_SUCCESS, payload: response });
    } catch (error) {
      dispatch({ type: GET_ERROR, payload: error });
    }
  };

  //Get by ID Request
export const getDataById = (code) => async (dispatch) => {
  dispatch({ type: GET_BY_ID_REQUEST });

  try {
    const response = await API.get(`/accounts/${code}`);
    dispatch({ type: GET_BY_ID_SUCCESS, payload: response });
  } catch (error) {
    dispatch({ type: GET_BY_ID_ERROR, payload: error });
  }
};

//Selectors 
export const getAllaccounts = state => state.accounts.allAccounts;
export const getAccount = state => state.accounts.account;
export const isRequestProcessing = state => state.accounts.isProcessing;
export const getError = state => state.accounts.error;

// ---------------------------
//Accoes
//Requests
const GET_REQUEST = "GET_REQUEST";
const GET_BY_ID_REQUEST = "GET_BY_ID_REQUEST";
const CREATE_REQUEST = "CREATE_REQUEST";
const DELETE_REQUEST = "CREATE_REQUEST";
const UPDATE_REQUEST = "CREATE_REQUEST";
//Erros
const GET_ERROR = "GET_ERROR";
const GET_BY_ID_ERROR = "GET_BY_ID_ERROR";
const CREATE_ERROR = "CREATE_ERROR";
const UPDATE_ERROR = "UPDATE_ERROR";
const DELETE_ERROR = "DELETE_ERROR";

//Sucesso
const CREATE_SUCCESS = "CREATE_SUCCESS";
const GET_SUCCESS = "GET_SUCCESS";
const GET_BY_ID_SUCCESS = "GET_BY_ID_SUCCESS";
const DELETE_SUCCESS = "DELETE_SUCCESS";
const UPDATE_SUCCESS = "UPDATE_SUCCESS";

// ---------------------------

//Reducers -> Mensagens de Erro
const error = (state = null, action) => {
  const { type, payload } = action;

  switch (type) {
    case CREATE_ERROR:
      return { message: payload.message, httpcode: payload.httpcode };
    case GET_ERROR:
        return { message: payload.message, httpcode: payload.httpcode };
    case GET_BY_ID_ERROR:
        return { message: payload.message, httpcode: payload.httpcode };
    default:
      return state;
  }
};

//Reducers -> Controlo de processamento
const isProcessing = (state = false, action) => {
    const { type } = action;
  
    switch (type) {
        case GET_BY_ID_REQUEST:
          return true;
        case GET_BY_ID_SUCCESS:
          return false;
        case GET_BY_ID_ERROR:
          return false;
        case GET_REQUEST:
          return true;
        case GET_SUCCESS:
          return false;
        case GET_ERROR:
          return false;
        case CREATE_REQUEST:
          return true;
        case CREATE_SUCCESS:
          return false;
        case CREATE_ERROR:
          return false;
        case UPDATE_REQUEST:
          return true;
        case UPDATE_SUCCESS:
          return false;
        case UPDATE_ERROR:
          return false;
        case DELETE_REQUEST:
          return true;
        case DELETE_SUCCESS:
          return false;
        case DELETE_ERROR:
          return false;
      default:
        return state;
    }
  };

  //Reducers -> Manipulacao do estado
  const allAccounts = (state = {data:[]}, action) => {
    const { type, payload } = action;
  
    switch (type) {
        case GET_SUCCESS:
            return {
                data: payload.list,
                totalPages: payload.totalPages,
                currentPage: payload.currentPage,
                totalElements: payload.totalElements
            };
        default:
            return state;
    }
  };

  //Reducers -> Manipulacao do estado
  const account = (state = {metadata:[]}, action) => {
    const { type, payload } = action;
  
    switch (type) {
        case GET_BY_ID_SUCCESS:
          return {
                metadata: payload.account.metadata,
                accountId: payload.account.accountId,
                state: payload.account.state,
          };
        default:
            return state;
    }
  };

  //Export All Reducers 
export default combineReducers({
  error,
  isProcessing,
  allAccounts,
  account
});

