import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  Row,
  Col,
  Table,
  Input,
  Button,
  Menu,
  Modal,
  Dropdown,
  PageHeader,
  Tabs,
} from "antd";
import {
  EditOutlined,
  DeleteOutlined,
  PicLeftOutlined,
  DownOutlined,
  ExclamationCircleOutlined,
} from "@ant-design/icons";
import {
  useHistory,
  useRouteMatch,
  useParams,
} from "react-router-dom";

import {
  getData,
  deleteRow,
  getAllMachines,
  isRequestProcessing,
} from "./redux";

const { confirm } = Modal;

const Maquinas = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  const allMachines = useSelector(getAllMachines);
  const isProcessing = useSelector(isRequestProcessing);

  useEffect(() => {
    dispatch(getData({ page: "", searchTerm: "", size: "" }));
  }, []);

  const handleMenuOption = (record, key) => {
    if (key.key === "0") {
      handleUpdate(record.key);
    } else if (key.key === "1") {
      handleDetails(record.key);
    } else if (key.key === "2") {
      handleDelete(record.key);
    }
  };

  const handleUpdate = (id) => {
    history.push(`/alterar-maquina/${id}`);
  };

  const handleDetails = (id) => {
    history.push(`/detalhes-maquina/${id}`);
  };

  const handleDelete = (id) => {
    console.log("DELETE - ID: ", id);
    showDeleteConfirm(id);
  };

  const showDeleteConfirm = (id) => {
    confirm({
      title: 'Tem certeza que quer APAGAR/EXCLUIR esse Registo?',
      icon: <ExclamationCircleOutlined />,
      content: '',
      okText: 'Sim',
      okType: 'danger',
      cancelText: 'Não',
      onOk() {
        console.log('OK');
        dispatch(
          deleteRow({
            code: id,
          })
        );
      },
      onCancel() {
        console.log('Cancelado');
      },
    });
  }

  const menu = (record) => {
    return (
      <Menu
        className="ccc-dropdown-menu"
        onClick={(key) => handleMenuOption(record, key)}
      >
        <Menu.Item key="0">
          <EditOutlined /> Alterar
        </Menu.Item>
        <Menu.Divider />
        <Menu.Item key="1">
          <PicLeftOutlined /> Detalhar
        </Menu.Item>
        <Menu.Divider />
        <Menu.Item className="ccc-delete-item" key="2">
          <DeleteOutlined /> Excluir
        </Menu.Item>
      </Menu>
    );
  };

  const columns = [
    {
      title: "Marca",
      dataIndex: "marca",
      key: "marca",
      width: "20%",
      sorter: (a, b) => a.marca.length - b.marca.length,
      ellipsis: true,
    },
    {
      title: "Modelo",
      dataIndex: "modelo",
      key: "modelo",
      width: "20%",
      sorter: (a, b) => a.modelo.length - b.modelo.length,
      ellipsis: true,
    },
    {
      title: "Número de Serie",
      dataIndex: "numero_serie",
      key: "numero_serie",
      width: "20%",
      sorter: (a, b) => a.numero_serie.length - b.numero_serie.length,
      ellipsis: true,
    },
    {
      title: "Endereço MAC",
      dataIndex: "endereco_mac",
      key: "endereco_mac",
      width: "15%",
      sorter: (a, b) => a.endereco_mac.length - b.endereco_mac.length,
      ellipsis: true,
    },
    {
      title: "PIN do Bluetooth",
      dataIndex: "bluetooth_pin",
      key: "bluetooth_pin",
      width: "15%",
      sorter: (a, b) => a.bluetooth_pin.length - b.bluetooth_pin.length,
      ellipsis: true,
    },
    {
      title: "",
      dataIndex: "accoes",
      key: "accoes",
      render: (text, record) => {
        //console.log(record.key);
        return (
          <Dropdown overlay={menu(record)} trigger={["click"]}>
            <Button className="ccc-btn-sec" onClick={(e) => e.preventDefault()}>
              Acções <DownOutlined />
            </Button>
          </Dropdown>
        );
      },
    },
  ];

  const generateMachines = (machines) => {
    const generatedMachines =
      machines &&
      machines.map((item) => {
        return {
          key: item.machineId,
          marca: item.brand,
          modelo: item.model,
          numero_serie: item.serialNumber,
          endereco_mac: item.macAddress,
          bluetooth_pin: item.bluetoothPin,
          accoes: "",
        };
      });

    return generatedMachines;
  };

  const handleOnShowSizeChange = (current, pageSize) => {
    console.log("Clicked: " + current);
  };

  return (
    <Row className="edoc-layout-content-general">
      <Col span={24}>
        <PageHeader
          className="site-page-header-responsive"
          onBack={() => window.history.back()}
          title="Maquinas"
          subTitle="Listagem das Maquinas"
          extra={[
            <>
              <Input.Search
                placeholder="Procurar Maquina"
                onSearch={(value) => console.log(value)}
              />
              <Button
                type="primary"
                onClick={() => history.push(`/nova-maquina`)}
              >
                Nova Maquina
              </Button>
            </>,
          ]}
          footer={<Tabs defaultActiveKey="1"></Tabs>}
        >
          <Row>
            <Col span={24}>
              <Table
                columns={columns}
                pagination={{
                  pageSize: 5,
                  total: allMachines.totalElements,
                  showTotal: (total) => `Total ${total} Registos`,
                  defaultPageSize: 5,
                  defaultCurrent: 1,
                  onShowSizeChange: () => handleOnShowSizeChange(),
                  showSizeChanger: true,
                }}
                dataSource={generateMachines(allMachines.data)}
                size="small"
                loading={isProcessing}
              />
            </Col>
          </Row>
        </PageHeader>
      </Col>
    </Row>
  );
};

export default Maquinas;
