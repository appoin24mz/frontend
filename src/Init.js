import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import { Switch, Redirect, Route } from "react-router-dom";
import Login from "./app/shared/auth/ui/Login";
import ClientLogin from "./page/shared/auth/client/Login";
import BackOfficeLogin from "./app/shared/auth/client/Login";
import ClientInit from "./page/shared/auth/client/ClientInit";
import ClientRegister from "./page/shared/auth/client/ClientRegister";
import { isUserLoggedIn } from "./page/shared/auth/redux";
import App from "./app/App";
import ForgetPassword from "./page/shared/auth/ui/ForgetPassword";
import Home from "./page";

const Init = () => {
  const isAuthenticated = useSelector(isUserLoggedIn);

  //useBeforeunload(() => localStorage.clear());

  return (
    <div>
      <Switch>
        <Route exact path="/login">
          <Login />
        </Route>

        <Route exact path="/client-login">
          <ClientLogin />
        </Route>

        <Route exact path="/client-register">
          <ClientRegister />
        </Route>

        <Route exact path="/client-init">
          <ClientInit />
        </Route>

        <Route exact path="/forgetpassword">
          <ForgetPassword />
        </Route>

        <Route exact path="/">
          <Home />
        </Route>

        <Route exact path="/backoffice-login">
          <BackOfficeLogin />
        </Route>

        <PrivateRoute path="/app" isAuthenticated={isAuthenticated}>
          <App />
        </PrivateRoute>

        {/* <Route path="*">
          <Redirect to="/login" />
        </Route> */}
      </Switch>
    </div>
  );
};

// A wrapper for <Route> that redirects to the login
// screen if you're not yet authenticated.
function PrivateRoute({ children, isAuthenticated, ...rest }) {
  return (
    <Route
      {...rest}
      render={() => (isAuthenticated ? children : <Redirect to="/backoffice-login" />)}
    />
  );
}

export default Init;
